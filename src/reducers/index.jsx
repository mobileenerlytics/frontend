// @flow
import { combineReducers } from "redux"
import openDrawer from "./openDrawer"
import about from "./about"
import records from "./records"
import recordStatus from "./recordStatus"
import upload from "./upload"
import commit from "./commit"
import commitStatus from "./commitStatus"
import branch from "./branch"
import project from "./project"
import profile from "./profile"
import common from "./common"
import * as Types from "../types"
import type { CombinedReducer } from "redux"

export const reducers = {
  openDrawer,
  about,
  records,
  recordStatus,
  upload,
  commit,
  commitStatus,
  branch,
  project,
  profile,
  common
}
export type ReducersType = typeof reducers
const rootReducer: CombinedReducer<Types.State, Types.Action> = combineReducers(reducers)
export default rootReducer