// @flow
import * as Types from "../types"

const branch = (state: Array<Types.BranchType> = [], action: Types.Action): Array<Types.BranchType> => {
	switch (action.type) {
		case Types.FETCH_BRANCHES_SUCCESS:
			const newState = mergeStates(action.branches, state)
			newState.sort((a,b) => b.updatedMs - a.updatedMs)
			return newState
		default:
			return state
	}
}

function mergeStates(newBranches: Array<Types.BranchType>, branches: Array<Types.BranchType>): Array<Types.BranchType> {
	let newBranchesId = newBranches.map(branch => branch._id)
	branches = branches.filter(branch => !newBranchesId.includes(branch._id))
	return branches.concat(newBranches)
}

export default branch
