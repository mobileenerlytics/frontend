// @flow
import * as Types from "../types"

const openDrawer = (state: boolean = true, action: Types.Action): boolean => {
  switch (action.type) {
    case Types.OPEN_DRAWER:
      return action.drawer
    default:
      return state
  }
}
export default openDrawer
