// @flow
import * as Types from "../types"
const profile = (state: Types.ProfileType = {}, action: Types.Action): Types.ProfileType => {
  switch (action.type) {
    case Types.FETCH_PROFILE_SUCCESS:
      return action.profile
    default:
      return state
  }
}

export default profile
