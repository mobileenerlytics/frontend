// @flow
import * as Types from "../types"

const commitStatus = (state: Types.StatusType = Types.STATUS_SUCCESS, action: Types.Action): Types.StatusType => {
  switch (action.type) {
    case Types.FETCH_COMMIT_START:
      return Types.STATUS_LOADING
    case Types.FETCH_COMMIT_SUCCESS:
      return Types.STATUS_SUCCESS
    case Types.FETCH_COMMIT_FAILURE:
      return Types.STATUS_FAILURE
    default:
      return state
  }
}

export default commitStatus
