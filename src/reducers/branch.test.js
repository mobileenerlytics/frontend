// @flow
import rootReducer from "./index";
import * as Types from "../types"

describe('branch actions', () => {
  const branchOld: Types.BranchType = {
      branchName:"older",
      updatedMs:1548116571032,
      _id:"5c86dee66f3589b0f3d58523",
      commits: [],
      tests: []
  }
  const branchNew: Types.BranchType  = {
      branchName:"newer",
      updatedMs:1548969999727,   // is latest
      _id:"5c86dee66f3589b0f3d58522",
      commits: [],
      tests: []
  }
  // const commit: Types.CommitType = {

  // }
  // const branch = {...branchOld, commit}

  it('existing branch is latest and should appearing first', () => {
    const action: Types.Action = {
      type: Types.FETCH_BRANCHES_SUCCESS,
      branches: [branchNew]
    }
    expect(rootReducer({branch: [branchOld]}, action).branch).toEqual([branchNew, branchOld])
  }),

  it('fetched branch is latest and should appear first', () => {
    const action: Types.Action = {
      type: Types.FETCH_BRANCHES_SUCCESS,
      branches: [branchOld]
    }
    expect(rootReducer({branch: [branchNew]}, action).branch).toEqual([branchNew, branchOld])
  }),

  it('should reject duplicate branches, instead of appending', () => {
    const action: Types.Action = {
      type: Types.FETCH_BRANCHES_SUCCESS,
      branches: [branchOld]
    }
    expect(rootReducer({branch: [branchOld]}, action).branch).toEqual([branchOld])
  })

  // it("Should merge commits", () => {
  //   const action: Types.Action = {
  //     type: Types.FETCH_COMMIT_SUCCESS,
  //     commits: [commit]
  //   }
  //   expect(rootReducer({branch: [branchOld]}, action).branch).toEqual([branch])
  // })
})