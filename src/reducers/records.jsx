// @flow
import * as Types from "../types"

const records = (state: Array<Types.RecordsType> = [], action: Types.Action): Array<Types.RecordsType> => {
  let commitId, testName, recordSet, newRecords 
  switch (action.type) {
    case Types.FETCH_ITERATIONS_SUCCESS:
      commitId = action.commitId
      testName = action.testName
      newRecords = action.records
      recordSet = state.find(rs => rs.commitId === commitId && rs.testName === testName)
      if (!recordSet) {
        recordSet = {
          commitId,
          testName,
          testRecords: newRecords
        }
        state = state.concat(recordSet)
      }
      recordSet.testRecords = newRecords
      recordSet.testRecords.sort(function (c1, c2) { return c1.updatedMs > c2.updatedMs ? 1 : -1 })
      return state
    case Types.DELETE_TESTRECORD_SUCCESS: 
      let testRecord = action.testRecord
      commitId = testRecord.commitId
      testName = testRecord.testName
      recordSet = state.find(rs => rs.commitId === commitId && rs.testName === testName)
      if (recordSet) {
        recordSet.testRecords = recordSet.testRecords.filter(record => record._id !== testRecord._id)
        recordSet.testRecords.sort(function (c1, c2) { return c1.updatedMs > c2.updatedMs ? 1 : -1 })
      }
      return state
    default:
      return state
  }
}

/** Filter commit for TestRecordSet for specific testName and addstheespecific records from the payload to it. */
// function mergeRecords(newRecords: Array<Types.TestRecordType>, commit: Types.CommitType): void {
//   let testName = newRecords[0].testName
//   let testRecordSet = commit.testRecordSet.filter(recordSet => recordSet.testName == testName)[0]
//   testRecordSet.records = newRecords
// }

export default records
