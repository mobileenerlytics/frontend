// @flow
import * as Types from "../types"

export default function project(state: Array<Types.ProjectType> = [], action: Types.Action): Array<Types.ProjectType> {
  switch (action.type) {
    case Types.FETCH_PROJECTS_SUCCESS:
      return action.projects
    case Types.UPDATE_PROJECTS_SUCCESS:
      let updatedProject = action.project
      return state.map((project) => {
        if (project._id === updatedProject._id)
          return updatedProject
        return project
      })
    default:
      return state
  }
}



