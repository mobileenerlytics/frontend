// @flow
import * as Types from "../types"

/** Reducer for upload in state. Upload holds all the most recent uploads done to a specific account. 
 * Displayed in RecentActivity 
 */
const upload = (state: Array<Types.UploadType> = [], action: Types.Action): Array<Types.UploadType> => {
  switch (action.type) {
    case Types.FETCH_UPLOADS_SUCCESS:
      const newState = mergeStates(action.uploads, state)
			return newState.sort((a,b) => b.uploadMs - a.uploadMs)
    default:
      return state
  }
}

/**
 * Function to merge Redux state with Uploads passed in from fetch
 * 
 * @param {} newUploads - Array of fetched Uploads to add to state
 * @param {*} uploads - previous state of Uploads
 * 
 * @returns - New merged state of newUploads with uploads
 */
function mergeStates(newUploads: Array<Types.UploadType>, uploads: Array<Types.UploadType>): Array<Types.UploadType> {
    let newUploadIds = newUploads.map(upload => upload._id)
    uploads = uploads.filter(upload => !newUploadIds.includes(upload._id))
    uploads = uploads.concat(newUploads)
    uploads.sort(function (u1, u2) { return u1.uploadMs > u2.uploadMs? -1 : 1})	
    return uploads
}

export default upload
