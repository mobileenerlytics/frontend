import * as actions from "../actions";
import rootReducer from "./index";

describe('redux', () => {
  it('state should not be undefined', () => {
    const branches = []
    const action = {
      type: actions.FETCH_BRANCHES_SUCCESS,
      payload: branches
    }
    expect(rootReducer(undefined, action)).not.toBeUndefined()
  })
})