// @flow
import * as Types from "../types"

const recordStatus = (state: Types.StatusType = Types.STATUS_SUCCESS, action: Types.Action): Types.StatusType => {
    switch (action.type) {
        case Types.FETCH_ITERATIONS_START:
            return Types.STATUS_LOADING
        case Types.FETCH_ITERATIONS_SUCCESS:
            return Types.STATUS_SUCCESS
        case Types.FETCH_ITERATIONS_FAILURE:
            return Types.STATUS_FAILURE
        case Types.DELETE_TESTRECORD_SUCCESS:
            return Types.STATUS_SUCCESS
        default:
            return state
    }
}

export default recordStatus
