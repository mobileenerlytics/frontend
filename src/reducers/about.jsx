// @flow
import * as Types from '../types'

export default function about(state: Types.AboutType = {version: null}, action: Types.Action): Types.AboutType {
  switch (action.type) {
    case Types.FETCH_ABOUT_SUCCESS:
      return action.about
    default:
      return state
  }
}