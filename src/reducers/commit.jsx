// @flow
import * as Types from "../types"

const commit = (state: Array<Types.CommitType> = [], action: Types.Action): Array<Types.CommitType> => {
  let commitId
  switch (action.type) {
    case Types.DELETE_COMMIT_SUCCESS: 
      commitId = action.commitId
      return state.filter(c => c._id !== commitId)
    case Types.FETCH_COMMIT_SUCCESS:
      format(action.commits)
      const newState = mergeStates(action.commits, state)
			return newState.sort((a,b) => b.updatedMs - a.updatedMs)
    default:
      return state
  }
}

function mergeStates(newCommits: Array<Types.CommitType>, commits: Array<Types.CommitType>): Array<Types.CommitType> {
	let newCommitsId = newCommits.map(commit => commit._id)
  commits = commits.filter(commit => !newCommitsId.includes(commit._id))
  commits = commits.concat(newCommits)
  commits.sort(function (c1, c2) { return c1.updatedMs > c2.updatedMs? -1 : 1})	
  return commits
}

function format(commits: Array<Types.CommitType>): void {
  commits.forEach(commit => {
    commit.testRecordSet && (commit.testRecordSet.forEach(t => {
      t.energy = parseFloat(t.energy.toFixed(2))
      t.energySquared = parseFloat(t.energySquared.toFixed(2))
      
      t.threadCompEnergies && t.threadCompEnergies.forEach(tce => {
        tce.energy = parseFloat(tce.energy.toFixed(2))
        tce.energySquared = parseFloat(tce.energySquared.toFixed(2))
      })
      t.componentUnit && t.componentUnit.forEach(cu => {
        cu.energy = parseFloat(cu.energy.toFixed(2))
        cu.energySquared = parseFloat(cu.energySquared.toFixed(2))
      })
      t.threadUnit && t.threadUnit.forEach(tu => {
        tu.energy = parseFloat(tu.energy.toFixed(2))
        tu.energySquared = parseFloat(tu.energySquared.toFixed(2))
      })
      t.events && t.events.forEach(e => {
        e.durationSec = parseFloat(e.durationSec.toFixed(2))
        e.durationSecSquared = parseFloat(e.durationSecSquared.toFixed(2))
      })
    }))
  })
}

export default commit
