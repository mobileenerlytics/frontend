// @flow
import * as Types from "../types"

const initialState: Types.CommonType = {
  curProjectIndex : 0, // current project index
  projectId : null
}

export default function common (state: Types.CommonType = initialState, action: Types.Action): Types.CommonType {
  switch (action.type) {
    case Types.FETCH_PROJECTS_SUCCESS: 
      return {
        ...state,
        projectId : action.projects[state.curProjectIndex]._id
      }
    case Types.UPDATE_PROJECTS_SUCCESS: 
      return {
        ...state,
        curProjectIndex : 0,
      }
    default:
      return state
  }
}