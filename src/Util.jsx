// @flow
import * as Types from "./types"
import { mainColors } from "./colors"
import _ from "underscore"

/** 
 * This function is used to extract GET params from URL. 
 * For example, if the current URL is https://beta.mobileenerlytics.com/commit?count=10,
 * calling this function with {@link searchTerm}=count will return 10.
 * @param search 
 * @param searchTerm key that needs to be searched
 * @param defaultValue value returned if key is not found in URL
 */
export function extractFromURL(search: URLSearchParams, searchTerm: string, defaultValue: string): string {
	if (!search.has(searchTerm))
		return defaultValue
	const value = search.get(searchTerm)
	if (value == null)
		return defaultValue
	return value
}

export function extractTestNamesFromBranch(branch: Types.BranchType): string[] {
	return branch.commits.reduce((names, commit) => {
		let testNames = extractTestNames(commit)
		names = names.concat(testNames)
		return names;
	}, [])
}

export function extractTestNames(commit: Types.CommitType): string[] {
	if (commit.testRecordSet == null) return [];
	return commit.testRecordSet.reduce((names, testRecord) => {
		if (!names.includes(testRecord.testName)) names.push(testRecord.testName)
		return names;
	}, [])
}


export function findTestRecord(commit: Types.CommitType, testName: string): ?Types.TestRecordType {
	return commit.testRecordSet.find(record => record.testName === testName);
}

export function extractBranches(branches: Array<Types.BranchType>, prefix: string): Array<Types.BranchType> {
	return branches.filter(branch => branch.branchName.startsWith(prefix));
}

export function extractBranch(branches: Array<Types.BranchType>, branchName: string): ?Types.BranchType {
	return branches.find(branch => branch.branchName === branchName);
}

export function extractTestNamesFromCommit(commit: Types.CommitType): Set<string> {
	return new Set([...commit.testRecordSet.map(record => record.testName)])
}

/* Retrieve n commits from the given branch */
export function getNCommits(allCommits: Array<Types.CommitType>, branchName: string, n: number): Array<Types.CommitType> {
	if(n <= 0) return []
	let selectedCommits = allCommits.filter((commit) => commit.branchName === branchName)
	selectedCommits = selectedCommits.slice(0, n)
	selectedCommits = selectedCommits.reverse()
	return selectedCommits
}

/**
 * Function to sum together distributions of tests from a branch. Because the tests may be 
 * correlated and Covariance cannot be precisely computed, the maximum covariance possible is
 * assumed when creating the variance of the combined variable of the sample test distributions. 
 * var(x + y) = var(x) + var(y) + 2*Cov(x, y)
 * Cov(x, y) <= sigma(x)*sigma(y)
 * 
 * for power: 
 * E[x] = duration1 * power1 + duration2 * power2 + .... + durationn * powern / (duration1 + duration2 + ... + durationn)
 * var(x) = E[(t1 * p1/ (t1 + t2))^2] - E[(t1 * p1) / (t1 + t2)]^2
 * var(x) = [t1 / (t1 + t2)]^2 * (E[p1^2] - E[p1]^2)
 * var(x) = [t1 / (t1 + t2)]^2 * var(p1)
 * var(x + y) <= (sigma(x) + sigma(y))^2
 * var(x + y) <= [(t1 * sigma(x) + t2 * sigma(y))/(t1 + t2)]^2
 * 
 * The tests may also have different n values. Currently we take the minimum of the n's to use in 
 * the combined distribution, but this is perhaps over compensating: introducing a new test with a 
 * low count will ruin results of all other tests. 
 * 
 * Returns an array with an entry for every metric possible to summarize. 
 * Current metrics: [energy, power]
 * 
 * @param {Array<Types.TestRecordType>} records: Array of Records with sample distribution data 
 * to accumulate into a single distribution to be compared with another branch
 */
export function sumEnergy(records: Array<Types.TestRecordType>): [Types.StatsInfo, Types.StatsInfo] {
	let deviationSum = 0
	let powerDeviationSum = 0
	let powerSum = 0
	let totalDuration = 0

	let count = records[0].count
	return records.reduce(
		function (acc, record) {
			let recordVariance = record.energySquared - record.energy*record.energy
			let recordDeviation = Math.sqrt(recordVariance)
			let mean = acc[0].mean + record.energy
			let variance = acc[0].variance + 2*deviationSum*recordDeviation + recordVariance
			count = record.count < count? record.count : count
			deviationSum = deviationSum + recordDeviation

			totalDuration += record.testDurationMS
			let powerDeviation = Math.sqrt(record.powerSquared - record.power * record.power)
			powerDeviationSum += record.testDurationMS * powerDeviation
			powerSum += record.testDurationMS * record.power

			return [{mean, variance, count}, {mean: powerSum / totalDuration, variance: Math.pow(powerDeviationSum / totalDuration, 2) , count}]
		}, [{mean: 0, variance: 0, count: 0}, {mean: 0, variance: 0, count: 0}]
	);
}

/**
 * Aggregates energy from Units and returns a map using unit name as the key.
 * @param {Array<Types.UnitType>} units input units that need to be aggregate. 
 * Example: [{name: CPU, energy: 10}, {name: GPU, energy: 20}, {name: CPU, energy: 30}]
 * @return an array of aggregated units of [energy, power]. Example: [{CPU: 40, GPU: 20}, {CPU: 3, GPU: 1}]
 */
export function aggregateUnits(data: Array<{unit: Types.UnitType, duration: number}>, count: number, totalDuration: number): [Map<string, Types.StatsInfo>, Map<string, Types.StatsInfo>] {
	let deviationsMap: Map<string, number> = new Map()
	let powerDeviationSums = new Map()
	let powerMap = new Map()
	data.forEach(unitData => {
		let prevEntry = getOrElse(powerDeviationSums, unitData.unit.name, {powerSum: 0, powerDeviationSum: 0})
		prevEntry.powerSum += unitData.unit.power * unitData.duration
		let powerDeviation = Math.sqrt(unitData.unit.powerSquared - Math.pow(unitData.unit.power, 2))
		prevEntry.powerDeviationSum += unitData.duration * powerDeviation
		powerDeviationSums.set(unitData.unit.name, prevEntry)
		powerMap.set(unitData.unit.name, {mean: prevEntry.powerSum / totalDuration, variance: Math.pow(prevEntry.powerDeviationSum / totalDuration, 2), count})
	})

	let energyMap = data.reduce(
		function (acc, unitData) {
			let unitVariance = unitData.unit.energySquared - unitData.unit.energy*unitData.unit.energy
			if (unitVariance < 0) unitVariance = 0
			let unitDeviation = Math.sqrt(unitVariance)
			// need to add all covariance terms <= sigma_x * sigma_y 
			// for each new component from a test that we add, we can take our previous sum, add this simga
			// and multiply by 2 * this sigma. This will account for adding new variance and covariance terms. 
			// Var(x + y) = var(x) + var(y) + 2cov(x, y)
			let deviationSum = getOrElse(deviationsMap, unitData.unit.name, 0)
			let variance = 2 * deviationSum * unitDeviation + unitVariance
			deviationsMap.set(unitData.unit.name, deviationSum + unitDeviation)
			let mean = unitData.unit.energy
			if (acc.has(unitData.unit.name)) {
				// distribution of current summed components
				let statInfo = getOrElse(acc, unitData.unit.name, { mean: 0, variance: 0, count: 0 })
				variance += statInfo.variance
				mean += statInfo.mean
			}
			let statsInfo: Types.StatsInfo = {
				mean,
				variance,
				count
			}
			acc.set(unitData.unit.name, statsInfo)
			return acc;
		},
		new Map());

		return [energyMap, powerMap]
}

/** Returns the smallest count present in a set of records */
function getSmallestCount(records: Array<Types.TestRecordType>): number {
	return records.reduce(
		function (count, r) {
			if (r.count < count)
				return r.count
			return count
		}, records[0].count)
}

/** Return the total test duration of all records */
function getTotalDuration(records: Array<Types.TestRecordType>): number {
	return records.reduce(
		function (totalDuration, r) {
			return totalDuration + r.testDurationMS
		}, 0)
}

export function sumComponents(records: Array<Types.TestRecordType>): [Map<string, Types.StatsInfo>, Map<string, Types.StatsInfo>] {
	let count = getSmallestCount(records)
	let totalDuration = getTotalDuration(records)
	return aggregateUnits(_.flatten(records.map(r => r.componentUnit.map(unit => {return {unit, duration: r.testDurationMS}}))), count, totalDuration)
}

export function sumThreads(records: Array<Types.TestRecordType>): [Map<string, Types.StatsInfo>, Map<string, Types.StatsInfo>] {
	let count = getSmallestCount(records)
	let totalDuration = getTotalDuration(records)
	return aggregateUnits(_.flatten(records.map(r => r.threadUnit.map(threadUnit => {return {unit: threadUnit, duration: r.testDurationMS}}))), count, totalDuration)
}

function extractVersions(release: Types.BranchType): string[] {
	let branchName = release.branchName;
	let i = branchName.length;
	while (i !== 0) {
		let c = branchName[i - 1];
		if (c === '.' || (c >= '0' && c <= '9')) i--;
		else break;
	}
	let partOfversions = branchName.substring(i, branchName.length);
	let versions = partOfversions.split('.');
	return versions;
}

export function sortReleases(releases: Array<Types.BranchType>): void {
	releases.sort(function (r1, r2) {
		let v1 = extractVersions(r1);
		let v2 = extractVersions(r2);
		let i = 0;
		let ml = Math.max(v1.length, v2.length);
		while (1 < ml) {
			if (i >= v1.length) return -1;
			if (i >= v2.length) return 1;
			if (v1[i] !== v2[i]) return parseInt(v1[i]) - parseInt(v2[i]);
			else i++;
		}
		// without version info, we will sort by createdTime.
		return r1.updatedMs - r2.updatedMs;
	})
}

export function msToTime(duration: number): ?string {
	if (duration == null) return null;
	var seconds = parseInt((duration / 1000) % 60)
		, minutes = parseInt((duration / (1000 * 60)) % 60)
		, hours = parseInt((duration / (1000 * 60 * 60)) % 24);

	hours = hours === 0 ? "" : hours + "h ";
	minutes = minutes + "m ";
	seconds = seconds + "s";

	return hours + minutes + seconds;
}

const idToColorMap: Map<string, Types.ColorType> = new Map()
let index: number = 0;
export function getRandomRGB(id: string, shade: ?Types.ShadeType = 500): Types.ColorType {
	id = String(id).toLowerCase();
	if (!idToColorMap.has(id)) {
		if (index === mainColors.length) index = 0
		idToColorMap.set(id, mainColors[index++])
	}
	return getOrElse(idToColorMap, id, mainColors[0])[shade]
}

export function findCommitById(commits: Array<Types.CommitType>, commitId: string): ?Types.CommitType {
	return commits.find(commit => commit._id === commitId)
}

/**
 * This is a helper function to get around flow warnings whenever we use map.get(key). map.get(key) always
 * returns a value of type ?V even if the key presence has been checked with map.has(key).
 */
export function getOrElse<K, V>(map: Map<K, V>, key: K, defaultValue: V): V {
	const v = map.get(key)
	return v ? v : defaultValue
}
