import React from "react"
import {Route } from "react-router-dom"
import AnalysisContainer from "../containers/analysis/AnalysisContainer.jsx"
import SettingsContainer from "../containers/settings/SettingsContainer.jsx"
const RoutesManualUser = () => (
  <div>    
    <Route path="/analysis" render={AnalysisContainer} />
    <Route path="/settings" render={SettingsContainer} />
    <Route path="/" exact render={AnalysisContainer} />
  </div>
)
export default RoutesManualUser
