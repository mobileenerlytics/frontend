import React from "react";
import {Route } from "react-router-dom";
import ReleaseContainer from "../containers/release/ReleaseContainer";
import CommitContainer from "../containers/commit/CommitContainer.jsx";
import AnalysisContainer from "../containers/analysis/AnalysisContainer.jsx";
import OverviewContainer from "../containers/overview/OverviewContainer.jsx";
import SettingsContainer from "../containers/settings/SettingsContainer.jsx";
// import IterationsContainer from "../containers/iterations/IterationsContainer";

const Routes = () => (
  <div>    
    {/* <Route path="/" exact render={OverviewContainer}/> */}
    <Route path="/release" render={ReleaseContainer} />
    <Route path="/commit" render={CommitContainer} />
    <Route path="/analysis" render={AnalysisContainer} />
    <Route path="/settings" render={SettingsContainer} />
    {/* todo change it to error page */}
    <Route path="/" exact render={OverviewContainer} />  
  </div>
)
export default Routes
