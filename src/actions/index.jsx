// @flow
import * as Types from "../types"

export function updateProjectSuccess(project: Object): Types.Action {
  return {
    type: Types.UPDATE_PROJECTS_SUCCESS,
    project: Types.toProjectType(project)
  }
}

export function fetchProjectSuccess(projects: Object): Types.Action {
  return {
    type: Types.FETCH_PROJECTS_SUCCESS,
    projects: projects.map(p => Types.toProjectType(p))
  }
}

export function fetchProfileSuccess(profile: Object): Types.Action {
  return {
    type: Types.FETCH_PROFILE_SUCCESS,
    profile: Types.toProfileType(profile)
  }
}

export function fetchBranchesSuccess(branches: Object): Types.Action {
  return {
    type: Types.FETCH_BRANCHES_SUCCESS,
    branches: branches.map(b => Types.toBranchType(b))
  }
}

export function fetchCommitsSuccess(commits: Object): Types.Action {
  return {
    type: Types.FETCH_COMMIT_SUCCESS,
    commits: commits.map(c => Types.toCommitType(c))
  }
}

export function startCommitFetch(): Types.Action {
  return {
    type: Types.FETCH_COMMIT_START
  }
}

export function commitFetchFailure(): Types.Action {
  return {
    type: Types.FETCH_COMMIT_FAILURE
  }
}

export function deleteCommitsSuccess(commitId: string): Types.Action {
  return {
    type: Types.DELETE_COMMIT_SUCCESS,
    commitId
  }
}

export function reprocessUploadSuccess(commitId: string): Types.Action {
  return {
    type: Types.REPROCESS_UPLOAD_SUCCESS, 
    commitId
  }
}

export function fetchIterationsSuccess(records: Array<Types.TestRecordType>, commit: Types.CommitType, testName: string): Types.Action {
  return {
    type: Types.FETCH_ITERATIONS_SUCCESS,
    records: records.map(r => Types.toTestRecordType(r, commit._id, commit.hash)),
    commitId: commit._id,
    testName,
  }
}

export function startIterationFetch(): Types.Action {
  return {
    type: Types.FETCH_ITERATIONS_START
  }
}

export function fetchIterationFailure(): Types.Action {
  return {
    type: Types.FETCH_ITERATIONS_FAILURE
  }
}

export function fetchAboutSuccess(about: Object): Types.Action {
  return {
    type: Types.FETCH_ABOUT_SUCCESS,
    about: Types.toAboutType(about)
  }
}

export function fetchUploadsSuccess(uploads: Array<Types.UploadType>): Types.Action {
  return {
    type: Types.FETCH_UPLOADS_SUCCESS,
    uploads: uploads.map(u => Types.toUploadType(u))
  }
}

export function deleteTestRecordSuccess(testRecord: Types.TestRecordType): Types.Action {
  return {
    type: Types.DELETE_TESTRECORD_SUCCESS,
    testRecord
  }
}

export function openDrawer(drawer: boolean): Types.Action {
  return {
    type: Types.OPEN_DRAWER,
    drawer
  }
}

const getInit = {
  method: "GET",
  credentials: "same-origin",
  headers: {
    "Content-Type": "application/json"
  }
}

const putInit = {
  method: "PUT",
  credentials: "same-origin"
}

const deleteInit = {
  method: "DELETE",
  credentials: "same-origin"
}

/*
about
*/
export function fetchAboutStart(): Types.ThunkAction {
  return (dispatch: Types.Dispatch) => {
    const url = `/api/about`
    return fetch(url, getInit)
      .then(res => {
        if (res.status !== 200) throw Error("fetch about failed:  " + res.statusText)
        return res.json()
      })
      .then(json => {
        dispatch(fetchAboutSuccess(json))
      })
      .catch(error => console.log(error))
  }
}

/*
  widget updates
*/
export function updateWidget(index: number, widgetProps: Object, callback?: (boolean, ?Error) => void): Types.ThunkAction {
  let widget = Types.toWidgetType(widgetProps)
  return (dispatch: Types.Dispatch, getState: Types.GetState) => {
    let projectId = getState().common.projectId
    if (!projectId) {
      console.log("Project id not set. Can't call API")
      return
    }
    let url =`/api/projects/${projectId}/widget/${index}?type=${widget.type}&firstBranch=${widget.firstBranchName}&secondBranch=${widget.secondBranchName}&firstCommit=${widget.firstCommitHash}&secondCommit=${widget.secondCommitHash}&metric=${widget.metric}`
    let projectIndex = getState().common.curProjectIndex
    let project = getState().project[projectIndex]
    project.widgets[index] = widget
    fetch(url, putInit)
    .then(res => {
      if (res.status !== 200) throw Error("put project/widget failed: " + res.statusText)
    })
    .then(() => {
      dispatch(updateProjectSuccess(project))
    }).then (() => {
      if(callback) callback(true)
    })
    .catch(err => {
      console.log("error putting project/widget", err)
      if(callback) callback(false)
    })
  }
}

/* 
  reprocess uploads
*/
export function reprocessUploadStart(upload: Types.UploadType, callback?: (boolean, ?Error) => void): Types.ThunkAction {
return (dispatch: Types.Dispatch, getState: Types.GetState) => {
    let projectId = getState().common.projectId
    if (!projectId) {
      console.log("Project id not set. Can't call API")
      return
    }
    let url = `api/upload/reprocess?upload_id=${upload._id}`
    return fetch(url, getInit)
      .then(res => {
        if (res.status !== 200) throw Error("reprocessing upload failed  " + res.statusText)
      })
      .then(() => {
        dispatch(reprocessUploadSuccess(upload._id))
      })
      .then(() => {
        if (callback) callback(true)
      })
      .catch(err => {
        console.log("error reprocessing upload", err)
        dispatch(fetchIterationFailure())
        if (callback) callback(false, err)
      })
  }
}

/*
  iterations
*/
export function fetchIterationsStart(commit: Types.CommitType, testName: string, count: number = 10, callback?: (boolean, ?Error) => void): Types.ThunkAction {
  return (dispatch: Types.Dispatch) => {
    let url = `/api/records?commitId=${commit._id}&testName=${testName}&count=${count}`
    dispatch(startIterationFetch())
    return fetch(url, getInit)
      .then(res => {
        if (res.status !== 200) throw Error("fetch test iterations failed:  " + res.statusText)
        return res.json()
      })
      .then(res => {
        dispatch(fetchIterationsSuccess(res, commit, testName))
      })
      .then(() => {
        if (callback) callback(true)
      })
      .catch(err => {
        console.log("error fetching test iterations", err)
        dispatch(fetchIterationFailure())
        if (callback) callback(false, err)
      })
  }
}

export function deleteTestRecordStart(testRecord: Types.TestRecordType, callback?: (boolean, ?Error) => void): Types.ThunkAction {
return (dispatch: Types.Dispatch, getState: Types.GetState) => {
    let projectId = getState().common.projectId
    if (!projectId) {
      console.log("Project id not set. Can't call API")
      return
    }
    // console.log("fetchCommitsStart: " + query)
    let url = `api/records/${projectId}/?testRecordId=${testRecord._id}`
    dispatch(startIterationFetch())
    return fetch(url, deleteInit)
      .then(res => {
        if (res.status !== 200) throw Error("deleting test record failed  " + res.statusText)
      })
      .then(() => {
        dispatch(deleteTestRecordSuccess(testRecord))
      })
      .then(() => {
        if (callback) callback(true)
      })
      .catch(err => {
        console.log("error deleting test record", err)
        dispatch(fetchIterationFailure())
        if (callback) callback(false, err)
      })
  }
}

/*
  commit
*/
export function fetchCommitsStart(count: number = 15, callback?: (boolean, ?Error) => void, branchName: string = "", midHash: string = "", hashPrefix: string = ""): Types.ThunkAction {
  return (dispatch: Types.Dispatch, getState: Types.GetState) => {
    let projectId = getState().common.projectId
    if (!projectId) {
      console.log("Project id not set. Can't call API")
      return
    }
    let urlExtension = ""
    if(hashPrefix !== "")
      urlExtension = urlExtension + `&hashPrefix=${hashPrefix}`
    if(midHash !== "")
      urlExtension = urlExtension + `&midHash=${midHash}`
    let url = `api/commits?projectId=${projectId}&count=${count}&branchName=${branchName}` + urlExtension
    dispatch(startCommitFetch())
    return fetch(url, getInit)
      .then(res => {
        if (res.status !== 200) throw Error("fetch commits failed:  " + res.statusText)
        return res.json()
      })
      .then(commits => {
        dispatch(fetchCommitsSuccess(commits))
      })
      .then(() => {
        if (callback) callback(true)
      })
      .catch(err => {
        console.log("error fetching commits", err)
        dispatch(commitFetchFailure())
        if (callback) callback(false, err)
      })
  }
}

/**
 * API to fetch uploads from server
 * 
 * @param {number} count - number of recent uploads to fetch
 */
export function fetchUploadsStart(count: number = 50): Types.ThunkAction {
  return (dispatch: Types.Dispatch, getState: Types.GetState) => {
    let projectId = getState().common.projectId
    if (!projectId) {
      console.log("Project id not set. Can't call API")
      return
    }
    let url = `api/upload?projectId=${projectId}&count=${count}`
    return fetch(url, getInit)
      .then(res => {
        if (res.status !== 200) throw Error("fetch uploads failed: " + res.statusText)
        return res.json()
      })
      .then(uploads => {
        dispatch(fetchUploadsSuccess(uploads))
      })
      .catch(error => {
        console.log("error fetching uploads", error)
      })
  }
}

export function deleteCommitsStart(commitId: string): Types.ThunkAction {
  return (dispatch: Types.Dispatch, getState: Types.GetState) => {
    let projectId = getState().common.projectId
    if (!projectId) {
      console.log("Project id not set. Can't call API")
      return
    }
    console.log("deleteCommitsStart: " + commitId)
    let url = `api/commits/${commitId}/?projectId=${projectId}`
    const request = {
      method: "DELETE",
      credentials: "same-origin"
    }

    return fetch(url, request)
      .then(res => {
        if (res.status !== 202) throw Error("delete commits failed:  " + res.statusText)
        return res
      })
      .then(() => {
        dispatch(deleteCommitsSuccess(commitId))
      })
      .catch(err => {
        console.log("error deleting commits", err)
      })
  }
}

/*
branch
*/
export function fetchBranchesStart(prefix: string = '', branchName: string = '', count: number = 1): Types.ThunkAction {
  // Do API call, merge with ALL_BRANCHES redux store.

  return (dispatch: Types.Dispatch, getState: Types.GetState) => {
    let projectId = getState().common.projectId
    if (!projectId) {
      console.log("Project id not set. Can't call API")
      return
    }
    prefix = encodeURIComponent(prefix)
    branchName = encodeURIComponent(branchName)
    let url = `/api/branches?count=${count}&prefix=${prefix}&projectId=${projectId}`
    if (branchName !== '')
      url = `/api/branches?count=${count}&branchName=${branchName}&projectId=${projectId}`
    return fetch(url, getInit)
      .then(response => {
        return response.text()
      })
      .then(resText => {
        return JSON.parse(resText)
      })
      .then(branches => {
        dispatch(fetchBranchesSuccess(branches))
      })
      .catch(err => {
        console.log("error fetching all branches", err)
        dispatch(fetchBranchesSuccess([]))
      })
  }
}

/*
profile
*/
export function fetchProfileStart(): Types.ThunkAction {
  return (dispatch: Types.Dispatch) => {
    const url = `/api/profile`
    return fetch(url, getInit)
      .then(res => {
        if (res.status !== 200) throw Error("fetch profile failed:  " + res.statusText)
        return res.json()
      })
      .then(profile => {
        dispatch(fetchProfileSuccess(profile))
      })
      .catch(error => {
        console.log(error)
      })
  }
}


/*
project
*/
export function fetchProjectStart(): Types.ThunkAction {
  return (dispatch: Types.Dispatch) => {
    let url = `api/projects`

    return fetch(url, getInit)
      .then(res => {
        if (res.status !== 200) throw Error("fetch projects failed:  " + res.statusText)
        return res.json()
      })
      .then(projects => {
        dispatch(fetchProjectSuccess(projects))
      })
      .catch(error => console.log(error))
  }
}

export function updateProjectStart(attributeName: string, attributeValue: string, projectId: string, callback?: (boolean, ?Error) => void): Types.ThunkAction {
  return (dispatch: Types.Dispatch) => {
    // const url = `api/projects/${projectId}/${attributeName}/${attributeValue}`
    const url = `api/projects/${projectId}/${attributeName}/?value=${attributeValue}`
    const call = {
      method: "PUT",
      credentials: "same-origin"
    }

    return fetch(url, call)
      .then(res => {
        if (res.status !== 200) throw Error("update Project failed:  " + res.statusText)
        return res.json()
      })
      .then(updatedProject => {
        dispatch(updateProjectSuccess(updatedProject))
          .then(() => {
            if (callback) callback(true)
          })
      })
      .catch(error => {
        console.log(error)
        if (callback) callback(false, error)
      })
  }
}
