import { Chart } from 'chart.js';

const defaultOptions = {
  /**
   * stroke color
   * @default: derived from borderColor
   */
  color: undefined,

  /**
   * width as number, or as string with pixel (px) ending, or as string with percentage (%) ending
   */
  width: 10,

  /**
   * lineWidth as number, or as string with pixel (px) ending
   */
  lineWidth: 2,

  /**
   * whether the error values are given in absolute values or relative (default)
   */
  absoluteValues: false
};

const ErrorBarsPlugin = {
  id: 'chartJsPluginErrorBars',

  /**
   * get original barchart base bar coords
   * @param chart chartjs instance
   * @returns {Array} containing label, x, y and color
   * @private
   */
  _getBarchartBaseCoords: function _getBarchartBaseCoords(chart) {
    var coords = [];
    chart.data.datasets.forEach(function (d, i) {
      var bars = chart.getDatasetMeta(i).data;
      var values = d.data;
      var visible = d.visible;
      var barLabels = d.xLabels
      let barcoords = bars.map(function (b, j) {

        // line charts do not have labels in their meta data, access global label array instead
        // var barLabel = '';
        // if (!b._model.label) {
        //   barLabel = chart.data.labels[j];
        // } else {
        //   barLabel = b._model.label; // required for hierarchical
        // }
        return {
          label: barLabels[j],
          value: values[j],
          x: b._model.x,
          y: b._model.y,
          color: b._model.borderColor,
        };
      });
      coords.push({visible: visible, barcoords: barcoords})
    });
    return coords;
  },


  /**
   * check whether the chart orientation is horizontal
   * @param chart chartjs instance
   * @returns {boolean}
   * @private
   */
  _isHorizontal: function _isHorizontal(chart) {
    return chart.config.type === 'horizontalBar';
  },


  /**
   * compute error bars width in pixel or percent
   * @param chart chartjs instance
   * @param horizontal orientation
   * @param options plugin options
   * @returns {*} width in pixel as number
   * @private
   */
  _computeWidth: function _computeWidth(chart, horizontal, options) {
    var width = options.width;
    var widthInPx = width;

    try {

      if (typeof width === 'string') {
        if (width.match(/px/)) {
          widthInPx = parseInt(width.replace(/px/, ''), 10);
        } else {

          // handle percentage values: convert to positive number between 0 and 100
          var widthInPercent = Math.min(100, Math.abs(Number(width.replace(/%/, ''))));
          var model = chart.getDatasetMeta(0).data[0]._model;

          if (chart.config.type === 'line') {
            widthInPx = parseInt(model.controlPointPreviousX + model.controlPointNextX, 10);
          } else if (horizontal) {
            widthInPx = parseInt(model.height, 10);
          } else if (!horizontal) {
            widthInPx = parseInt(model.width, 10);
          }

          widthInPx = widthInPercent / 100 * widthInPx;
        }
      }
    } catch (e) {
      console.error(e);
    } finally {
      if (Number.isNaN(widthInPx)) {
        widthInPx = options.width;
      }
    }
    return widthInPx;
  },


  /**
   * draw error bar mark
   * @param ctx canvas context
   * @param model bar base coords
   * @param plus positive error bar position
   * @param minus negative error bar position
   * @param color error bar stroke color
   * @param width error bar width in pixel
   * @param lineWidth error ber line width
   * @param horizontal orientation
   * @private
   */
  _drawErrorBar: function _drawErrorBar(ctx, model, plus, minus, color, width, lineWidth, horizontal) {
    ctx.save();
    ctx.lineWidth = lineWidth;
    ctx.strokeStyle = color;
    ctx.beginPath();
    if (horizontal) {
      ctx.moveTo(minus, model.y - width / 2);
      ctx.lineTo(minus, model.y + width / 2);
      ctx.moveTo(minus, model.y);
      ctx.lineTo(plus, model.y);
      ctx.moveTo(plus, model.y - width / 2);
      ctx.lineTo(plus, model.y + width / 2);
    } else {
      ctx.moveTo(model.x - width / 2, plus);
      ctx.lineTo(model.x + width / 2, plus);
      ctx.moveTo(model.x, plus);
      ctx.lineTo(model.x, minus);
      ctx.moveTo(model.x - width / 2, minus);
      ctx.lineTo(model.x + width / 2, minus);
    }
    ctx.stroke();
    ctx.restore();
  },


  /**
   * plugin hook to draw the error bars
   * @param chart chartjs instance
   * @param easingValue animation function
   * @param options plugin options
   */
  afterDraw: function afterDraw(chart, easingValue, options) {
    var _this = this;

    // wait for easing value to reach 1 at the first render, after that draw immediately
    chart.__renderedOnce = chart.__renderedOnce || easingValue === 1;

    if (!chart.__renderedOnce) {
      return;
    }

    options = Object.assign({}, defaultOptions, options);

    // error bar and barchart bar coords
    var errorBarCoords = chart.data.datasets.map(function (d) {
      return d.errorBars;
    });
    var barchartCoords = this._getBarchartBaseCoords(chart);

    if (!barchartCoords || !barchartCoords || !barchartCoords[0] || !barchartCoords[0].barcoords[0] || !errorBarCoords) {
      return;
    }

    // determine value scale and orientation (vertical or horizontal)
    var horizontal = this._isHorizontal(chart);
    var vScale = horizontal ? chart.scales['xAxis'] : chart.scales['yAxis'];

    var errorBarWidth = this._computeWidth(chart, horizontal, options);

    var ctx = chart.ctx;
    ctx.save();
    var stacked = chart.options.scales.yAxes[0].stacked
    let prev_values = new Array(chart.data.datasets[0].data.length).fill(0)

    // map error bar to barchart bar via label property
    barchartCoords.forEach(function (dataset, i) {
      dataset.barcoords.forEach(function (bar, j) {
        var cur = errorBarCoords[i];
        if (!cur || !dataset.visible || chart.getDatasetMeta(i).hidden != null) {
          return;
        }
        // var hasLabelProperty = cur.hasOwnProperty(bar.label);
        var errorBarData = null;

        // common scale such as categorical
        errorBarData = cur[bar.label]
        // if (hasLabelProperty) {
        //   errorBarData = cur[bar.label];
        // } else if (!hasLabelProperty && bar.label && bar.label.label && cur.hasOwnProperty(bar.label.label)) {
        //   // hierarchical scale has its label property nested in b.label object as b.label.label
        //   errorBarData = cur[bar.label.label];
        // }

        // error bar data for the barchart bar or point in linechart
        if (errorBarData) {
          var errorBarColor = options.color ? options.color : bar.color;
          var value = vScale.getRightValue(bar.value);
          if (stacked) {
            value += prev_values[j]
            prev_values[j] = value
          }

          var plusValue = options.absoluteValues ? Math.abs(errorBarData.plus) : value + Math.abs(errorBarData.plus);
          var minusValue = options.absoluteValues ? Math.abs(errorBarData.minus) : value - Math.abs(errorBarData.minus);

          var plus = vScale.getPixelForValue(plusValue);
          var minus = vScale.getPixelForValue(minusValue);

          if (errorBarData.plus >= 0)
            _this._drawErrorBar(ctx, bar, plus, minus, errorBarColor, errorBarWidth, options.lineWidth, horizontal);
        }
      });
    });

    ctx.restore();
  }
};

Chart.register(ErrorBarsPlugin);

export default ErrorBarsPlugin;
