import React from "react"
import ReactDOM from "react-dom"
import 'bootstrap/dist/css/bootstrap.min.css'
// import 'jquery-ui-bundle/jquery-ui.min.css'
import "./styles/App.scss"
import "./styles/loader.scss"
import App from "./App"
import { unregister } from "./registerServiceWorker"
import { ThemeProvider, createTheme } from "@material-ui/core/styles"
import { Provider } from "react-redux"
import { BrowserRouter as Router } from "react-router-dom"
import configureStore from "./store/configureStore"
import METheme from "./theme"
//import darkBaseTheme from 'material-ui/styles/baseThemes/darkBaseTheme'
import {Switch, Route} from "react-router-dom"

const store = configureStore();
const baseTheme = createTheme({
  palette: {
    type: 'light',
  },
});

const WrappedApp = () => (
  <ThemeProvider theme={METheme(baseTheme)}>
    <App />
  </ThemeProvider>
);

const RouteApp = () => (
  <Switch>
    <Route component={WrappedApp} />
  </Switch>
);

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <RouteApp />
    </Router>
  </Provider>,
   document.getElementById("root")
);
unregister();
