// @flow
import React from "react"
import Navbar from "./components/Navbar"
import Routes from "./routes/Routes"
import RoutesManualUser from "./routes/RoutesManualUser"
import {withRouter} from "react-router-dom"
import { connect } from "react-redux"
import { bindActionCreators } from "redux"
import * as Actions from "./actions"
import Loader from './components/Loader'
import * as Types from "./types"

type OwnProps = { }
type Props = {
  ...OwnProps,
  ...Types.ReduxProps<typeof mapStateToProps, typeof mapDispatchToProps>
}
type State = {
  appStatus: boolean
}

class App extends React.Component<Props, State> {
  constructor(props, state) {
    super(props, state)
    this.state = {
      appStatus : false
    }
  }
  componentDidMount() {
    const context = this

    const initialize = async () => {
      await context.props.actions.fetchProjectStart()
      await context.props.actions.fetchBranchesStart()
      await context.props.actions.fetchProfileStart()
      context.props.actions.fetchAboutStart()
      context.props.actions.fetchUploadsStart(15)
      this.setState({appStatus: true})
    }
    
    initialize().catch((err: Error) => console.error(err))
  }

  redirect() {
    setTimeout(function() {
      let url = "https://tester.mobileenerlytics.com/instructions.html"
      console.log("redirecting " + url)
      let win = window.open(url, '_blank')
      win.focus()}, 5000) 
  }

  render() {
    const profile: Types.ProfileType = this.props.profile
    let {role} = profile
    let content 
    if (this.state.appStatus) {
      if (!this.props.isDataAvaliable) {
        this.redirect()
        content = (
          <div>          
          <p>
          Hmm.. looks like you have not updated any test results yet. You will be redirected to getting started page in 5 seconds.
          </p>
        </div>
        )
      } else {
        content = (
          <div className="App">
            <Navbar />
            {role !== "manual" ? (<Routes />) : (<RoutesManualUser />) }            
          </div>
        )
      }
    } else {
      content = (<div className="App">
          <Loader /> 
      </div>)
    }

    return (      
      <div>
        {content}
      </div>
    )
  }
}

const mapStateToProps = (state: Types.State) => {
  return {
    isDataAvaliable: state.branch != null,
    profile: state.profile,
  }
}

const mapDispatchToProps = (dispatch: *) => {
  return {
    actions: bindActionCreators(Actions, dispatch)
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App))
