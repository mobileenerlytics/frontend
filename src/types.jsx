// @flow
import {mainColors, shades} from './colors'
import type { ReducersType } from './reducers';

/**
 * Verifies that the input object indeed contains all the properties
 * @param o Object that needs to be tested
 * @param props All the properties that need to be tested in the object
 * @throws Error if a property is not found in the object
 */
function hasAllProperties(o: Object, props: Array<string>): void {
  props.forEach(prop => {
    if(!o.hasOwnProperty(prop))
      throw new Error(prop + " not found")
  })
}

/** Types stored in redux store for commitStatus and recordStatus, updated after fetchAPI calls. */
export const STATUS_LOADING = "STATUS_LOADING"
export const STATUS_SUCCESS = "STATUS_SUCCESS"
export const STATUS_FAILURE = "STATUS_FAILURE"
export type StatusType =  "STATUS_LOADING" | "STATUS_SUCCESS" | "STATUS_FAILURE"

/** Upload types for Recent Activity. */
export type UploadType = {
  _id: string,
  hash: string,
  branchName: string,
  authorName: string,
  authorEmail: string,
  uploadMs: number,
  jobDurationMs: number,
  jobStatus: string,
}

/* Type used to describe the properties of a widget */ 
export type WidgetType = {
  type: string,
  firstBranchName: string,
  secondBranchName: string,
  firstCommitHash: string,
  secondCommitHash: string,
  metric: string
}

/** Data types returned from API and managed by Redux reducers */
export type ContributorType = {
  email: string,
  name: string
}

export type ThreadCompEnergiesType = {
  threadId: string,
  componentId: string,
  energy: number,
  energySquared: number,
}

export type UnitType = {
  processName: ?string,
  name: string,
  energy: number,
  energySquared: number,
  power: number,
  powerSquared: number
}

export type EventsType = {
  processName: ?string,
  eventName: string,
  name: string,
  durationSec: number,
  durationSecSquared: number
}

export type ProfileType = {
  email: string,
  username: string,
  role: "manual" | "default" | "demo",
  firstname: string,
  lastname: string,
  organization: string,
  registrationTime: ?number,
  expirationTime: ?number,
}

export type ProjectType = {
  _id: string,
  prefix: string,
  commitUrlPrefix: string,
  tourStatus: boolean,
  widgets: Array<WidgetType>
}

export type CommonType = {
  curProjectIndex: number,
  projectId: ?string
}

export type AboutType = {
  version: ?string
}

export type TestRecordType = {
  _id: string,
  count: number,
  energy: number,
  energySquared: number,
  power: number,
  powerSquared: number,
  testName: string,
  updatedMs: number,
  threadCompEnergies: Array<ThreadCompEnergiesType>,
  events: Array<EventsType>,
  componentUnit: Array<UnitType>,
  threadUnit: Array<UnitType>,
  commitId: string,
  commitHash: string,
  testDurationMS: number
}

/** RecordsType is the form Iterations are stored in Redux */
export type RecordsType = {
  commitId: string,
  testName: string,
  testRecords: Array<TestRecordType>
}


export type CommitType = {
  _id: string,
  hash: string,
  desc: string,
  contributor: ContributorType,
  branchName: string,
  updatedMs: number,
  jobStatus: ?string,
  jobDurationMs: ?number,
  testRecordSet: Array<TestRecordType>
}

export type BranchType = {
  _id: string,
  branchName: string,
  updatedMs: number,
  commits: Array<CommitType>,
  tests: Array<string>
}
/**
 * Helper functions to convert input object to a Type. Checks for all the
 * mandatory properties in the object. Converts string to numbers and booleans
 * where required
 */
export function toBranchType(o: Object): BranchType {
  hasAllProperties(o, ["_id", "branchName", "updatedMs", "tests"])
  return {
    _id : o._id,
    branchName : o.branchName,
    updatedMs : parseInt(o.updatedMs),
    commits : o.commits ? o.commits.map(c => toCommitType(c)) : [],
    tests: o.tests
  }
}

export function toCommitType(o: Object): CommitType {
  hasAllProperties(o, ["_id", "hash", "contributor", "branchName",  "updatedMs", "testRecordSet"])
  return {
    _id : o._id,
    hash : o.hash,
    desc : o.desc ? o.desc : '',
    contributor : toContributorType(o.contributor),
    branchName : o.branchName,
    updatedMs : parseInt(o.updatedMs),
    jobStatus : o.jobStatus,
    jobDurationMs : o.jobDurationMs ? parseInt(o.jobDurationMs) : undefined,
    testRecordSet : o.testRecordSet.map(ts  => toTestRecordType(ts, o._id, o.hash))
  }
}

export function toUploadType(o: Object): UploadType {
  hasAllProperties(o, ["_id", "hash", "branchName", "authorEmail", "authorName", "uploadMs", "jobStatus"])
  return {
    _id : o._id,
    hash: o.hash,
    branchName : o.branchName,
    authorEmail : o.authorEmail,
    authorName : o.authorName,
    jobDurationMs: o.jobDurationMs != null ? parseInt(o.jobDurationMs) :  -1,
    jobStatus: o.jobStatus,
    uploadMs: (new Date(o.uploadMs)).getTime(),
  }
}

export function toTestRecordType(o: Object, commitId: string, commitHash: string): TestRecordType {
  hasAllProperties(o, ["_id", "energy", "testName", "updatedMs"])
  if(! commitId)
    throw new Error("No commitId specified in toTestRecordType")
  return {
    _id : o._id,
    count : o.count ? parseInt(o.count) : 1,
    energy : parseFloat(o.energy),
    energySquared : o.energySquared ? parseFloat(o.energySquared) : o.energy * o.energy,
    power : o.power ? parseFloat(o.power) : 0,
    powerSquared : o.powerSquared ? parseFloat(o.powerSquared) : 0,
    testName : o.testName,
    updatedMs : parseInt(o.updatedMs),
    threadCompEnergies : o.threadCompEnergies ? o.threadCompEnergies.map(tce => toThreadCompEnergiesType(tce)) : [],
    events : o.events ? o.events.map(e => toEventsType(e)) : [], 
    componentUnit : o.componentUnit ? o.componentUnit.map(u => toUnitType(u)) : [],
    threadUnit : o.threadUnit ? o.threadUnit.map(u => toUnitType(u)) : [],
    records : o.records ? o.records.map(r => toTestRecordType(r, commitId, commitHash)) : undefined,
    testDurationMS: parseInt(o.testDurationMS),
    commitHash,
    commitId
  }
}

export function toContributorType(o: Object): ContributorType {
  hasAllProperties(o, ["email", "name"])
  return {
    email: o.email,
    name: o.name
  }
}

export function toThreadCompEnergiesType(o: Object): ThreadCompEnergiesType {
  hasAllProperties(o, ["threadId", "componentId", "energy", "energySquared"])
  return {
    threadId : o.threadId,
    componentId : o.componentId,
    energy : parseInt(o.energy),
    energySquared : parseInt(o.energySquared)
  }
}

export function toUnitType(o: Object): UnitType {
  hasAllProperties(o, ["name", "energy", "energySquared"])
  return {
    processName : o.processName,
    name : o.name,
    energy : parseFloat(o.energy),
    energySquared : parseFloat(o.energySquared),
    power : o.power ? o.power : 0,
    powerSquared : o.powerSquared ? o.powerSquared : 0
  }
}

export function toEventsType(o: Object): EventsType {
  hasAllProperties(o, ["eventName", "name", "durationSec", "durationSecSquared"])
  return {
    processName : o.processName,
    eventName : o.eventName,
    name : o.name,
    durationSec : parseFloat(o.durationSec),
    durationSecSquared : parseFloat(o.durationSecSquared)
  }
}

export function toProfileType(o: Object): ProfileType {
  return {
    email : o.email ? o.email : "N/A",
    username : o.username ? o.username : "N/A",
    role : o.role ? o.role : "default",
    firstname : o.firstname ? o.firstname : "User",
    lastname : o.lastname ? o.lastname : "",
    organization : o.organization ? o.organization : "Org",
    registrationTime : o.registrationTime ? parseInt(o.registrationTime) : undefined,
    expirationTime : o.expirationTime ? parseInt(o.expirationTime) : undefined,
  }
}

export function toProjectType(o: Object): ProjectType {
  hasAllProperties(o, ["_id", "prefix", "tourStatus", "commitUrlPrefix", "widgets"])
  return {
    _id : o._id,
    prefix : o.prefix ? o.prefix : 'origin/release',
    commitUrlPrefix : o.commitUrlPrefix ? o.commitUrlPrefix : '',
    tourStatus : o.tourStatus === 'true',
    widgets : buildWidgetArray(o.widgets),
  }
}

function buildWidgetArray(widgets: ?Array<Object>): Array<WidgetType> {
  if (widgets == null) return buildDefaultWidgetArray()
  return widgets.map(widget => toWidgetType(widget))
}

function buildDefaultWidgetArray(): Array<WidgetType> {
  return [
    toWidgetType({type: BRANCH_COMPARISON, firstBranchName: "", firstCommitHash: DEFAULT_COMMIT_SELECTION, secondBranchName: "", secondCommitHash: DEFAULT_COMMIT_SELECTION}),
    toWidgetType({type: BRANCH_COMPARISON, firstBranchName: "", firstCommitHash: DEFAULT_COMMIT_SELECTION, secondBranchName: "", secondCommitHash: DEFAULT_COMMIT_SELECTION}),
    toWidgetType({type: TEST_COMPARISON, firstBranchName: "", firstCommitHash: DEFAULT_COMMIT_SELECTION, secondBranchName: "", secondCommitHash: DEFAULT_COMMIT_SELECTION}),
    toWidgetType({type: COMPONENT_COMPARISON, firstBranchName: "", firstCommitHash: DEFAULT_COMMIT_SELECTION, secondBranchName: "", secondCommitHash: DEFAULT_COMMIT_SELECTION}),
    toWidgetType({type: THREAD_COMPARISON, firstBranchName: "", firstCommitHash: DEFAULT_COMMIT_SELECTION, secondBranchName: "", secondCommitHash: DEFAULT_COMMIT_SELECTION}),
  ]
}

export function toWidgetType(o: Object): WidgetType {
  hasAllProperties(o, ["type", "firstBranchName", "secondBranchName", "firstCommitHash", "secondCommitHash"])
  return {
    type: o.type,
    firstBranchName: o.firstBranchName,
    secondBranchName: o.secondBranchName,
    firstCommitHash: o.firstCommitHash,
    secondCommitHash: o.secondCommitHash,
    metric: o.metric ? o.metric : ENERGY_METRIC
  }
}

export function toCommonType(projectId: ?string) {
  return {
    curProjectIndex : 0,
    projectId
  }
}

export function toAboutType(o: Object) {
  return {
    version: o.version
  }
}

/* Type each entry of data array in recordMap should be */
export type RecordMapDataType = {
  category: string,
  value: number,
  valueSquared: number,
  count: number
}
/* Type passed in an array to ChartDataPrep */
export type RecordMapType = {
  xLabel: string,
  id: string,
  commitId: string,
  data: Array<RecordMapDataType>
}

/* Type passed to Units to filter records */ 
export type Unit = {
  label: string,
  extractData: (record: TestRecordType, count: number)=> Array<RecordMapDataType>,
  scaleLabelStr: string
}

/* Type that dataset fed to chartJs should be in */
export type ChartDatasetType = {
  label: string,
  xLabels: Array<string>,
  borderColor: string,
  backgroundColor: string,
  borderWidth: Array<number>,
  counts: Array<number>,
  data: Array<number>,
  deviations: Array<number>,
  intervals: Array<number>,
  id: Array<string>,
  visible: boolean,
  errorBars: ?any
}

/* Type used to compare two distributions with a t-test */
export type StatsInfo = {
    mean: number,
    variance: number,
    count: number
}

/** Util types */
export type ColorType = $Values<typeof mainColors>
export type ShadeType = $Values<typeof shades>
export type StepType = {element: string, intro: string}

/** Comparison types */
export const BRANCH_COMPARISON = "branch"
export const BRANCH_ENERGY_COMPARISON_DISPLAY = "Branch Energy Comparison"
export const BRANCH_POWER_COMPARISON_DISPLAY = "Branch Power Comparison"
export const BRANCH_COMPARISON_DISPLAY = "Branch Comparison"

export const TEST_COMPARISON = "test"
export const TEST_ENERGY_COMPARISON_DISPLAY = "Test Energy Comparison"
export const TEST_POWER_COMPARISON_DISPLAY = "Test Power Comparison"
export const TEST_COMPARISON_DISPLAY = "Test Comparison"

export const COMPONENT_COMPARISON = "component"
export const COMPONENT_ENERGY_COMPARISON_DISPLAY = "Component Energy Comparison"
export const COMPONENT_POWER_COMPARISON_DISPLAY = "Component Power Comparison"
export const COMPONENT_COMPARISON_DISPLAY = "Component Comparison"

export const THREAD_COMPARISON = "thread"
export const THREAD_ENERGY_COMPARISON_DISPLAY = "Thread Energy Comparison"
export const THREAD_POWER_COMPARISON_DISPLAY = "Thread Power Comparison"
export const THREAD_COMPARISON_DISPLAY = "Thread Comparison"

/** Metric types */
export const POWER_METRIC = "power"
export const ENERGY_METRIC = "energy"

export const comparisonTypes = [BRANCH_COMPARISON, TEST_COMPARISON, COMPONENT_COMPARISON, THREAD_COMPARISON]
export function getComparisonTypeDisplay(comparisonType: string, metric: string): string {
  // metric may be undefined when user is trying to pick a widget
  switch (comparisonType) {
    case BRANCH_COMPARISON: 
      if(metric === ENERGY_METRIC)
        return BRANCH_ENERGY_COMPARISON_DISPLAY 
      else if(metric === POWER_METRIC)
        return BRANCH_POWER_COMPARISON_DISPLAY
      return BRANCH_COMPARISON_DISPLAY
    case TEST_COMPARISON: 
      if(metric === ENERGY_METRIC)
        return TEST_ENERGY_COMPARISON_DISPLAY 
      else if(metric === POWER_METRIC)
        return TEST_POWER_COMPARISON_DISPLAY
      return TEST_COMPARISON_DISPLAY
    case COMPONENT_COMPARISON: 
      if(metric === ENERGY_METRIC)
        return COMPONENT_ENERGY_COMPARISON_DISPLAY 
      else if(metric === POWER_METRIC) 
        return COMPONENT_POWER_COMPARISON_DISPLAY
      return COMPONENT_COMPARISON_DISPLAY
    default: 
      if(metric === ENERGY_METRIC) 
        return THREAD_ENERGY_COMPARISON_DISPLAY
      else if(metric === POWER_METRIC) 
        return THREAD_POWER_COMPARISON_DISPLAY
      return THREAD_COMPARISON_DISPLAY
  }
}

export const DEFAULT_COMMIT_SELECTION = "Default(most recent commit)"

/** Redux types */
export type ExtractReturn<Fn> = $Call<<T>((...Iterable<any>) => T) => T, Fn>;
export type $ExtractFunctionReturn = <V>(v: (...args: any) => V) => V;
export type State = $ObjMap<ReducersType, $ExtractFunctionReturn>;

// Actions
export const FETCH_PROFILE_SUCCESS = "FETCH_PROFILE_SUCCESS"
export const OPEN_DRAWER = "OPEN_DRAWER"
export const FETCH_ABOUT_SUCCESS = "FETCH_ABOUT_SUCCESS"
export const FETCH_UPLOADS_SUCCESS = "FETCH_UPLOADS_SUCCESS"
export const FETCH_ITERATIONS_SUCCESS = "FETCH_ITERATIONS_SUCCESS"
export const FETCH_ITERATIONS_FAILURE = "FETCH_ITERATIONS_FAILURE"
export const FETCH_ITERATIONS_START = "FETCH_ITERATIONS_START"
export const FETCH_COMMIT_SUCCESS = "FETCH_COMMIT_SUCCESS"
export const FETCH_COMMIT_START = "FETCH_COMMIT_START"
export const FETCH_COMMIT_FAILURE = "FETCH_COMMIT_FAILURE"
export const DELETE_COMMIT_SUCCESS = "DELETE_COMMIT_SUCCESS"
export const DELETE_TESTRECORD_SUCCESS = "DELETE_TESTRECORD_SUCCESS"
export const FETCH_BRANCHES_SUCCESS = "FETCH_BRANCHES_SUCCESS"
export const FETCH_PROJECTS_SUCCESS = "FETCH_PROJECTS_SUCCESS"
export const UPDATE_PROJECTS_SUCCESS = "UPDATE_PROJECTS_SUCCESS"
export const UPDATE_WIDGET_SUCCESS = "UPDATE_WIDGET_SUCCESS"
export const REPROCESS_UPLOAD_SUCCESS = "REPROCESS_UPLOAD_SUCCESS"

export type Action = { type: "FETCH_PROFILE_SUCCESS", profile: ProfileType }
| {type: "FETCH_ITERATIONS_SUCCESS", records: Array<TestRecordType>, commitId: string, testName: string}
| {type: "FETCH_ITERATIONS_START"}
| {type: "FETCH_ITERATIONS_FAILURE"}
| {type: "DELETE_TESTRECORD_SUCCESS", testRecord: TestRecordType}
| {type: "OPEN_DRAWER", drawer: boolean}
| {type: "FETCH_ABOUT_SUCCESS", about: AboutType}
| {type: "FETCH_COMMIT_SUCCESS", commits: Array<CommitType>}
| {type: "FETCH_COMMIT_START"}
| {type: "FETCH_COMMIT_FAILURE"}
| {type: "FETCH_UPLOADS_SUCCESS", uploads: Array<UploadType>}
| {type: "DELETE_COMMIT_SUCCESS", commitId: string}
| {type: "FETCH_BRANCHES_SUCCESS", branches: Array<BranchType>}
| {type: "FETCH_PROJECTS_SUCCESS", projects: Array<ProjectType>}
| {type: "UPDATE_PROJECTS_SUCCESS", project: ProjectType}
| {type: "REPROCESS_UPLOAD_SUCCESS", commitId: string}

// eslint-disable-next-line
export type Dispatch = (action: Action | ThunkAction | PromiseAction) => any
export type GetState = () => State
export type ThunkAction = (dispatch: Dispatch, getState: GetState) => any
export type PromiseAction = Promise<Action>
export type ReduxProps<M, D> = $ReadOnly<{|
  ...ExtractReturn<M>,
  ...ExtractReturn<D>
|}>
export type ReduxProp<M> = $ReadOnly<{|
  ...ExtractReturn<M>,
|}>
