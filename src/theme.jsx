import { createTheme } from '@material-ui/core/styles';
function METheme() {
  var metheme = {
    // FROM _theme_fonts.scss
    typography: {
      fontFamily: "Open Sans, sans serif",
      body: {
        fontSize: 15
      },
      h1: {
        fontWeight: 700,
        fontSize: 48
      }, 
      h2: {
        fontWeight: 700,
        fontSize: 24,
        textTransform: 'uppercase',
        letterSpacing: '.125em'
      },
      h3: {
        fontSize: 16
      },
      chart: {
        fontSize: 14
      }
    },

    // FROM _theme-colors.scss
    palette: {
      primary: {
        main: "#88C442",
      },
      secondary: {
        main: "#F05A2B", 
      },
      action: {
        main: "#F7F7F7",
      },
      primary1Color: "#88C442",       //cyan500,
      primary2Color: "#76A538",        //cyan700,
      //primary3Color:        //grey400,
      accent1Color:  "#F05A2B",        //pinkA200,
      accent2Color:  "#D84F2A",       //grey100,
      //accent3Color:         //grey500,
      //textColor:            //darkBlack,
      //alternateTextColor:   //white,
      //canvasColor:          //white,
      //borderColor:          //grey300,
      //pickerHeaderColor:    //cyan500,
      //shadowColor:          //fullBlack,

      third: "#458BCA",
      thirdDarker: "#366E99",

      forth: "#192B33",
      forthDarker: "#0D1619",

      // grey: "#CCCCCC",
      greyDarker: "#888888",
      greyDarkest: "#666666",
      greyLighter: "#EEEEEE",
      greyLightest: "#F7F7F7",

      graphFirst: "rgba(54, 162, 235, 0.8)",
      graphFirstBorder: "rgba(54, 162, 235, 1)",
      graphSecond: "rgba(54, 235, 162, 0.8)",
      graphSecondBorder: "rgba(54, 235, 162, 1)",

    }
  }
  return createTheme(metheme);
}

export default METheme;