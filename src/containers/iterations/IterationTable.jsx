// @flow
import React from "react"
import type { ContextRouter } from "react-router"
import * as Types from '../../types'
import { withRouter } from "react-router-dom"
import { connect } from "react-redux"
import { bindActionCreators } from "redux"
import * as Actions from "../../actions"
import { withTheme } from '@material-ui/core/styles'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import { Delete } from "@material-ui/icons"
import TimeAgo from "react-timeago"
import * as Util from "../../Util"

type Props = {
    ...ContextRouter,   // For history and location to work
    ...Types.ReduxProps<typeof mapStateToProps, typeof mapDispatchToProps>,
    records: Array<Types.TestRecordType>, 
    count: number,
    currentBranch: string,
}

type State = {
  showDialog: boolean,
  steps: Array<Object>,
}

const cell = {
    fontSize: "16px"
}

class IterationTable extends React.PureComponent<Props, State> {
  onClickDelete = function(testRecord: Types.TestRecordType) {
    let callback = () => {
      this.props.actions.fetchCommitsStart(this.props.count, null, this.props.currentBranch)
    }
    this.props.actions.deleteTestRecordStart(testRecord, callback)
  }

  render() {
    const { recordStatus } = this.props
    return (<Table>
    <TableHead>
      <TableRow>
        <TableCell style={cell}>Iteration</TableCell>
        <TableCell style={cell}>Actions</TableCell>
        <TableCell style={cell}>Test duration</TableCell>
        <TableCell style={cell}>Processed</TableCell>
      </TableRow>
    </TableHead>
    <TableBody>
      {this.props.records.map((r) => (<TableRow key={r._id}>
        <TableCell style={cell}>
          {(new Date(r.updatedMs)).toLocaleString()}
        </TableCell>
        <TableCell style={cell}>
          <Delete enabled={(recordStatus !== Types.STATUS_LOADING).toString()} style={{cursor:"pointer"}} onClick={() => this.onClickDelete(r)} />
        </TableCell>
        <TableCell style={cell}>
          {Util.msToTime(r.testDurationMS)}
        </TableCell>
        <TableCell style={cell}>
          <TimeAgo date={r.updatedMs} /> 
        </TableCell>
      </TableRow>))}
    </TableBody>
    </Table>)
  }
}

const mapDispatchToProps = (dispatch: *) => {
    return {
        actions: bindActionCreators(Actions, dispatch)
    }
}

const mapStateToProps = (state: Types.State) => {
  return {
    recordStatus: state.recordStatus,
    projectId: state.project[state.common.curProjectIndex]._id,
  };
};

export default withRouter(
  withTheme(connect(mapStateToProps, mapDispatchToProps)(IterationTable))
);
