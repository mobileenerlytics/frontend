import React from 'react'
import { withTheme } from '@material-ui/core/styles'
import { Email, Delete, Brightness1 } from "@material-ui/icons"
import CommitUrlDialog from "./CommitUrlDialog"
import PropTypes from "prop-types"
import TestBreakDown from "./TestBreakDown"
import CommitContributorEmail from "./CommitContributorEmail"
import moment from "moment"
import { connect } from "react-redux"
import { Link, withRouter } from "react-router-dom"
import { Table, TableCell, TableRow, TableBody, TableHead, Grid } from '@material-ui/core'
import * as Util from "../../Util"

class CommitDetail extends React.Component {
    constructor(props, context) {
        super(props, context)
        this.state = {
            openCommitDislog: false,
            openCommitEmail: false,
        }
        this.onClickHash = this.onClickHash.bind(this)

    }

    onClickHash() {
        const { commitUrlPrefix } = this.props.curProject
        const { commit } = this.props
        if (commitUrlPrefix.length === 0) {
            this.setState({ openCommitDislog: true })
        } else {
            let url = commitUrlPrefix + "/" + commit.hash
            let win = window.open(url, '_blank')
            win.focus()
        }
    }

    render() {
        const { commit, onClickDelete, curProject, history, theme } = this.props
        const queryUrl = window.location.host + history.location.pathname + history.location.search
        const style = {
            primary: { flex: "0 0 180px" },
            iconEmail: theme.palette.third,
            iconDelete: theme.palette.accent1Color,
        }
        return (
            <Grid container justifyContent="space-evenly">
                <Grid item>
                    <TestBreakDown
                        commit={commit}
                    />
                </Grid>
                <Grid item>
                    <h3 align="center">Commit details</h3>
                    <Table>
                        <TableBody>
                            <TableRow>
                                <TableCell>Hash</TableCell>
                                <TableCell><CommitUrlDialog
                                        open={this.state.openCommitDislog}
                                        onClose={() => this.setState({ openCommitDislog: false })}
                                        value={curProject.commitUrlPrefix.length === 0 ? "https://" : curProject.commitUrlPrefix}
                                    />
                                    <a onClick={this.onClickHash}>{commit.hash}</a>
                                </TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell>Created </TableCell>
                                <TableCell>  
                                    {moment(commit.updatedMs).format("MM/DD/YYYY HH:mm")} 
                                    <div id="deleteIcon" style={{display: "inline"}} className="iconStyle">
                                        <Delete
                                            onClick={onClickDelete}
                                            style={{ paddingLeft: "10px", height: "20px", width: "20px", boxSizing: "content-box", fill: style.iconDelete}}
                                        />
                                    </div>
                                </TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell>Contributor</TableCell>
                                <TableCell>{commit.contributor.name}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell>Email</TableCell>
                                <TableCell>
                                    <a onClick={() => this.setState({ openCommitEmail: true })} >
                                        {commit.contributor.email}
                                    </a>
                                    <div id="emailIcon" style={{display: "inline"}} className="iconStyle">
                                        <Email
                                            style={{ paddingLeft: "10px", height: "20px", width: "20px", overflow: "visible", boxSizing: "content-box", fill: style.iconEmail}}
                                            onClick={() => this.setState({ openCommitEmail: true })}
                                        />
                                    </div>
                                <CommitContributorEmail commit={commit} queryUrl={queryUrl} open={this.state.openCommitEmail} onClose={() => this.setState({ openCommitEmail: false })} />
                                </TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell>Branch Name</TableCell>
                                <TableCell>{commit.branchName}</TableCell>
                            </TableRow>
                        </TableBody>
                    </Table>
                    <br/>
                <h3 align="center"> Tests </h3>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>Test name</TableCell>
                            <TableCell>Number of iterations</TableCell>
                            <TableCell>Test duration</TableCell>
                            <TableCell>Energy</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {commit.testRecordSet.map((t) =>
                            <TableRow key={t._id}>
                                <TableCell>
                                    <Link to={`/analysis?count=10&branchName=${commit.branchName}&selectedCommit=${commit.hash}&testName=${t.testName}`} >
                                        {t.testName}
                                        <Brightness1 style={{fill: Util.getRandomRGB(t.testName, 100)}}/>
                                    </Link>
                                </TableCell>
                                <TableCell>{t.count}</TableCell>
                                <TableCell>{Util.msToTime(t.testDurationMS)}</TableCell>
                                <TableCell>{t.energy} uAh</TableCell>
                            </TableRow>
                        )}
                    </TableBody>
                </Table>
                </Grid>
            </Grid>
        )
    }
}

const mapStateToProps = state => {
    return {
        curProject: state.project[state.common.curProjectIndex],
    }
}


CommitDetail.propTypes = {
    commit: PropTypes.object.isRequired,
    onClickDelete: PropTypes.func.isRequired
}

export default withRouter(
    withTheme(connect(mapStateToProps, null)(CommitDetail))
)
