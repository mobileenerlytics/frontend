// @flow
import React from "react"
import Commits from "../../components/Commits"
import CommitPrep from "../../components/CommitPrep"
import ChartDataPrep from "../../components/ChartDataPrep"
import CommitDetail from "./CommitDetail"
import { connect } from "react-redux"
import { bindActionCreators } from "redux"
import { withRouter } from "react-router-dom"
import * as Actions from "../../actions"
import Appbar from "../../components/Appbar"
import { withTheme } from '@material-ui/core/styles'
import AnalysisTopbar from "../../components/AnalysisTopbar";
import UrlManager from "../../components/UrlManager";
import * as Types from '../../types'
import type { ContextRouter } from "react-router"
import { Divider } from '@material-ui/core'

type Props = {
    ...ContextRouter,   // For history and location to work
    ...Types.ReduxProps<typeof mapStateToProps, typeof mapStateToDispatch>,
}

type State = {
  flagdetail: boolean,
  steps: Array<Object>
}

class CommitContainer extends React.Component<Props, State> {
  constructor(props, context) {

    super(props, context)
    this.state = {
      flagdetail: false,
      steps: [
        {
          element: '.commitTrend',
          intro: 'The comparison of all the commits in every branch is displayed.',
        },
        {
          element: '.chartjs-render-monitor',
          intro: 'Please click on the bar to see a detailed test breakdown of the commit.',
        },
      ],
      stepsDetails: [
        {
          element: '#deleteIcon',
          intro: 'This commit can be deleted by clicking on the delete icon.',
        },
        {
          element: '#emailIcon',
          intro: 'Clicking on this email icon will send an email with a link to the commits trend page to the contributor.',
        },
        {
          element: '#testAnalysisDrawer',
          intro: 'Please click on the link to navigate to the test analysis page.',
        },
      ],
    }
  }

  handleCommitDelete(commitId) {
    this.props.actions.deleteCommitsStart(commitId)
  }

  onElementsClick(elems) {
    console.log("onElementsClick" + elems)
    if (this.props.tourStatus) {
      this.setState({
        flagdetail: true
      })
    }
  }

  render() {
    return (
      <div>
        <Appbar pageTitle="Commit Trend: Total Battery Drain per Commit" />
        <div className={this.props.openDrawer ? "openWrapper" : "closedWrapper"}>
          <div className="commitTrend">
            {/* AnalysisTopbar holds dropdowns for branch, unit, count, and testName selection. It passes these selections to its children. */}
            <AnalysisTopbar >
              {(currentBranch, unit, count) => (
                /* Commits filters commits for only commits on selected branch. Also manages selecting a specific commit to render iterations of. */
                <Commits currentBranchName={currentBranch.branchName} count={count}>
                  {(commitRecords, selectedCommit, selectCommitFunction) => (
                    <div>
                      <UrlManager testName={null} branchName={currentBranch.branchName} count={count} selectedCommit={selectedCommit} history={this.props.history} />
                      {/* Units takes commits and expands them by testName. Put into the following form (same as Units):
                        *  {xLabel: xLabel1, id: id, data: [{category: testName1, count: count, value: value, valueSquared: valueSquared}, ...]},
                        */}
                      <CommitPrep commitRecords={commitRecords} xLabelIsBranchName={false}>
                        {(recordMap) => (
                          /* Takes the filtered records from TestFilter and prepares them for the form that React-js Charts expects. */
                          <ChartDataPrep recordMap={recordMap} onClick={selectCommitFunction} selectedCommit={selectedCommit} xLabel={"Commit Hash"} yLabel={"Energy (uAh)"} meanbar={false} />
                        )}
                      </CommitPrep>
                        {selectedCommit != null ? 
                          <div>
                            <Divider/>
                            <CommitDetail
                              commit={selectedCommit}
                              onClickDelete={() => this.handleCommitDelete(selectedCommit._id)}
                            />
                          </div>
                          : null}
                    </div>
                  )}
                </Commits>
              )}
            </AnalysisTopbar>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state: Types.State) => {
  return {
    allBranches: state.branch,
    allCommits: state.commit,
    curProject: state.project[state.common.curProjectIndex],
    openDrawer: state.openDrawer,
    tourStatus: state.project[state.common.curProjectIndex].tourStatus,
    projectId: state.project[state.common.curProjectIndex]._id,
  }
}

const mapStateToDispatch = (dispatch: *) => {
  return {
    actions: bindActionCreators(Actions, dispatch)
  }
}

export default withRouter(
  withTheme(connect(mapStateToProps, mapStateToDispatch)(CommitContainer))
)
