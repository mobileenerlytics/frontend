import React from 'react'
import BasicSunburst from "../../components/BasicSunburst";
import * as Util from "../../Util";
export default function TestBreakDown( {commit}){
        var data = {};
        data.sum = commit.testRecordSet.reduce((sum, cur) => sum + cur.energy, 0.0);
        data.children = commit.testRecordSet.map((t) => {
            if (t.threadCompEnergies == null) t.threadCompEnergies = [];
            const componentNames = t.threadCompEnergies.reduce((acc, tce) => acc.includes(tce.componentId) ? acc : [...acc, tce.componentId], []);
            return {
                nameDisplay: t.testName,
                name: t.testName,
                sum: t.energy,
                // hex: rgbMapping[t.testName],
                hex: Util.getRandomRGB(t.testName, 100),
                children: componentNames.map((cname) => {
                    var tceByCname = t.threadCompEnergies.filter((tce) => tce.componentId === cname);
                    var threadNames = tceByCname.map((tcb) => tcb.threadId);
                    return {
                        nameDisplay: cname,
                        name: cname + "_" + t.testName,
                        sum: tceByCname.reduce((acc, tce) => acc + tce.energy, 0.0),
                        hex: Util.getRandomRGB(cname, 400),
                        children: threadNames.map(tname => {
                            var energy = tceByCname
                                .filter((tcb) => tcb.threadId === tname)
                                .reduce((acc, cur) => acc + cur.energy, 0.0);
                            return {
                                nameDisplay: tname,
                                name: tname + "_" + cname + "_" + t.testName,
                                hex: Util.getRandomRGB(tname, 200),
                                value: energy,
                                sum: energy,
                            }
                        })
                    };
                })
            }
        })
        return (
        <div>
            <h3 align="center"> Test Breakdown </h3>
            <BasicSunburst data={data} />
        </div>
        )
    }

