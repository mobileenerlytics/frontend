import React from 'react'
import { Button, Dialog, TextField } from '@material-ui/core';

export default class CommitContributorEmail extends React.PureComponent {
    constructor(props, context) {
        super(props, context);
        this.state = {};
        this.onRequestEmail = this.onRequestEmail.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
    }

    static getDerivedStateFromProps(props) {
        const { commit, queryUrl } = props
        const contributor = commit.contributor
        const { email, name } = contributor
        const defaultTitle = `Eager Tester commit ${commit.hash}`
        return {
            msg: "",
            recipients: `${email}, `,
            subject: defaultTitle,
            content: `Hi ${name}, \n\nThe commit "${commit.hash}" in branch "${commit.branchName}" is taking ${commit.testRecordSet.reduce((sum, cur) => sum + cur.energy, 0.0)} uAh energy. Go to https://${queryUrl} for full report.\n\nMobile Enerlytics`
        }
    }

    handleInputChange(evt) {
        this.setState({ [evt.target.name]: evt.target.value });
        if (this.state[evt.target.name].length === 0) 
            this.setState({msg: `${evt.target.name} is required!`});
    }

    onRequestEmail() {
        if (this.state.recipients.length === 0) {
            this.setState({ msg: "recipients is required!" });
            return;
        }
        var context = this;
        var formData = new FormData();
        formData.append("recipients", this.state.recipients);
        formData.append("subject", this.state.subject);
        formData.append("content", this.state.content);
        console.log(formData);
        const init = {
            method: "POST",
            credentials: "same-origin",
            headers: {},
            body: formData
        };
        const url = `/api/email`;
        fetch(url, init)
            .then(response => {
                if (!response.ok) throw Error(response.statusText);
                return response;
            })
            .then(function () {                                
                context.setState({msg: "Sent successfully, will close in 3 seconds.",});
                setTimeout(()=> {
                    context.props.onClose();                        
                }, 3000);
            })
            .catch(function () {            
                context.setState({ msg: "Sent failed, please check the recipients input." });                
            });
    }

    render() {
        const { open, onClose } = this.props;
        const actions = [
            <Button
                key="Cancel"
                variant="text"
                onClick={onClose}>Cancel</Button>,
            <Button
                key="Send"
                variant="text"
                primary={true}
                onClick={this.onRequestEmail}>Send</Button>,
        ]
        return (
            <div>
                <Dialog
                    title="Email"
                    // contentStyle={}
                    actions={actions}
                    modal={true}
                    open={open}
                    autoScrollBodyContent={true}
                >

                    <TextField
                        floatingLabelText="Recipients"
                        floatingLabelFixed={true}
                        name="recipients"
                        hintText="Recipients, divided by ','"
                        onChange={this.handleInputChange}
                        value={this.state.recipients}
                        fullWidth={true}
                        underlineShow={true}
                    /> <br />
                    <TextField
                        floatingLabelText="Subject"
                        floatingLabelFixed={true}
                        name="subject"
                        onChange={this.handleInputChange}
                        hintText="Subject"
                        value={this.state.subject}
                        fullWidth={true}
                    /> <br />
                    <TextField
                        name="content"
                        onChange={this.handleInputChange}
                        hintText="Content"
                        value={this.state.content}
                        multiLine={true}
                        rows={10}
                        rowsmin={4}
                        fullWidth={true}
                    /> <br />
                    <span > {this.state.msg} </span>
                </Dialog>
            </div>
        )
    }

}

