import React from 'react'
import { Button, Dialog, TextField } from '@material-ui/core';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as Actions from "../../actions";

class CommitUrlDialog extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {            
            newCommitUrl: this.props.value,
            msgCommitUrl: "",
        };
        this.onRequestUpdateCommitUrl = this.onRequestUpdateCommitUrl.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
    }

    handleInputChange(evt) {
        this.setState({ [evt.target.name]: evt.target.value });
    }
      
    onRequestUpdateCommitUrl() {
        if (this.state.newCommitUrl.length === 0) {
          this.setState({msgCommitUrl: "please input your new commit url."});
          return;
        }
        this.props.actions.updateProjectStart(
          "commitUrlPrefix",
          this.state.newCommitUrl, 
          this.props.curProject._id,
          () => {
            // close  
            this.setState({
              newCommitUrl: this.props.value,
              msgCommitUrl: "Successfully updated!",
            });
            this.props.onClose();
          }
        );  
      }

    render() {
        let {open, onClose} = this.props;
        const LONG_URL_STYLE = {
            fontSize: "14px",
            display: "inline-block"
        }
        const commitActions = [
            <Button
                key="cancel"
                variant="text"
                onClick={onClose}>Cancel</Button>,
            <Button
                key="Submit"
                variant="text"
                primary={true}
                onClick={this.onRequestUpdateCommitUrl}>Submit</Button>,
        ]
        return (
            <div>                
                <Dialog
                    title="Commit URL Prefix"
                    contentStyle={{ width: '70%' }}
                    actions={commitActions}
                    modal={true}
                    open={open}
                >
                    To see commit diff, setup the commit URL prefix here. <br/> <br/>
                    
                    For example, for Wordpress project, the commit <span style={LONG_URL_STYLE} className="color-second-text">e12ca7fb602ce99d54ff76bee8c073367dd58ef2</span> is at URL
                    <br/>
                    <span style={LONG_URL_STYLE} className="color-first-text">https://github.com/wordpress-mobile/WordPress-Android/commit/</span><span style={LONG_URL_STYLE} className="color-second-text">e12ca7fb602ce99d54ff76bee8c073367dd58ef2</span>
                    <br/>
                    then <span className="color-first-text" style={LONG_URL_STYLE}>https://github.com/wordpress-mobile/WordPress-Android/commit/</span> is the commit URL prefix.
                    <br/>
                    <TextField 
                    name="newCommitUrl" 
                    floatingLabelText="New Commit Url"  
                    value={this.state.newCommitUrl} 
                    fullWidth={true}
                    onChange={this.handleInputChange} />
                </Dialog>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
      curProject: state.project[state.common.curProjectIndex],
    };
  };
  
const mapStateToDispatch = dispatch => {
    return {
      actions: bindActionCreators(Actions, dispatch)
    };
  };
  
export default connect(mapStateToProps, mapStateToDispatch)(CommitUrlDialog);
  
