import React from 'react'
import BasicSunburst from "../../components/BasicSunburst";
import * as Util from "../../Util";
export default function ProcessBreakDown({ testRecord }) {
    let {threadCompEnergies} = testRecord;
    let tces = threadCompEnergies;
    const processIds = tces.reduce((acc, tce) => acc.includes(tce.processId) ? acc : [...acc, tce.processId], []);

    let data = {};
    data.sum = tces.reduce((sum, cur) => sum + cur.energy, 0.0);
    data.children = processIds.map((pid) => {
        let tces_pid = tces.filter((tce) => tce.processId == pid);
        const componentIds = tces_pid.reduce((acc, tce) => acc.includes(tce.componentId) ? acc : [...acc, tce.componentId], []);
        let sum_pid = tces_pid.reduce((sum, cur) => sum + cur.energy, 0.0);
        return {
            nameDisplay: pid,
            name: pid,
            sum: sum_pid,
            hex: Util.getRandomRGB(pid),
            children: componentIds.map((cid) => {
                let tces_pid_cid = tces_pid.filter((tce) => tce.componentId == cid);
                let tids = tces_pid_cid.reduce((acc, tce) => acc.includes(tce.threadId) ? acc : [...acc, tce.threadId], []);
                let sum_pid_cid = tces_pid_cid.reduce((sum, cur) => sum + cur.energy, 0.0);
                return {
                    nameDisplay: cid,
                    name: pid + "_" + cid,
                    sum: sum_pid_cid,
                    hex: Util.getRandomRGB(cid),
                    children: tids.map(tid => {
                        let tces_pid_cid_tid = tces_pid_cid.filter((tce) => tce.threadId == tid)[0];
                        let sum_pid_cid_tid = tces_pid_cid_tid.energy;
                        return {
                            nameDisplay: tid,
                            name: pid + "_" + cid + "_" + tid,
                            hex: Util.getRandomRGB(tid),
                            value: sum_pid_cid_tid,
                            sum: sum_pid_cid_tid,
                        }
                    })
                };
            })
        }
    })
    return (
        <div style={{ width: "50%" }}>
            <h3> Test BreakDown </h3>
            <BasicSunburst data={data} />
        </div>
    )
}

