import React from 'react'

import PropTypes from "prop-types";
import { Divider } from '@material-ui/core';
import { withTheme } from '@material-ui/core/styles'
import ProcessBreakDown from "./ProcessBreakDown";
import { connect } from "react-redux";
class TestRecordDetail extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
        };
    }
    render() {
        const { testRecord, muiTheme } = this.props;

        const style = {
            primary: { flex: "0 0 180px" },
            iconEmail: muiTheme.palette.third,
            iconDelete: muiTheme.palette.accent1Color,
        }
        return (
            <div style={{ marginLeft: "8px", marginTop: "50px" }}>
                <Divider />
                <div style={{ display: "flex" }}>
                    <ProcessBreakDown
                        testRecord={testRecord}
                    />
                    <div >
                        <div>
                            <h3> Test </h3>
                            {
                              testRecord.appMap &&
                              Object.keys(testRecord.appMap).map(key => {
                                    return (
                                        <div key={key} style={{ display: "flex" }}>
                                            <div style={style.primary}> <span> {key} </span> </div>
                                            <div>
                                                {testRecord.appMap[key]}
                                            </div>
                                        </div>
                                    );
                                })
                            }

                        </div>
                        <br />
                        <div>
                            <Divider />
                            <h3> Device </h3>
                            {
                              testRecord.deviceMap &&
                                Object.keys(testRecord.deviceMap).map(key => {
                                    return (
                                        <div key={key} style={{ display: "flex" }}>
                                            <div style={style.primary}> <span> {key} </span> </div>
                                            <div>
                                                {testRecord.deviceMap[key]}
                                            </div>
                                        </div>
                                    );
                                })
                            }

                        </div>
                    </div>
                </div>

            </div>
            //   </div>
            // </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        curProject: state.project[state.common.curProjectIndex],
    };
};


TestRecordDetail.propTypes = {
    testRecord: PropTypes.object.isRequired,
};

export default withTheme(connect(mapStateToProps, null)(TestRecordDetail));
