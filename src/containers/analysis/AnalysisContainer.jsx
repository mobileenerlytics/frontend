// @flow
import React from "react"
import Commits from "../../components/Commits"
import TestFilter from "../../components/TestFilter"
import Iterations from "../../components/Iterations"
import Units from "../../components/Units"
import ChartDataPrep from "../../components/ChartDataPrep"
import IterationTable from "../iterations/IterationTable"
import { connect } from "react-redux"
import { withRouter } from "react-router-dom"
import Appbar from "../../components/Appbar"
import { withTheme } from '@material-ui/core/styles'
import { Dialog } from "@material-ui/core"
import "../../Plugin.Errorbars"
import AnalysisTopbar from "../../components/AnalysisTopbar";
import UrlManager from "../../components/UrlManager";
import * as Types from '../../types'
import type { ContextRouter } from "react-router"
import Paper from '@material-ui/core/Paper'

/**
 * Analysis Container should handle the outer structure of the page. It is in charge of 
 * holding the Tour, Dialogs, and Chart, but does not pull any of the data itself. The 
 * chart is in charge of fetching and using the data its needs.  
 */
type Props = {
    ...ContextRouter,   // For history and location to work
    ...Types.ReduxProp<typeof mapStateToProps>,
}

type State = {
  showDialog: boolean,
  steps: Array<Object>,
}

class AnalysisContainer extends React.PureComponent<Props, State> {
  constructor(props, context) {
    super(props, context)
    let showDialog = false

    this.state = {
      // TODO: MOVE THIS OUT
      showDialog,
      steps: [
        {
          element: '.testAnalysis',
          intro: 'The comparison of same tests in every commit of individual branches is displayed.',
        },
        {
          element: '.App',
          intro: 'Thank you for taking the tour. Please click on the Guided tour button from the dropdown menu in the top right corner to relaunch the tour.',
        },
      ],
    }
  }

  render() {
    const { profile } = this.props;
    return (
      <div>
        {/* TODO: MOVE THE Dialog OUT*/}
        <Dialog
          open={this.state.showDialog}
          onRequestClose={() => this.setState({ showDialog: false })}
        >
          <h3>Looks like you haven't uploaded any tests yet.</h3>
          Please visit setup <a href={"https://tester.mobileenerlytics.com/instructions.html" + (profile.role === "manual" ? "?subscription=manual" : "")} rel="noopener noreferrer" target="_blank">instructions</a>
        </Dialog>
        <Appbar pageTitle="Test Analysis" id="testAnalysisTitle" />

        <div className={this.props.openDrawer ? "openWrapper" : "closedWrapper"}>
          {/* AnalysisTopbar holds dropdowns for unit and testName selection. It passes these selections to its children. */}
          <AnalysisTopbar  unitsEnabled={true} testsEnabled={true}>
            {(currentBranch, unit, count, testName) => (
              /* Commits filters commits for only commits on selected branch. Also manages selecting a specific commit to render iterations of. */
              <Commits currentBranchName={currentBranch.branchName} count={count}>
                {(commitRecords, selectedCommit, selectCommitFunction) => (
                  /* TestFilter takes commits filtered by selected branch and a specific selected commit and extracts the Records associated with a specific test from them. */
                  <TestFilter commitRecords={commitRecords} testName={testName} selectedCommit={selectedCommit}>
                    {(filteredRecords, filteredSelect) => (
                    <div>
                      {/* Manages the URL based on testName, branchName, count, and the selectedCommit. */}
                      <UrlManager branchName={currentBranch.branchName} testName={testName} count={count} selectedCommit={filteredSelect} history={this.props.history} />
                      {/* Units takes records filtered by testName and filters them by a UnitType: Thread, Component, Alarms, etc. Put into the following form:
                        *  {xLabel: xLabel1, id: id, data: [{category: dataCategory1, count: count, value: value, valueSquared: valueSquared}, ...]},
                        */}
                      <Units records={filteredRecords} unit={unit} iterations={false}>
                        {(recordMap, yLabel) => (
                          /* Takes the filtered records from TestFilter and prepares them for the form that React-js Charts expects. */
                          <ChartDataPrep recordMap={recordMap} onClick={selectCommitFunction} selectedCommit={filteredSelect} xLabel={"Commit Hash"} yLabel={yLabel} meanbar={false} >
                            {(topCategories) => (
                              filteredSelect != null ?
                                <Paper>
                                  <br />
                                  {/* Extracts test records from the selected commits */}
                                  <Iterations testName={testName} selectedCommit={filteredSelect} count={10} >
                                      {(selectedRecords) => (
                                        <div>
                                          <Units records={selectedRecords} unit={unit} iterations={true}>
                                            {(recordMap, yLabel) => (
                                              <ChartDataPrep recordMap={recordMap} xLabel={"Iteration Timestamp"} yLabel={yLabel} meanbar={true} selectLabels={topCategories} />
                                            )}
                                          </Units>
                                          {/* <IterationTable records={selectedRecords} onClickDelete={deleteTestRecord} loading={loading}/> */}
                                          <IterationTable records={selectedRecords} currentBranch={currentBranch.branchName} count={count}/>
                                        </div>
                                      )}
                                  </Iterations>
                                </Paper> : null
                            )}
                          </ChartDataPrep>
                        )}
                      </Units>
                    </div>
                    )}
                  </TestFilter>
                  )}
                </Commits>
              )}
            </AnalysisTopbar>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: Types.State) => {
  return {
    openDrawer: state.openDrawer,
    projectId: state.project[state.common.curProjectIndex]._id,
    profile: state.profile,
  };
};

export default withRouter(
  withTheme(connect(mapStateToProps)(AnalysisContainer))
);
