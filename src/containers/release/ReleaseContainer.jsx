import React from "react";
// import { Bar as BarChart } from "react-chartjs-2";
// import { defaults } from "react-chartjs-2";
import ReleaseTopbar from "./ReleaseTopbar";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as Actions from "../../actions";
import { withRouter } from "react-router-dom";
import _ from "underscore";
import * as Util from "../../Util";
import Appbar from "../../components/Appbar";
import { withTheme } from '@material-ui/core/styles'
import CommitPrep from "../../components/CommitPrep"
import ChartDataPrep from "../../components/ChartDataPrep"

class ReleaseContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      count: '5',
      loading: false,
      initialStep: 0,
      stepsEnabled: this.props.tourStatus === "true" ? true : false,
      steps: [
        {
          element: '.topbarWrapper',
          intro: 'The prefix of the branch name is by default set to origin/release. Please click on the Edit Prefix button to change the value.',
        },
        {
          element: '.releaseTrend',
          intro: 'The comparison of the master branch with the release branches is displayed.',
        },
        {
          element: '#commitTrendDrawer',
          intro: 'Please click on the link to navigate to the commits trend page.',
        },
      ],
      flag : true,
      commitRecords: []
    };
    this.handleCountChange = this.handleCountChange.bind(this);
    this.onExit = this.onExit.bind(this);
    this.onComplete = this.onComplete.bind(this);
  }

  handleCountChange(count: number) {
    this.setState({ count })
  }

  onExit () {
    console.log("onexit in ReleaseTrend");
    if(this.state.stepsEnabled && this.state.flag){
      this.setState({ 
      stepsEnabled: false
    });
    this.props.actions.updateProjectStart("tourStatus","false", this.props.projectId, ()=>console.log("exit in ReleaseTrend updated tourStatus")); 
    }
  }

  onComplete() {
    console.log("oncomplete in ReleaseTrend");
    this.setState({ 
      flag: false
    });
    console.log("oncomplete in ReleaseTrend set flag to false");
  }

  static getDerivedStateFromProps(props: Props) {
    const masterBranch = Util.extractBranch(props.allBranches, "origin/master");
    const releaseBranches = Util.extractBranches(
      props.allBranches,
      props.prefix
    );
    Util.sortReleases(releaseBranches);
    let labels = _.map(releaseBranches, function(branch) {
      return branch.branchName;
    });
    let commitRecords = releaseBranches.flatMap((branch) => {
      let commits = Util.getNCommits(props.allCommits, branch.branchName, 1)
      if(commits.length > 0)
        return commits[0]
      return []
    });
    if(masterBranch != null) {
      labels.push(masterBranch.branchName);
      let commits = Util.getNCommits(props.allCommits, masterBranch.branchName, 1)
      if(commits.length > 0) {
        commitRecords.push(commits[0])
      }
    }
    return { commitRecords }
  }

  render() {
    console.log(this.state);
    // console.log(this.state.stepsEnabled+"In release trend");
    // console.log(this.props.tourStatus+"In release trend");
    // const data = {
    //   labels: this.state.labels,
    //   datasets: [
    //     {
    //       label: "Total Battery Drain",
    //       backgroundColor: this.props.theme.palette.graphFirst,
    //       borderColor: this.props.theme.palette.graphFirstBorder,
    //       borderWidth: 1,
    //       data: this.state.data
    //     }
    //   ]
    // };
    // defaults.font.size = this.props.theme.typography.chart.fontSize;
    // defaults.font.family = this.props.theme.typography.fontFamily;

    // const options = {
    //   responsive: true,
    //   maintainAspectRatio: false,
    //   scales: {
    //     xAxes: [
    //       {
    //         stacked: true,
    //         scaleLabel: {
    //           display: true,
    //           labelString: "Branches",
    //           fontSize: this.props.theme.typography.h3.fontSize
    //         }
    //       }
    //     ],
    //     yAxes: [
    //       {
    //         stacked: true,
    //         scaleLabel: {
    //           display: true,
    //           labelString: "Energy (uAh)",
    //           fontSize: this.props.theme.typography.h3.fontSize
    //         }
    //       }
    //     ]
    //   },
    //   legend: {
    //       labels: {
    //           fontSize: this.props.theme.typography.h3.fontSize
    //       }
    //   }
    // };

    return (
      <div>
      <Appbar pageTitle="Release Trend: Total Battery Drain per Release" id="releaseTrendTitle"/>
        <div className={this.props.openDrawer ? "openWrapper" : "closedWrapper"}>
        <div className="releaseTrend">
          <div className="topbarColWrapper">
            <ReleaseTopbar count={this.state.count} handleCountChange={this.handleCountChange} prefix={this.props.prefix} />
          </div>
          <CommitPrep commitRecords={this.state.commitRecords} xLabelIsBranchName={true}>
            {(recordMap) => (
              /* Takes the filtered records from TestFilter and prepares them for the form that React-js Charts expects. */
              <ChartDataPrep recordMap={recordMap} xLabel={"Commit Hash"} yLabel={"Energy (uAh)"} meanbar={false} />
            )}
          </CommitPrep>
          {/* <div className="chartColWrapper">
            <BarChart data={data} options={options} />
          </div> */}
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    allBranches: state.branch,
    allCommits: state.commit,
    prefix: state.project[state.common.curProjectIndex].prefix,
    releaseComparison: state.releaseComparison,
    tourStatus: state.project[state.common.curProjectIndex].tourStatus,
    projectId: state.project[state.common.curProjectIndex]._id,
    openDrawer: state.openDrawer
  };
};

const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators(Actions, dispatch)
  };
};

export default withRouter(
  withTheme(connect(mapStateToProps, mapDispatchToProps)(ReleaseContainer))
);
