import React from "react";
// import { orange, blue } from "@material-ui/core/colors";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import * as Actions from "../../actions";
import { Dialog, DialogTitle, DialogActions, Menu, MenuItem, Button, TextField } from "@material-ui/core";
import PopupState, { bindTrigger, bindMenu } from 'material-ui-popup-state'

class ReleaseTopbar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: 1,
      prefix: "",
      isEditPrefixOpen: false,
      prefixWarning: false
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleCountChange = this.handleCountChange.bind(this);
    this.openEditPrefixModal = this.openEditPrefixModal.bind(this);
    this.handlePrefixChange = this.handlePrefixChange.bind(this);
    this.handlePrefixSubmit = this.handlePrefixSubmit.bind(this);
  }


  openEditPrefixModal() {
    this.setState({
      isEditPrefixOpen: !this.state.isEditPrefixOpen
    });
  }
  handleClose() {
    this.setState({
      isEditPrefixOpen: false
    });
  }

  handleChange(e) {
    console.log("inside handleChange", e);
    this.props.actions.updateReleaseCount(e);
  }

  handlePrefixChange(e) {
    this.setState({
      prefix: e.target.value
    });
  }

  handlePrefixSubmit() {
    if (this.state.prefix.length !== 0)
      this.props.actions.updateProjectStart("prefix", this.state.prefix, this.props.curProject._id)
    this.setState({prefix: ""})
    this.handleClose()
  }

  handleCountChange(selectedCount: number, popupState: Object) {
    return (e: Object) => {
      this.props.handleCountChange(selectedCount)
      popupState.close()
    }
  }

  render() {
    // const styles = {
    //   errorStyle: {
    //     color: orange[500]
    //   },
    //   underlineStyle: {
    //     borderColor: orange[500]
    //   },
    //   floatingLabelStyle: {
    //     color: blue[500]
    //   },
    //   floatingLabelFocusStyle: {
    //     color: blue[500]
    //   }
    // };

    const EditPrefixButton = () => (
      <div>
        <Button
          variant="contained"
          onClick={this.openEditPrefixModal}
          color="primary">Edit Prefix</Button>
      </div>
    );

    const DropDownSelectField = () => (
      <div>
        {/**<IconButton
          tooltip="Older Releases"
          touch={true}
          tooltipPosition="bottom-right"
        >
          <LeftArrow />
        </IconButton>**/}
        <span>
          <PopupState variant="popover" popupId="count-popup">
            {(popupState) => (
              <React.Fragment>
                <Button variant="contained" {...bindTrigger(popupState)}>
                  {this.props.count}
                </Button>
                <Menu {...bindMenu(popupState)}>
                  {[5,10,15].map(number => (<MenuItem key={number} onClick={this.handleCountChange(number, popupState)}> {number} </MenuItem>))}
                </Menu>
              </React.Fragment>
            )}
          </PopupState>
        </span>
        {/**<IconButton
          tooltip="Newer Releases"
          touch={true}
          tooltipPosition="bottom-right"
        >
          <RightArrow />
        </IconButton>**/}
      </div>
    );

    const actions = [
          ];

    return (
      <div className="topbarWrapper" >
        <EditPrefixButton />
        {/* todo put the dialog as a component */}
        <Dialog
          actions={actions}
          open={this.state.isEditPrefixOpen}
          onClose={this.handleClose}
          aria-labelledby="prefix-title"
        >
        <DialogTitle id="prefix-title">Customize Prefix</DialogTitle>
          <TextField
            onChange={e => this.handlePrefixChange(e)}
            defaultValue={this.props.prefix}
            label="Release branch name prefix"
            variant="filled"
            ref="prefixSelector"
            helperText={this.state.prefixWarning && ("That prefix does not match any of your branches")}
          />
          
          <DialogActions>
            <Button key="cancel" color="primary" onClick={this.handleClose}>Cancel</Button>
            <Button
              key="submit" 
              color="primary"
              // keyboardFocused={true}
              onClick={this.handlePrefixSubmit}>Submit</Button>
          </DialogActions>

        </Dialog>
        <DropDownSelectField />
        {/* TODO: ADD SHARE FEATURE
          <span>Share</span>
          <IconButton
            tooltip="Share"
            touch={true}
            tooltipPosition="bottom-left"
          >
            <SocialShare color="#458bca" />
          </IconButton>
      */}
      </div>
    );
  }
}
const mapStateToProps = state => {
  return {
    releaseCount: state.releaseCount,
    openDrawer: state.openDrawer,
    curProject: state.project[state.common.curProjectIndex],
    allBranches: state.branch
  };
};

const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators(Actions, dispatch)
  };
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(ReleaseTopbar)
);
