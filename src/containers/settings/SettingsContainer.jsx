// @flow
import React from "react"
import { bindActionCreators } from "redux"
import * as Actions from "../../actions"
import { withRouter } from "react-router-dom"
import { TextField, Button, Dialog, Table, TableBody, TableRow, TableCell } from "@material-ui/core"
import moment from "moment"
import CommitUrlDialog from "../../containers/commit/CommitUrlDialog"
import { connect } from "react-redux"
import fetch from "isomorphic-fetch"
import Appbar from "../../components/Appbar"
import * as Types  from "../../types"

type Props = {
  ...Types.ReduxProps<typeof mapStateToProps, typeof mapDispatchToProps>
}

type State = {
  msgSubsribe: string,
  msgPassword: string,
  newPassword: string,
  oldPassword: string,
  openSubcribeComfirm: boolean,
  openChangePassword: boolean,
  openCommitUrlDialog: boolean
}

class SettingsContainer extends React.Component<Props, State> {
  constructor(props) {
    super(props)
    this.state = {
      msgSubsribe: "",
      data: {},

      msgPassword: "",
      newPassword: '',
      oldPassword: '',

      openSubcribeComfirm: false,
      openChangePassword: false,
      openCommitUrlDialog: false,
    }

    this.handleInputChange = this.handleInputChange.bind(this)
    this.onRequestChangePassword = this.onRequestChangePassword.bind(this)
    this.onRequestUnsubscribe = this.onRequestUnsubscribe.bind(this)
    this.onRequestUpgrade = this.onRequestUpgrade.bind(this)
  }


  /*:: handleInputChange: (SyntheticInputEvent<*>) => void */
  handleInputChange(evt: SyntheticInputEvent<*>) {
    this.setState({ [evt.target.name]: evt.target.value })
  }

  /*:: onRequestChangePassword: () => void */
  onRequestChangePassword() {
    var context = this
    var formData = new FormData()
    formData.append("password", this.state.oldPassword)
    console.log(formData)
    const init = {
      method: "POST",
      credentials: "same-origin", // necessary
      headers: {},
      body: formData
    }

    if (this.state.newPassword === this.state.oldPassword) {
      this.setState({ msgPassword: "new password is same as old password" })
      return
    }

    const url = `/api/profile/${this.props.profile.username}/password/${this.state.newPassword}`
    fetch(url, init)
      .then(response => {
        if (!response.ok) throw Error(response.statusText)
        return response
      })
      .then(function (response) {
        console.log(response)
        var token = context.props.profile.username + ':' + context.state.newPassword
        var hash = window.btoa(token)
        document.cookie = "eagleHash=" + hash + " Path=/"

        // clear
        context.setState({
          oldPassword: "",
          newPassword: "",
          msgPassword: "Successfully updated!",
          openChangePassword: false
        })
      })
      .catch(function () {
        context.setState({ msgPassword: "Password is unmatched, please check the password" })
      })
  }

  /*:: onRequestUnsubscribe: () => void */
  onRequestUnsubscribe() {
    console.log("unSubscription")
    var context = this
    const url = "api/payment/cancel"
    var request = new Request(url, {
      method: "POST",
      headers: new Headers()
    })

    fetch(request).then(function (res) {
      if (res.status >= 400) {
        res.text().then(t =>
          context.setState({
            msgSubsribe: t
          }))
      } else {
        context.setState({ msgSubsribe: "Successfully unsubscribed!", openSubcribeComfirm: false })
      }
    })
  }

  /*:: onRequestUpgrade: () => void */
  onRequestUpgrade() {
    let url = "https://tester.mobileenerlytics.com/pay.html"
    let win = window.open(url, '_blank')
    win.focus()    
  }

  render() {
    const {curProject, profile} = this.props
    let {role} = profile

    let { commitUrlPrefix } = curProject
    commitUrlPrefix = commitUrlPrefix == null ? "" : commitUrlPrefix
    const passwordActions = [
      <Button
        key="closePassword"
        variant="text"
        primary={true}
        onClick={() => {
          this.setState({
            openChangePassword: false,
            oldPassword: "",
            newPassword: "",
            msgPassword: ""
          })
        }}>Cancel</Button>,
      <Button
        key="update"
        variant="text"
        primary={true}
        keyboardFocused={true}
        onClick={this.onRequestChangePassword}>
        Update
      </Button>
    ]
    const subscribeActions = [
      <Button
        key="cancel"
        variant="text"
        primary={true}
        onClick={() => this.setState({ openSubcribeComfirm: false, msgSubsribe: "" })}>Cancel</Button>,
      <Button
        key="unsub"
        variant="text"
        primary={true}
        keyboardFocused={true}
        onClick={this.onRequestUnsubscribe}>Unsubscribe</Button>,
      <Button
        key="upgrade"
        variant="text"
        primary={true}
        keyboardFocused={true}
        onClick={this.onRequestUpgrade}>Upgrade</Button>      
    ]
    return (
      <div>
        <Appbar pageTitle="Settings" id="settingsTitle" />
        <div className={this.props.openDrawer ? "openWrapper" : "closedWrapper"} >
          <div className="settingsTableStyle">
            <Table>
              <TableBody displayRowCheckbox={false}>
              {profile.username &&
                <TableRow>
                  <TableCell>
                    <h3>Username:</h3>
                  </TableCell>
                  <TableCell>
                    <h3>{profile.username}</h3>
                  </TableCell>
                </TableRow>}
              {profile.role && <TableRow>
                  <TableCell>
                    <h3>User type:</h3>
                  </TableCell>
                  <TableCell>
                    <h3>{profile.role}</h3>
                  </TableCell>
                </TableRow>}
              {profile.firstname &&  <TableRow>
                  <TableCell>
                    <h3>Name:</h3>
                  </TableCell>
                  <TableCell>
                    <h3>{profile.firstname} {profile.lastname}</h3>
                  </TableCell>
                </TableRow>}
                <TableRow>
                  <TableCell>
                    <h3>Password:</h3>
                  </TableCell>
                  <TableCell style={{ display: "flex", justifyContent:"space-between", alignItems: "center"}}>
                    <h3> {'*******'} </h3>
                    <Button variant="contained" onClick={() => this.setState({ openChangePassword: true })}>Edit</Button>
                  </TableCell>
                </TableRow>
                <Dialog
                  title="Change password"
                  contentStyle={{ width: '40%' }}
                  actions={passwordActions}
                  modal={false}
                  open={this.state.openChangePassword}
                  onRequestClose={() => this.setState({ msgPassword: "" })}
                >
                  <TextField name="oldPassword" floatingLabelText="Old password" required={true} type="password" value={this.state.oldPassword} onChange={this.handleInputChange} />
                  <br />
                  <TextField name="newPassword" floatingLabelText="New password" required={true} type="password" pattern="[0-9a-fA-F]{4,8}" value={this.state.newPassword} onChange={this.handleInputChange} />
                  <span > {this.state.msgPassword} </span>
                </Dialog>

                <TableRow hidden= {role === "manual"}>
                  <TableCell >
                    <h3>Commit url:</h3>
                  </TableCell>
                  <TableCell style={{ display: "flex", justifyContent:"space-between", alignItems: "center" }}>
                    <h3> {commitUrlPrefix.length > 20 ? (commitUrlPrefix.substr(0,17) + "...") : commitUrlPrefix} </h3>
                    <Button variant="contained" style={{ marginLeft: "5px" }} onClick={() => this.setState({ openCommitUrlDialog: true })}>Edit</Button>
                  </TableCell>                    
                </TableRow>
                <CommitUrlDialog
                      open={this.state.openCommitUrlDialog}
                      onClose={() => this.setState({openCommitUrlDialog : false})}
                      value = {commitUrlPrefix.length === 0 ? "https://" : commitUrlPrefix}
                    />  
                {profile.email && <TableRow>
                  <TableCell>
                    <h3>Email:</h3>
                  </TableCell>
                  <TableCell>
                    <h3>{profile.email}</h3>
                  </TableCell>
                </TableRow>}
                {profile.organization && <TableRow>
                  <TableCell>
                    <h3>Organization:</h3>
                  </TableCell>
                  <TableCell>
                    <h3>{profile.organization}</h3>
                  </TableCell>
                </TableRow>}
                <TableRow>
                  <TableCell>
                    <h3>Registration Date:</h3>
                  </TableCell>
                  <TableCell>
                    <h3>
                      {moment(profile.registrationTime).format(
                        "MMM DD, YYYY"
                      )}
                    </h3>
                  </TableCell>
                </TableRow>
                <TableRow hidden= {role === "manual"}>
                  <TableCell>
                    <h3>Expiration date:</h3>
                  </TableCell>
                  <TableCell>
                    <h3>
                      {moment(profile.expirationTime).format(
                        "MMM DD, YYYY"
                      )}
                    </h3>
                  </TableCell>
                </TableRow>
              </TableBody>
            </Table>
            <br />
            { role === "manual" ? 
              (<Button variant="contained" onClick={this.onRequestUpgrade}>Upgrade</Button>) :
              (<Button variant="contained" hidden={true} onClick={() => this.setState({ openSubcribeComfirm: true })}>Unsubscribe</Button>)
            }
                          
            <Dialog
              title="Are you sure?"
              actions={subscribeActions}
              modal={false}
              open={this.state.openSubcribeComfirm}
              onRequestClose={() => this.setState({ openSubcribeComfirm: false, msgSubsribe: "" })}
            >
              <h3>We're sad to see you go.</h3>
              <ul>
                <li>Cancellation will be effective at the end of your current billing period on {moment(profile.expirationTime).format( "MMM DD, YYYY")}.</li>
                <li>Click "Unsubscribe" below to cancel your membership.</li>
              </ul>
              <span className="unsubscribeMessage"> {this.state.msgSubsribe} </span>
            </Dialog>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state: Types.State) => {
  return {
    openDrawer: state.openDrawer,
    profile: state.profile,
    curProject: state.project[state.common.curProjectIndex],
  }
}

const mapDispatchToProps = (dispatch: *) => {
  return {
    actions: bindActionCreators(Actions, dispatch)
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(SettingsContainer))
