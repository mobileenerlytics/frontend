// @flow
import React from "react";
import Ticker from "./Ticker";

type Prop = {
  dataMap: Map<string, number>,
}

class GainsLosses extends React.Component<Prop> {
  render() {
    const errorMsg = "No tests common to both commits."
    const noSigDifMsg = "No significant difference in energy use was found between these commits." 
    const {dataMap} = this.props
    if(dataMap.size === 0) {
      return (
        <div>
          <h3>
            {errorMsg}
          </h3>
        </div>
      )
    }

    // Find top 3 positive entries 
    const positive = Array.from(dataMap.entries()).filter(e => e[1] > 0.1).sort((e1, e2) => e1[1] - e2[1]).reverse().splice(0, 3)

    // Find top 3 negative entries 
    const negative = Array.from(dataMap.entries()).filter(e => e[1] < -0.1).sort((e1, e2) => e2[1] - e1[1]).reverse().splice(0, 3)
    if (positive.length === 0 && negative.length === 0) {
      return (
        <div>
          <h3>
            {noSigDifMsg}
          </h3>
        </div>
      )
    }
    return (
      <div>
          {positive.length > 0 &&
          <div className="gainHeader">
            <h4>Top Increase</h4>
          </div>
          }
          {positive.map(e => (
            <div key={e[0]} className="gainLossItem">
              <p className="gainLossText">{e[0]}</p>
              <Ticker value={e[1]} />
            </div>
          ))}
          {negative.length > 0 &&
          <div className="gainHeader gainHeaderLosses">
            <h4>Top Decrease</h4>
          </div>
          }
          {negative.map(e => (
            <div className="gainLossItem" key={e[0]}>
              <p className="gainLossText">{e[0]}</p>
              <Ticker value={e[1]} />
            </div>
          ))}
        </div>
      // </div>
    );
  }
}

export default GainsLosses;
