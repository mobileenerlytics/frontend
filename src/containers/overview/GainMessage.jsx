// @flow
import React from "react";

const GainMessage = () => (
	<div>
		<h4>Uh Oh!</h4>
		<p>
			View our <a href="http://mobileenerlytics.com/blog">blog</a> for
			tips on further optimizations to improve your apps battery life
		</p>
	</div>
);

export default GainMessage;