// @flow
import React, { useState } from "react"
import { connect } from "react-redux"
import { withRouter } from "react-router-dom"
import ComparisonSelection from "./ComparisonSelection"
import RecentActivity from "./RecentActivity"
import Appbar from "../../components/Appbar"
import * as Types from '../../types'
import * as OverviewUtil from "./OverviewUtil"
// import 'intro.js/introjs.css'

type Props = {
  ...Types.ReduxProp<typeof mapStateToProps>
}

const OverviewContainer = (props: Props) => {
  const [comparisonBranchMap: Map<string, Types.ComparisonType>] = useState(new Map())

  const getComparisonData = (firstCommit: ?Types.CommitType, secondCommit: ?Types.CommitType): ?OverviewUtil.ComparisonType => {
    if (firstCommit == null || secondCommit == null) 
      return null
    let comparisonKey = firstCommit._id + secondCommit._id
    let comparisonData = comparisonBranchMap.get(comparisonKey)
    if (comparisonData != null) 
      return comparisonData
    comparisonData = OverviewUtil.getComparison(firstCommit, secondCommit)
    comparisonBranchMap.set(comparisonKey, comparisonData)
    return comparisonData
  }

  return (
    <div>
      <Appbar pageTitle="Overview" id="overviewTitle" />
      <div className={props.openDrawer ? "grid" : "grid-open"}>
        <div className="grid-item master">
          <ComparisonSelection getComparisonData={getComparisonData} index={0} />
        </div>
        <div className="grid-item recentActivity">
          <RecentActivity />
        </div>
        <div className="grid-item latestRelease" style={{ "flex": "1" }}>
          <ComparisonSelection getComparisonData={getComparisonData} index={1} />
        </div>
        <div className="grid-item tests">
          <ComparisonSelection getComparisonData={getComparisonData} index={2} />
        </div>
        <div className="grid-item component">
          <ComparisonSelection getComparisonData={getComparisonData} index={3} />
        </div>
        <div className="grid-item thread">
          <ComparisonSelection getComparisonData={getComparisonData} index={4} />
        </div>
      </div>
    </div>
  )
}

const mapStateToProps = (state: Types.State) => {
  return {
    openDrawer: state.openDrawer,
  }
}

export default withRouter(
  connect(mapStateToProps)(OverviewContainer)
)
