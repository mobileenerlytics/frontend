// @flow
import React from "react"
import AsyncSelect from "react-select/async"
import Select from "react-select"
import CancelIcon from "@material-ui/icons/Clear"
import SaveButton from "@material-ui/core/Button"
import Switch from "@material-ui/core/Switch"
import { connect } from "react-redux"
import { withRouter } from "react-router-dom"
import { bindActionCreators } from "redux"
import * as Types from '../../types'
import * as Actions from "../../actions"

type Props = {
    ...Types.ReduxProps<typeof mapStateToProps, typeof mapDispatchToProps>,
    widgetProps: Types.WidgetType,
    savePrefs: (comparisonType: string, firstBranch: string, firstCommit: string, secondBranch: string, secondCommit: string, metric: string) => null,
    cancelEdit: () => void
}

type State = {
    comparisonType: string,
    firstBranchName: string,
    firstCommitHash: string,
    secondBranchName: string,
    secondCommitHash: string,
    comparisonMetric: string,
    loading: boolean
}

/** EditWidget is a window that shows in the widget component after the edit icon has been selected. It displays
 *  dropdowns to allow a user to choose the comparison type which commits to compare in the widget
 *  taken up by edit window. 
 * 
 * @param {function} savePrefs : function to save the current chosen widget preferences and close the edit window
 * @param {function} cancelEdit: function to ignore chosen widget preferences and close the edit window
 */
class EditWidget extends React.Component<Props, State> {
    constructor(props) {
        super(props)
        let widgetProps = props.widgetProps
        this.state = {
            comparisonType: widgetProps.type,
            firstBranchName: widgetProps.firstBranchName,
            firstCommitHash: widgetProps.firstCommitHash,
            secondBranchName: widgetProps.secondBranchName,
            secondCommitHash: widgetProps.secondCommitHash,
            selectedBranches: [widgetProps.firstBranchName, widgetProps.secondBranchName],
            selectedCommits: [widgetProps.firstCommitHash, widgetProps.secondCommitHash],
            comparisonMetric: widgetProps.metric,
            loading: false
        }
        this.loadOptions = this.loadOptions.bind(this)
    }

    static getDerivedStateFromProps(props, state) {
        if (!state.loading) return
        let firstCommits = props.commits.filter(commit => commit.branchName === state.firstBranchName)
        let secondCommits = props.commits.filter(commit => commit.branchName === state.secondBranchName)
        if (firstCommits.length === 0 || secondCommits.length === 0) return
        return { loading: false }
    }

    /*:: loadOptions: (string, string, any) => void */
    loadOptions(branchName: string, hashPrefix: string, callback): void {
        let fetchCallback = () => {
            let matchingCommits = this.props.commits.filter(commit => (commit.branchName === branchName && commit.hash.startsWith(hashPrefix)))
            callback(matchingCommits.map(commit => ({label: commit.hash, value: commit.hash})))
        }
        this.props.actions.fetchCommitsStart(5, fetchCallback, branchName, "", hashPrefix)
    }

    render() {
        const {comparisonType, firstBranchName, firstCommitHash, secondBranchName, secondCommitHash, comparisonMetric, loading } = this.state
        let firstCommitOptions = [
            {label: Types.DEFAULT_COMMIT_SELECTION, value: Types.DEFAULT_COMMIT_SELECTION},
            ...this.props.commits.filter(commit => commit.branchName === firstBranchName).map(commit => (
                {label: commit.hash, value: commit.hash}
            ))
        ]
        let secondCommitOptions = [
            {label: Types.DEFAULT_COMMIT_SELECTION, value: Types.DEFAULT_COMMIT_SELECTION},
            ...this.props.commits.filter(commit => commit.branchName === secondBranchName).map(commit => (
                {label: commit.hash, value: commit.hash}
            ))
        ]
        return (
            <div className="overview-grid-panel">
                <div className="overview-grid-panel-column-item">
                    <CancelIcon style={{ cursor:"pointer", display:"inline-flex", position: "absolute", right: "0", verticalAlign: "top", paddingRight: "10px", paddingTop: "10px", marginRight: "0", height: "20px", width: "20px", overflow: "visible", boxSizing: "content-box", }}
                                onClick={() => this.props.cancelEdit()} />
                    <h6 style={{marginTop:"2px", marginBottom:"1px"}}>Select comparison type</h6>
                    <div style={{display:"flex", flexDireciton: "row", justifyContent:"center", alignContent:"center"}}>
                        <div className="widgetSelectorStyle">
                            <Select
                                options={Types.comparisonTypes.map(ct => (
                                    {label: Types.getComparisonTypeDisplay(ct), value: ct}
                                ))}
                                isLoading={loading}
                                defaultValue={{label: Types.getComparisonTypeDisplay(comparisonType), value: comparisonType}}
                                onChange={(selectedComparison) => this.setState({comparisonType: selectedComparison.value})}
                            />

                        </div>
                        <div className="widgetSelectorStyle">
                            Energy
                            <Switch
                                checked={comparisonMetric === Types.POWER_METRIC}
                                color="default"
                                inputProps={{ 'aria-label': 'checkbox with default color' }}
                                onChange={(event) => this.setState({comparisonMetric: event.target.checked ? Types.POWER_METRIC : Types.ENERGY_METRIC})}
                            />
                            Power
                        </div>
                    </div>
                    <h6 style={{marginTop:"2px", marginBottom: "1px"}}>Select first branch and first commit</h6>
                    <div style={{display:"flex", flexDireciton: "row", justifyContent:"center", alignContent:"center"}}>
                        <div className="widgetSelectorStyle">
                            <Select
                                style={{marginLeft: "2px", marginRight: "2px"}}
                                options={this.props.allBranches.filter(branch => branch.commits.length > 0).map(branch => (
                                    {label: branch.branchName, value: branch.branchName}
                                ))}
                                isLoading={loading}
                                defaultValue={{label: firstBranchName, value: firstBranchName}}
                                onChange={(selectedBranch) => {
                                    this.setState({loading: true, firstBranchName: selectedBranch.label, firstCommitHash: Types.DEFAULT_COMMIT_SELECTION})
                                    this.props.actions.fetchCommitsStart(10, null, selectedBranch.label)
                                }}
                            />
                        </div>
                        <div className="widgetSelectorStyle">
                            <AsyncSelect
                                cacheOptions
                                isLoading={loading}
                                value={{label: firstCommitHash, value: firstCommitHash}}
                                defaultOptions={firstCommitOptions}
                                loadOptions={(hashPrefix, callback) => {this.loadOptions(firstBranchName, hashPrefix, callback)}}
                                onInputChange={() => {}}
                                onChange={(selectedCommit) => {this.setState({firstCommitHash: selectedCommit.label})}}
                            />
                        </div>
                    </div>
                    <h6 style={{marginTop:"1px", marginBottom: "1px"}}>Select second branch and second commit</h6>
                    <div style={{display:"flex", flexDireciton: "row", justifyContent:"center", alignContent:"center"}}>
                        <div className="widgetSelectorStyle">
                            <Select
                                options={this.props.allBranches.filter(branch => branch.commits.length > 0).map(branch => (
                                    {label: branch.branchName, value: branch.branchName}
                                ))}
                                isLoading={loading}
                                defaultValue={{label: secondBranchName, value: secondBranchName}}
                                onChange={(selectedBranch) => {
                                    this.setState({loading: true, secondBranchName: selectedBranch.label, secondCommitHash: Types.DEFAULT_COMMIT_SELECTION})
                                    this.props.actions.fetchCommitsStart(10, null, selectedBranch.label)
                                }}
                            />
                        </div>
                        <div className="widgetSelectorStyle">
                            <AsyncSelect
                                cacheOptions
                                value={{label: secondCommitHash, value: secondCommitHash}}
                                defaultOptions={secondCommitOptions}
                                loadOptions={(hashPrefix, callback) => {this.loadOptions(secondBranchName, hashPrefix, callback)}}
                                onInputChange={() => {console.log()}}
                                onChange={(selectedCommit) => {this.setState({secondCommitHash: selectedCommit.label})}}
                            />
                        </div>
                    </div>
                    <div style={{display:"flex", flexDireciton: "row", justifyContent:"center", alignContent:"center"}}>
                            <SaveButton variant="outlined" size="large" className="button" color="primary"
                                onClick={() => {
                                    this.props.savePrefs(comparisonType, firstBranchName, firstCommitHash, secondBranchName, secondCommitHash, comparisonMetric)
                                }}
                                style={{marginLeft:"8px", marginTop:"8px"}}
                            >
                                    Save Preferences
                            </SaveButton>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state: Types.State) => {
  return {
    allBranches: state.branch,
    commits: state.commit,
  }
}

const mapDispatchToProps = (dispatch: *) => {
    return {
        actions: bindActionCreators(Actions, dispatch),
    }
}

export default withRouter(
    connect(mapStateToProps, mapDispatchToProps)(EditWidget)
)
