// @flow
import tValue from "sample-sizer/lib/tvalue"
import * as Types from "../../types"

/* Constant for level of confidence needed to reject null hypothesis in T-Tests */
const confidenceLevel = .95

/**
 * Function to find maximum r such that Pr(mu_distribution1 > r*mu_distribution2) >= confidenceLevel
 * Done with binary search: 
 *    - first verify Pr(mu_distribution1 > mu_distribution2) >= confidenceLevel, if not return 0
 *    - start with r = distribution1.mean / distribution2.mean
 *         - if executeTTest: return r
 *         - else binary search r between max and min value possible 
 * 
 * @param {statsInfo} distribution1 - Mean, variance, and count of first distribution
 * @param {statsInfo} distribution2 - Mean, variance, and count of second distribution
 * 
 * if mu_distribution1 > mu_distribution2, 
 * @returns maximum r such that Pr(mu_distribution1 > r*mu_distribution2) >= confidenceLevel
 * else
 * @returns minimum r such that Pr(mu_distribution1 < r*mu_distribution2) >= confidenceLevel
 */
export function calculateRatio(distribution1: Types.StatsInfo, distribution2: Types.StatsInfo): number {
    if (distribution1.count < 2 || distribution2.count < 2)
        return 0
    if (distribution2.mean > distribution1.mean) {
        let ratio = calculateRatio(distribution2, distribution1)
        if (ratio === 0) return 0
        return 1 / calculateRatio(distribution2, distribution1)
    }
    if (!executeScaledTTest(distribution1, distribution2, confidenceLevel, 1))
        return 0
    let left = 1
    let right = distribution1.mean / distribution2.mean
    let ratio = (left + right) / 2
    while ((right - ratio) > .001) {
        if (!executeScaledTTest(distribution1, distribution2, confidenceLevel, ratio))
            right = ratio
        else
            left = ratio
        ratio = (right + left) / 2
    }
    return ratio
}

/**
 * Function to conduct a two tailed t test on two different distributions. 
 * null: mu_distribution1 - ratio*mu_distribution2 <= 0
 * alternative: mu_distribution1 - ratio*mu_distribution2 > 0
 * 
 * @param {statsInfo} distribution1 - Mean, variance, and count of first distribution
 * @param {statsInfo} distribution2 - Mean, variance, and count of second distribution
 * @param {number} confidence - number between 0 and 1 indicating level of confidence required to reject the null
 * @param {number} ratio - number to scale distribution 2 by 
 * 
 * @returns true if can reject the null with at least confidence and claim mu_distribution1 > mu_distribution2 
 * i.e. Pr(mean1 - mean2 > 0) >= confidence
 */
export function executeScaledTTest(distribution1: Types.StatsInfo, distribution2: Types.StatsInfo, confidence: number, ratio: number): boolean {
    let scaledDistribution2: Types.StatsInfo = {
        mean: distribution2.mean * ratio, 
        variance: distribution2.variance * Math.pow(ratio, 2),
        count: distribution2.count
    }
    let df = calculateDF(distribution1, scaledDistribution2)
    // tValue returns a tValue for a two tailed Student Distribution. Only need one tailed: turn .95 into .90. 
    let oneTailedConfidence = Math.round((1 - (1 - confidence) * 2) * 100) / 100
    if (!df || df < 1 || df > 200) return false
    let tThreshold = tValue(df, oneTailedConfidence) 
    let tScore = calculateTScore(distribution1, scaledDistribution2)

    return tScore > tThreshold
}

/** Function to calculate the t - score for a null hypothesis of mu_distribution1 - mu_distribution2 <= 0 */
export function calculateTScore(distribution1: Types.StatsInfo, distribution2: Types.StatsInfo): number {
    let varianceOvern1 = distribution1.variance / distribution1.count
    let varianceOvern2 = distribution2.variance / distribution2.count

    let numerator = distribution1.mean - distribution2.mean
    let denominator = Math.sqrt(varianceOvern1 + varianceOvern2)
    return numerator / denominator
}

/**
 * Function to calculate the Degrees of Freedom when conducting a two sample t test on two 
 * distributions
 * 
 * @param {statsInfo} distribution1 - Mean, variance, and count of first distribution
 * @param {statsInfo} distribution2 - Mean, variance, and count of second distribution 
 */
export function calculateDF(distribution1: Types.StatsInfo, distribution2: Types.StatsInfo): number {
    let varOvern1 = distribution1.variance / distribution1.count
    let varOvern2 = distribution2.variance / distribution2.count

    let numerator = Math.pow((varOvern1 + varOvern2), 2)
    let denominator = Math.pow(varOvern1, 2) / (distribution1.count - 1) + Math.pow(varOvern2, 2) / (distribution2.count - 1)
    return Math.round(numerator / denominator)
}

/**
 * Function to retrieve all commits in branch available for comparison.
 * 
 * @param {string} branchName - name of branch to retrieve commits of
 * @param {Array<Types.CommitType} branches - all commits from this account. 
 */
export function getBranchCommits(branchName: string, branches: Array<Types.BranchType>): Array<Types.CommitType>  {
    let branch = branches.find(branch => branch.branchName === branchName)
    if (branch == null) 
        return []
    return branch.commits
}
