// @flow
import React from "react";
import _ from "underscore";
import { withTheme } from '@material-ui/core/styles'
import type { Theme } from "@material-ui/core/styles/createMuiTheme"

type Prop = {
  theme: Theme,
  value: number
}

class Ticker extends React.Component<Prop> {
  render() {
  // console.log(this.props.value[0])
    const gainValue = {
      //flex: "auto",
      background: this.props.theme.palette.accent1Color,
      border: "1px solid black",
      width: "30%"
    };

    const lossValue = _.extend({}, gainValue, {
      background: this.props.theme.palette.primary1Color
    });
    // console.log(lossValue);
    if (this.props.value > 0)
      return <span style={gainValue}>+{this.props.value.toFixed(1)}%</span>;
    else return <span style={lossValue}>{this.props.value.toFixed(1)}%</span>;
  }
}

export default withTheme(Ticker);
