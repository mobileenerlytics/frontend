// @flow
import React from "react";

const LossMessage = () => (
	<div>
		<h4>Great Work!</h4>
		<p>You had an overall energy improvement, keep it up!</p>
	</div>
);

export default LossMessage;
