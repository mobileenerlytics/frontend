// @flow
import React from "react";

const NeutralMessage = () => (
  <div>
    <h4>Not enough data</h4>
    <p>
      Crunching the numbers yielded no net change. If you still need help
      integrating check out our 
      {/*<Link to="docs"> getting started </Link>*/}
      <a href="instructions.html"> getting started </a> 
      section or out <a href="https://mobileenerlytics.com/blog">blog</a>
    </p>
  </div>
);

export default NeutralMessage;
