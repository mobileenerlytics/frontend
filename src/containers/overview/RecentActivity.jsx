// @flow
import React from "react"
import TimeAgo from "react-timeago"
import ReprocessButton from "@material-ui/icons/Autorenew"
import { withRouter } from "react-router-dom"
import { connect } from "react-redux"
import { Link } from "react-router-dom"
import { bindActionCreators } from "redux"
import * as Types from '../../types'
import * as Actions from "../../actions"
import * as Util from "../../Util"

type Props = {
  ...Types.ReduxProps<typeof mapStateToProps, typeof mapDispatchToProps>,
  recentActivity: Array<Types.UploadType>,
}

/** Recent Activity displays the most recently uploaded traces to the current project. */
class RecentActivity extends React.PureComponent<Props> {
  render() {
    return (
      <div className="overview-grid-panel recent_activity_panel">
        <div className='overview_header'>
          <div className='recent_activity_flex_row'>
            <h3 className='recent_activity_title'>Recent Activity</h3>
          </div>
        </div>
        <div className="recent_activity_headers" >
          <span style={{ paddingLeft: "15px", minWidth: "30%", textAlign: "left" }}>Contributor</span>
          <span style={{ paddingLeft: "15px", minWidth: "20%", textAlign: "center" }}>Branch</span>
          <span style={{ minWidth: "20%", textAlign: "center" }}>App</span>
          <span style={{ minWidth: "10%", textAlign: "center" }}>Status</span>
          <span style={{ minWidth: "15%", textAlign: "center" }}>When</span>
        </div>
        <div className="recent_activity_list">
          {
            this.props.recentActivity.map((upload, i) => {
              return (
                <div key={upload.hash + i.toString()} className="recent_activity_item">
                  <span className="recent_activity_author">
                    {upload.authorEmail}
                  </span>
                  <span className="recent_activity_branch">
                    {upload.branchName.substring(0, 25)}
                  </span>
                  <Link to={`/commit?branchName=${upload.branchName}&selectedCommit=${upload.hash}`}>
                    <span className="recent_activity_hash" >
                      {upload.hash.substring(0, 25)}
                    </span>
                  </Link>
                  {/* <span className="recent_activity_tests">
                        {upload.testRecords == null ? 0 : upload.testRecords.length} tests
                      </span> */}
                  <span className={upload.jobStatus === "Done" ? "recent_activity_done" : "recent_activity_processing"}>
                    {upload.jobStatus === "Done" ? Util.msToTime(upload.jobDurationMs) : upload.jobStatus}
                  </span>
                  <span className="recent_activity_time">
                    <TimeAgo date={upload.uploadMs} />
                  </span>
                  <ReprocessButton onClick={() => {this.props.actions.reprocessUploadStart(upload)}} style={{ cursor:"pointer", display:"inline-flex", verticalAlign: "top", paddingRight: "10px", boxSizing: "content-box", color: "$color-first-darker"}} />
                </div>
              );
            })
          }
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: Types.State) => {
  return {
    recentActivity: state.upload
  }
}

const mapDispatchToProps = (dispatch: *) => {
  return {
      actions: bindActionCreators(Actions, dispatch)
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(RecentActivity));
