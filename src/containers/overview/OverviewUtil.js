// @flow
import React from "react"
import * as Util from "../../Util"
import * as Types from '../../types'
import * as ComparisonUtil from "./ComparisonUtil"

export type IntersectionType = {
  commonCount: number,
  masterBranchCount: number,
  releaseBranchCount: number,
}

export type ComparisonType = {
  percentChange: ?number,
  powerPercentChange: ?number,
  testDifference: Map<string, number>,
  testPowerDifference: Map<string, number>,
  componentsDifference: Map<string, number>,
  componentsPowerDifference: Map<string,number>,
  threadsDifference: Map<string, number>,
  threadsPowerDifference: Map<string, number>
}

export const steps: Array<Types.StepType> = [
      {
        element: '.master',
        intro: 'The total percentage increase or decrease in energy of the master branch as compared to the latest release is displayed. ',
      },
      {
        element: '.latestRelease',
        intro: 'The total percentage increase or decrease in energy of the latest release branch as compared to the previous release is displayed. ',
      },
      {
        element: '.tests',
        intro: 'The total percentage increase or decrease in energy of the individual tests of the master branch as compared to the latest release branch is displayed. ',
      },
      {
        element: '#releaseTrendDrawer',
        intro: 'Click on the link to navigate to the release trends page.',
      },
    ]


export function extractComparableTest(records: Array<Types.TestRecordType>): [Map<string, Types.StatsInfo>, Map<string, Types.StatsInfo>] {
  return [new Map([...records.map(r => [r.testName, {
    mean: r.energy,
    variance: r.energySquared - r.energy * r.energy,
    count: r.count
  }])]), new Map([...records.map(r => [r.testName, {
    mean: r.power,
    variance: r.powerSquared - r.power * r.power,
    count: r.count
  }])])]
}

export function getDifference(firstEnergy: number, firstData: Map<string, Types.StatsInfo>, 
                secondEnergy: number, secondData: Map<string, Types.StatsInfo> 
               ): Map<string, number> {
    let keys: Set<string> = new Set([...firstData.keys(), ...secondData.keys()])
    let differences: Map<string, number> = new Map()
    keys.forEach(key => {
      let other = {mean: 0, variance: 0, count: 0}
      let firstDist = Util.getOrElse(firstData, key, other)
      let secondDist = Util.getOrElse(secondData, key, other)
      let value = ComparisonUtil.calculateRatio(firstDist, secondDist)
      value = value > 0 ? (value - 1) * 100 : value 
      differences.set(key, value)
    })
    return new Map([...differences.entries()].sort());
  }

export function getComparison(firstCommit: ?Types.CommitType, secondCommit: ?Types.CommitType): ?ComparisonType {
    // return percentage, tests, threads, components
    // 1. create master and release arrays
    if (firstCommit == null || secondCommit == null)
      return

    const firstCommitTestNames: Set<string> = Util.extractTestNamesFromCommit(firstCommit)
    const secondCommitTestNames: Set<string> = Util.extractTestNamesFromCommit(secondCommit)

    // 2. create intersection set
    const commonTestNames: Set<string> = new Set([...firstCommitTestNames].filter(x => secondCommitTestNames.has(x)));

    if (commonTestNames.size === 0) {
      return
    }

    const firstBranchCommonTests: Array<Types.TestRecordType> = firstCommit.testRecordSet.filter(t => commonTestNames.has(t.testName))
    const secondBranchCommonTests: Array<Types.TestRecordType> = secondCommit.testRecordSet.filter(t => commonTestNames.has(t.testName))

    const firstBranchEnergyStats: [Types.StatsInfo, Types.StatsInfo] = Util.sumEnergy(firstBranchCommonTests)
    const secondBranchEnergyStats: [Types.StatsInfo, Types.StatsInfo] = Util.sumEnergy(secondBranchCommonTests)
    const firstBranchEnergy: number = firstBranchEnergyStats[0].mean
    const secondBranchEnergy: number = secondBranchEnergyStats[0].mean
    let ratio = ComparisonUtil.calculateRatio(firstBranchEnergyStats[0], secondBranchEnergyStats[0]) 
    const percentChange: number = ratio > 0? (ratio - 1)*100 : 0

    const firstBranchPower: number = firstBranchEnergyStats[1].mean
    const secondBranchPower: number = secondBranchEnergyStats[1].mean
    let powerRatio = ComparisonUtil.calculateRatio(firstBranchEnergyStats[1], secondBranchEnergyStats[1]) 
    const powerPercentChange: number = powerRatio > 0? (powerRatio - 1)*100 : 0

    const firstBranchComps: [Map<string, Types.StatsInfo>, Map<string, Types.StatsInfo>] = Util.sumComponents(firstBranchCommonTests)
    const secondBranchComps: [Map<string, Types.StatsInfo>, Map<string, Types.StatsInfo>] = Util.sumComponents(secondBranchCommonTests)

    const firstBranchThreads: [Map<string, Types.StatsInfo>, Map<string, Types.StatsInfo>] = Util.sumThreads(firstBranchCommonTests)
    const secondBranchThreads: [Map<string, Types.StatsInfo>, Map<string, Types.StatsInfo>] = Util.sumThreads(secondBranchCommonTests)

    const firstBranchTests: [Map<string, Types.StatsInfo>, Map<string, Types.StatsInfo>] = extractComparableTest(firstBranchCommonTests)
    const secondBranchTests: [Map<string, Types.StatsInfo>, Map<string, Types.StatsInfo>] = extractComparableTest(secondBranchCommonTests)

    const testDifference: Map<string, number> = getDifference(
      firstBranchEnergy,
      firstBranchTests[0],
      secondBranchEnergy,
      secondBranchTests[0]
    )

    const testPowerDifference: Map<string, number> = getDifference(
      firstBranchPower,
      firstBranchTests[1],
      secondBranchPower,
      secondBranchTests[1]
    )

    const componentsDifference: Map<string, number> = getDifference(
      firstBranchEnergy,
      firstBranchComps[0],
      secondBranchEnergy,
      secondBranchComps[0],
    )

    const componentsPowerDifference: Map<string, number> = getDifference(
      firstBranchPower,
      firstBranchComps[1],
      secondBranchPower,
      secondBranchComps[1]
    )

    const threadsDifference: Map<string, number> = getDifference(
      firstBranchEnergy,
      firstBranchThreads[0],
      secondBranchEnergy,
      secondBranchThreads[0],
    )

    const threadsPowerDifference: Map<string, number> = getDifference(
      firstBranchPower,
      firstBranchThreads[1],
      secondBranchPower,
      secondBranchThreads[1]
    )

    return {
      percentChange,
      powerPercentChange,
      testDifference,
      testPowerDifference,
      componentsDifference,
      componentsPowerDifference,
      threadsDifference,
      threadsPowerDifference
    }
  }

/**
 * Returns the corresponding dataMap of a specific comparisonType from comparisonData.
 * @param {string} comparisonType - Type of comparison : Branch, Test, Component, Thread
 * @param {string} metric - which metric (power or energy) used to compare across commits
 * @param {ComparisonType} comparisonData - ComparisonType returned by getComparison
 */
export function getComparisonDataMap(comparisonType: string, metric: string, comparisonData: ComparisonType): Map<string, number> {
  if (!comparisonData) return new Map()
  switch (comparisonType) {
    case Types.TEST_COMPARISON:
        return metric === Types.POWER_METRIC? comparisonData.testPowerDifference: comparisonData.testDifference
    case Types.COMPONENT_COMPARISON:
        return metric === Types.POWER_METRIC? comparisonData.componentsPowerDifference: comparisonData.componentsDifference
    case Types.THREAD_COMPARISON:
        return metric === Types.POWER_METRIC? comparisonData.threadsPowerDifference: comparisonData.threadsDifference
    default:
        return new Map()
  }
}

/**
 * Creates the description for a specific comparisonType to be displayed in the information ToolTip
 * @param {string} comparisonType - Branch, Test, Component, Thread
 * @param {Types.CommitType} firstCommit - Commit who's enrgy is the  numerator in ratio
 * @param {Types.CommitType} secondCommit - commit who's energy is the denominator in ratio
 */
export function getComparisonDescription(comparisonType: string, firstCommit: Types.CommitType, secondCommit: Types.CommitType, metric: string) {
  let metricDescription = (metric === Types.ENERGY_METRIC) ? "total energy" : "average power"
  switch (comparisonType) {
    case Types.TEST_COMPARISON:
      return <React.Fragment>Ratios of the {metricDescription} of common tests of <em>{firstCommit.branchName}/{firstCommit.hash}</em> commit to <em>{secondCommit.branchName}/{secondCommit.hash}</em> commit. <br /> Ratios calculated with 95% significance.</React.Fragment>
    case Types.COMPONENT_COMPARISON:
      return <React.Fragment>Ratios of the {metricDescription} per component of <em>{firstCommit.branchName}/{firstCommit.hash}</em> commit to <em>{secondCommit.branchName}/{secondCommit.hash}</em> commit across all common tests. <br /> Ratios calculated with 95% significance.</React.Fragment>
    case Types.THREAD_COMPARISON:
      return <React.Fragment>Ratios of the total {metricDescription} per thread of <em>{firstCommit.branchName}/{firstCommit.hash}</em> commit to <em>{secondCommit.branchName}/{secondCommit.hash}</em> commit across all common tests. <br /> Ratios calculated with 95% significance.</React.Fragment>
    default:
      return <React.Fragment>Ratio of the total {metricDescription} of <em>{firstCommit.branchName}/{firstCommit.hash}</em> commit to the total energy of <em>{secondCommit.branchName}/{secondCommit.hash}</em> commit across all common tests. <br /> Ratio calculated with 95% significance.</React.Fragment>
  }
}
