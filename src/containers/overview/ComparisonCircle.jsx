// @flow
import React from "react"
import { Box } from "@material-ui/core"
import { Link, withRouter } from "react-router-dom"
import { connect } from "react-redux"
import GainMessage from "./GainMessage"
import LossMessage from "./LossMessage"
import NeutralMessage from "./NeutralMessage"
import { withTheme } from '@material-ui/core/styles'
import type { Theme } from "@material-ui/core/styles/createMuiTheme"

type Prop = {
  firstBranch: string,
  secondBranch: string,
  value: number,
  errorMsg: string,
  theme: Theme
}

class ComparisonCircle extends React.PureComponent<Prop> {
  render() {
    const {value} = this.props
    // value is null or undefined
    let errMsg = (value == null) ? "No tests common to both commits." : "No significant difference in energy use was found between these commits."
    if(value == null || value === 0)
      return (        
        <div>
          <h3>
            {errMsg}
          </h3>
        </div>
      )

    // value is available
    const circleStyle = {
      background:
        value < 0
          ? this.props.theme.palette.primary1Color 
          : this.props.theme.palette.accent1Color
    }
    return (
      <div>
          <div className="overview-grid-panel-column-item">
            <div className="overviewPercentChangeFlex">
              <Link to="/commit">
                <Box
                  style={circleStyle}
                  className="circleStyle"
                  elevation={3}
                  borderRadius="50%"
                >
                  <h4>
                    {value
                      ? value.toFixed(1) + "%"
                      : "n/a"}
                  </h4>
                </Box>
              </Link>
              <span className="comparisonMessage">
                {/* eslint-disable */}
                {value === 0 ? (
                  <NeutralMessage />
                ) : value > 0 ? (
                  <GainMessage />
                ) : (
                  <LossMessage />
                )}
                {/* eslint-enable */}
              </span>
            </div>

        </div>
      </div>
    )
  }
}

export default withTheme(
  withRouter(connect(null, null)(ComparisonCircle))
)
