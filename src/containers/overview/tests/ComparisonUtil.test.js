// @flow
import * as ComparisonUtil from "../ComparisonUtil"
import * as Types from "../../../types"

describe('Comparison Util Test', () => {
    let distribution1: Types.StatsInfo = {
        mean: 1100,
        variance: 900,
        count: 20
    }
    let distribution2: Types.StatsInfo = {
        mean: 1700,
        variance: 4900,
        count: 20
    }

    let distribution3: Types.StatsInfo = {
        mean: 1788,
        variance: 1788*1788,
        count: 20 
    }
    
    let distribution4: Types.StatsInfo = {
        mean: 1650,
        variance: 165/2*165/2,
        count: 20
    }

    let distribution5: Types.StatsInfo = {
        mean: 1650 + 165,
        variance: 165/2 * 165/2,
        count: 20
    }

    let distribution6: Types.StatsInfo = {
        mean: 1650 + 165, 
        variance: 165/2 * 165/2,
        count: 1
    }

    it('calculateDf should compute correct Degrees of Freedom', () => {
        // expected DF found using formula for degrees of freedom for a two sample t test
        expect(ComparisonUtil.calculateDF(distribution1, distribution2)).toEqual(26)
    }),

    it('calculateTScore should compute correct T score', () => {
        // t-score = (distribution1.mean - distribution2.mean) / sqrt(distribution1.variance/distribution1.count + distribution2.variance/distribution2.count)
        expect(ComparisonUtil.calculateTScore(distribution2, distribution1)).toEqual(600 / Math.sqrt(290))
    }),

    it('calculateRatio should not compute ratio when the variance is too large', () => {
        expect(ComparisonUtil.calculateRatio(distribution3, distribution4)).toEqual(0)
    }),

    it('calculateRatio should not compute ratio when the distributions are equivalent', () => {
        expect(ComparisonUtil.calculateRatio(distribution3, distribution3)).toEqual(0)
    }),

    it('calculateRatio should return -1 if the sample size is too small', () => {
        expect(ComparisonUtil.calculateRatio(distribution5, distribution6)).toEqual(0)
    }),

    it('calculateRatio should never return a value greater than the division of the means', () => {
        let distribution7 = {mean: 100, variance: 1, count: 1000}
        let distribution8 = {mean: 110, variance: 1, count: 1000}
        expect(ComparisonUtil.calculateRatio(distribution8, distribution7)).toBeLessThan(1.1)
    }),

    it('calculateRatio should not compute ratio when the variance is too large', () => {
        // distribution5 mean is 10% higher than distribution4 and each's deviation is half the distance between the two
        expect(ComparisonUtil.calculateRatio(distribution5, distribution4)).toBeGreaterThan(1.05)
    }),

     it('calculateRatio should compute the same ratio regardless of ', () => {
        // distribution5 mean is 10% higher than distribution4 and each's deviation is half the distance between the two
        expect(ComparisonUtil.calculateRatio(distribution5, distribution4)).toBeGreaterThan(1.05)
    }),

    it('calculateRatio should compute a ratio less than one if the first distribution is smaller than second', () => {
        // distribution5 mean is 10% higher than distribution4 and each's deviation is half the distance between the two
        expect(ComparisonUtil.calculateRatio(distribution4, distribution5)).toBeLessThan(.95)
    })
})