// @flow
// import * as Types from "../../../types"
describe('What if analysis', () => {
  it('making tester happy', () => {
  })
  var ComparisonUtil = require("../ComparisonUtil")
  var fs = require("fs")
  var contents = fs.readFileSync("/Users/abhilash/Downloads/state.json")
  var data = JSON.parse(contents)

  const ORIG_BRIGHT = 0.38

  function percent(floatNum) {
    return (100 * floatNum).toFixed(2) + "%"
  }

  function whatIfAnalysis ( WHAT_IF_BRIGHT ) {
    const RGB000 = 136.25

    // Array of record summaries
    // summary contains
    // {
    //   testName: string,
    //   commitHash: string,
    //   count: number,
    //   SCREEN: {
    //     mean: number,
    //     sq_mean: number,
    //     variance: number
    //   },
    //   Total: {
    //     mean: number,
    //     sq_mean: number,
    //     variance: number
    //   }
    // }
    const recordSummary = new Object()

    // console.log(data)
    data.records.forEach(record => {
      record.testRecords.forEach(tr => {
        var compEnergy = tr.componentUnit.reduce((accum, tce) => {
          var energy = tce.energy
          if(tce.name === "SCREEN" && tce.energy > 0) {
            var avgCurrent = (tce.energy)/(tr.testDurationMS/3600)
            var frameCurrent = (avgCurrent-RGB000)/ORIG_BRIGHT + RGB000
            var whatIfCurrent = (frameCurrent-RGB000)*WHAT_IF_BRIGHT + RGB000
            energy = whatIfCurrent * (tr.testDurationMS/3600)
            // console.log(tce, avgCurrent, frameCurrent, whatIfCurrent, energy)
          }
          if(tce.name in accum)
            accum[tce.name] += energy
          else
            accum[tce.name] = energy
          // console.log(accum, tce)
          accum.Total += energy
          return accum
        }, {"Total": 0})
        // console.log(tr._id, tr.testName, tr.commitHash, compEnergy)
        const key = tr.commitHash + ":" + tr.testName
        if(key in recordSummary) {
          var summary = recordSummary[key]
          summary.Total.mean = (summary.Total.mean * summary.Total.count + compEnergy.Total) / (summary.Total.count + 1) 
          summary.Total.sq_mean = (summary.Total.sq_mean * summary.Total.count + compEnergy.Total * compEnergy.Total) / (summary.Total.count + 1) 
          summary.Total.variance = (summary.Total.sq_mean - summary.Total.mean * summary.Total.mean)
          summary.Total.count++

          summary.SCREEN.mean = (summary.SCREEN.mean * summary.SCREEN.count + compEnergy.SCREEN) / (summary.SCREEN.count + 1) 
          summary.SCREEN.sq_mean = (summary.SCREEN.sq_mean * summary.SCREEN.count + compEnergy.SCREEN * compEnergy.SCREEN) / (summary.SCREEN.count + 1)
          summary.SCREEN.variance = (summary.SCREEN.sq_mean - summary.SCREEN.mean * summary.SCREEN.mean)
          summary.SCREEN.count++

          recordSummary[key] = summary
        } else {
          recordSummary[key] = {
            testName: tr.testName,
            commitHash: tr.commitHash,
            SCREEN: {
              mean: compEnergy.SCREEN,
              sq_mean: compEnergy.SCREEN * compEnergy.SCREEN,
              variance: 0,
              count: 1
            },
            Total: {
              mean: compEnergy.Total,
              sq_mean: compEnergy.Total * compEnergy.Total,
              variance: 0,
              count: 1
            }
          }
        }
        // console.log(recordSummary)
      })
    })


    function cov (stat) {
      return percent(Math.sqrt(stat.variance) / stat.mean)
    } 
    
    // console.log(recordSummary)
    console.log("Testname", "TotalSavings", "ScreenSavings", "CoVDark", "CoVLight", "PercentScreenDark", "PercentScreenLight")
    function metrics(name, darkTotal, lightTotal, darkScreen, lightScreen) {
      console.log(name, percent(1 - ComparisonUtil.calculateRatio(darkTotal, lightTotal)), 
        percent(1 - ComparisonUtil.calculateRatio(darkScreen, lightScreen)),
        cov(darkTotal), cov(lightTotal), 
        percent(darkScreen.mean / darkTotal.mean), percent(lightScreen.mean / lightTotal.mean))
        // ?? percent(ComparisonUtil.calculateRatio(darkScreen, darkTotal)), percent(ComparisonUtil.calculateRatio(lightScreen, lightTotal)))

      // process.stdout.write(" " + name + " " + percent(1 - ComparisonUtil.calculateRatio(darkTotal, lightTotal)))
      // process.stdout.write(" " + name + " " + percent(1 - ComparisonUtil.calculateRatio(darkScreen, lightScreen)))
    }
    metrics("Chrome", recordSummary['Chrome-75.0-DarkMode:ChromeDarkModeTest'].Total, recordSummary['Chrome-75.0-LIghtMode:ChromeDarkModeTest'].Total, recordSummary['Chrome-75.0-DarkMode:ChromeDarkModeTest'].SCREEN, recordSummary['Chrome-75.0-LIghtMode:ChromeDarkModeTest'].SCREEN)
    metrics("Calendar", recordSummary['Calendar-6.0-DarkMode:CalendarTest'].Total, recordSummary['Calendar-6.0-LightMode:CalendarTest'].Total, recordSummary['Calendar-6.0-DarkMode:CalendarTest'].SCREEN, recordSummary['Calendar-6.0-LightMode:CalendarTest'].SCREEN)
    metrics("GoogleNews", recordSummary['GoogleNews-5.12-DarkMode:GoogleNewsReadingTest'].Total, recordSummary['GoogleNews-5.12-LightMode:GoogleNewsReadingTest'].Total, recordSummary['GoogleNews-5.12-DarkMode:GoogleNewsReadingTest'].SCREEN, recordSummary['GoogleNews-5.12-LightMode:GoogleNewsReadingTest'].SCREEN)
    metrics("YoutubeBrowsing", recordSummary['YouTube-14.25-DarkMode:YouTubeDarkModeTest'].Total, recordSummary['YouTube-14.25-LightMode:YouTubeDarkModeTest'].Total, recordSummary['YouTube-14.25-DarkMode:YouTubeDarkModeTest'].SCREEN, recordSummary['YouTube-14.25-LightMode:YouTubeDarkModeTest'].SCREEN)
    metrics("YoutubeLandscape", recordSummary['YouTube-14.31-DarkMode:YouTubeLandscapeVideoTest-tracereduceFix'].Total, recordSummary['YouTube-14.31-LightMode:YouTubeLandscapeVideoTest-tracereduceFix'].Total, recordSummary['YouTube-14.31-DarkMode:YouTubeLandscapeVideoTest-tracereduceFix'].SCREEN, recordSummary['YouTube-14.31-LightMode:YouTubeLandscapeVideoTest-tracereduceFix'].SCREEN)
    metrics("YoutubePortrait", recordSummary['YouTube-14.31-DarkMode:YouTubePortraitVideoTest-tracereduceFix'].Total, recordSummary['YouTube-14.31-LightMode:YouTubePortraitVideoTest-tracereduceFix'].Total, recordSummary['YouTube-14.31-DarkMode:YouTubePortraitVideoTest-tracereduceFix'].SCREEN, recordSummary['YouTube-14.31-LightMode:YouTubePortraitVideoTest-tracereduceFix'].SCREEN)
  }
  whatIfAnalysis(ORIG_BRIGHT)
  // for(var i = 0.2; i <=1 ; i += 0.1) {
    // whatIfAnalysis(i)
    // process.stdout.write(" " + percent(i) + "\n")
  // }
})