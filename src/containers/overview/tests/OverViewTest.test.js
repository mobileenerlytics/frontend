// @flow
import * as Util from "../../../Util"
import * as Types from '../../../types'
import * as TestUtil from "../../../components/tests/TestUtil"
import * as ComparisonUtil from "../ComparisonUtil"

describe('Comparison Util Test', () => {
    // want deviation 10% of the mean 
    // want deviation of 1 variance = 101 - 10*10
    let unit1 = TestUtil.mockUnitType("CPU", 10, 101)
    // want deviation of 1.5, variance of 2.25 = 227.25 - 225
    let unit2 = TestUtil.mockUnitType("CPU", 15, 227.25)
    // want deviation of 1.25, variance of 1.5626
    let unit3 = TestUtil.mockUnitType("CPU", 12.5, 157.8125)
    // want deviation of 1.5, variance of 2.25
    let unit4 = TestUtil.mockUnitType("CPU", 15, 227.25)
    // want deviation of 1
    let unit5 = TestUtil.mockUnitType("CPU", 10, 101)
    // want deviation of 2, variance of 4
    let unit6 = TestUtil.mockUnitType("CPU", 20, 404)
    let unitArray1: Array<Types.UnitType> = [unit1, unit2]
    // unitArray2's aggregate will have a mean that is 10% higher than unitArray1's
    let unitArray2: Array<Types.UnitType> = [unit3, unit4]
    // unitArray3's aggregate will have a mean that is 20% higher than unitArray1's
    let unitArray3: Array<Types.UnitType> = [unit5, unit6]
    
    let statsMap1 = Util.aggregateUnits(unitArray1, 10)
    let statsMap2 = Util.aggregateUnits(unitArray2, 10)
    let statsMap3 = Util.aggregateUnits(unitArray3, 10)


    it('For aggregated distributions with 10% deviations for all components and a 10% difference in aggregated means, there should be a statistical difference for a count of 10', () => {
        let statInfo1 = Util.getOrElse(statsMap1, "CPU", {mean: 0, variance: 0, count:0})
        let statInfo2 = Util.getOrElse(statsMap2, "CPU", {mean: 0, variance: 0, count:0})
        expect(ComparisonUtil.calculateRatio(statInfo2, statInfo1)).toBeGreaterThan(0)
    }),

    it('For aggregated distributions with 10% deviations for all components and a 20% difference in aggregated means', () => {
        let statInfo1 = Util.getOrElse(statsMap1, "CPU", {mean: 0, variance: 0, count:0})
        let statInfo3 = Util.getOrElse(statsMap3, "CPU", {mean: 0, variance: 0, count: 0})
        expect(ComparisonUtil.calculateRatio(statInfo3, statInfo1)).toBeGreaterThan(1.05)
    })
})