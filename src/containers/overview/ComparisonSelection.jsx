// @flow
import React from "react"
import GainsLosses from "./GainsLosses"
import ComparisonCircle from "./ComparisonCircle"
import EditWidget from "./EditWidget"
import { Create, InfoOutlined } from "@material-ui/icons"
import { Tooltip } from "@material-ui/core"
import * as Types from '../../types'
import * as OverviewUtil from "./OverviewUtil"
import * as Actions from "../../actions"
// import { withStyles } from '@material-ui/core/styles';
import { connect } from "react-redux"
import { bindActionCreators } from "redux"
import { Link, withRouter } from "react-router-dom"

type Props = {
    ...Types.ReduxProps<typeof mapStateToProps, typeof mapDispatchToProps>,
    getComparisonData: (firstCommit: ?Types.CommitType, secondCommit: ?Types.CommitType) => OverviewUtil.ComparisonType,
    index: number
}

type State = {
    widgetProps: Types.WidgetType,
    editing: boolean,
    loading: boolean,
}

/** Class in charge of displaying the correct type of widget based on the type of comparison 
 *  chosen and which Commits are selected to compare. 
 * 
 *  @param {number} index : identifies the index of this widget. Used to retrieve correct properties 
 *                          stored for this widget
 *  @param {function} getComparisonData : Function used to generate comparisonData between two commits. ComparisonSelection
 *                                        does not keep this in case the comparison being run has already been done by another
 *                                        widget. Avoids double work. 
 * 
 * Manages EditWidget, the screen that appears when user selects to edit the widget and calls the put API to update
 * the stored widget properties (comparisonType, branches, commits) when user chooses to save preferences.
 */
class ComparisonSelection extends React.Component<Props, State> {
    constructor(props) {
        super(props)
        let widgetProps: Types.WidgetType = props.widgets[props.index]
        if (widgetProps.firstBranchName === "" && props.allBranches.length > 0) {
            widgetProps.firstBranchName = props.allBranches[0].branchName
        }
        if (widgetProps.secondBranchName === "" && props.allBranches.length > 0) {
            widgetProps.secondBranchName = props.allBranches[0].branchName
        }
        this.state = {
            widgetProps,
            editing: false,
            loading: false,
        }
        this.savePrefs = this.savePrefs.bind(this)
        this.cancelEdit = this.cancelEdit.bind(this)
    }       

    /* Check that both commits this Widget uses are found. If not then need to fetch them. */
    componentDidMount() {
        let widgetProps = this.state.widgetProps
        let errorCallback = (success: boolean) => {
            if (!success) {
                this.setState({loading: false})
            }
        }
        let firstCommit = ComparisonSelection.findCommit(this.props.commits, widgetProps.firstBranchName, widgetProps.firstCommitHash)
        let secondCommit = ComparisonSelection.findCommit(this.props.commits, widgetProps.secondBranchName, widgetProps.secondCommitHash)
        // Do not fetch if commits already found
        if (firstCommit != null && secondCommit != null) return
        this.setState({loading: true})
        this.props.actions.fetchCommitsStart(10, errorCallback, widgetProps.firstBranchName, widgetProps.firstCommitHash)
        this.props.actions.fetchCommitsStart(10, errorCallback, widgetProps.secondBranchName, widgetProps.secondCommitHash)
    }

    /* Execution comes here after redux state updated. Widget will see results of any fetches made in ComponentWillMount. 
       If widget is loading then check to see if commits it is waiting for have been fetched. */
    static getDerivedStateFromProps(props: Props, state: State) {
        let { loading, widgetProps } = state
        if (!loading) return state
        let firstCommit = ComparisonSelection.findCommit(props.commits, widgetProps.firstBranchName, widgetProps.firstCommitHash)
        let secondCommit = ComparisonSelection.findCommit(props.commits, widgetProps.secondBranchName, widgetProps.secondCommitHash)
        if (firstCommit != null && secondCommit != null) {
            return {loading: false}
        }
        return state
    }

    /**
     * Finds commit with a specific commit hash and branch from an array of commits. If commitHash is Default option, returns
     * first commit off commits in branch.
     * @param {Array<Types.CommitType>} commits - Array of commits to to search through
     * @param {string} branchName - branch desired commit on
     * @param {string} commitHash - Hash of desired commit
     */
    /*:: findCommit: (Array<Types.CommitType>, string, string) => ?Types.CommitType */
    static findCommit(commits: Array<Types.CommitType>, branchName: string, commitHash: string): ?Types.CommitType {
        let branchCommits = commits.filter(commit => commit.branchName === branchName)
        if (branchCommits.length === 0) return null
        if (commitHash === Types.DEFAULT_COMMIT_SELECTION || commitHash === "") return branchCommits[0]
        return branchCommits.find(commit => commit.hash === commitHash)
    }

    /*:: cancelEdit: () => void */
    cancelEdit() {
        this.setState({editing: false})
    }
    
    /*:: savePrefs: (string, string, string, string, string) => void */
    savePrefs(comparisonType: string, firstBranchName: string, firstCommitHash: string, secondBranchName: string, secondCommitHash: string, metric: string): void {
        let widget: Types.WidgetType = {type: comparisonType, firstBranchName, secondBranchName, firstCommitHash, secondCommitHash, metric}
        let callback = (success) => {
            if (!success) {
                this.cancelEdit()
                return
            }
            this.setState({
                widgetProps: widget,
                editing: false
            })
        }
        this.props.actions.updateWidget(this.props.index, widget, callback)
    }

    render() {
        const { widgetProps, editing, loading } = this.state
        const errorMessage = (widgetProps.firstBranchName === "" || widgetProps.secondBranchName === "") ? "Please upload data to a branch to enable comparisons." :
                                    "Please upload data from \"" + widgetProps.firstBranchName + "\" branch and \"" + widgetProps.secondBranchName + "\" branch to enable this comparison."
 
        if (editing) return <EditWidget widgetProps={widgetProps}
                                        cancelEdit={this.cancelEdit}
                                        savePrefs={this.savePrefs}/>
        if (loading) return (
                <div className="overview-grid-panel">
                    <div className="overview-grid-panel-column-item">
                        <div className="widget_header">
                            <div className="overview-header-link">             
                            </div>
                        </div>
                        <h3>
                            loading...
                        </h3>
                    </div>
                    <Create style={{ cursor:"pointer", display:"inline-flex", position: "absolute", right: "0", verticalAlign: "top", paddingRight: "10px", paddingTop: "5px", marginRight: "0", height: "20px", width: "20px", overflow: "visible", boxSizing: "content-box", }}
                              onClick={() => this.setState({editing: true, loading: false})} />
                </div>)
        // const LightTooltip = withStyles(theme => ({
        //     tooltip: {
        //         backgroundColor: theme.palette.common.white,
        //         color: 'rgba(0, 0, 0, 0.87)',
        //         boxShadow: theme.shadows[1],
        //         fontSize: 13,
        //     },
        // }))(Tooltip);
        let firstCommit = ComparisonSelection.findCommit(this.props.commits, widgetProps.firstBranchName, widgetProps.firstCommitHash)
        let secondCommit = ComparisonSelection.findCommit(this.props.commits, widgetProps.secondBranchName, widgetProps.secondCommitHash)
        let title = Types.getComparisonTypeDisplay(widgetProps.type, widgetProps.metric)
        if (firstCommit == null || secondCommit == null)
            return (
                <div className="overview-grid-panel">
                    <div className="overview-grid-panel-column-item">
                        <div className="widget_header">
                            <div className="overview-header-link">             
                                <h3 style={{marginTop:"0"}}>{title}</h3>
                            </div>
                        </div>
                        <h3>
                            {errorMessage}
                        </h3>
                    </div>
                    <Create style={{ cursor:"pointer", display:"inline-flex", position: "absolute", right: "0", verticalAlign: "top", paddingRight: "10px", paddingTop: "5px", marginRight: "0", height: "20px", width: "20px", overflow: "visible", boxSizing: "content-box", }}
                              onClick={() => this.setState({editing: true})} />
                </div>)

        let comparisonData = this.props.getComparisonData(firstCommit, secondCommit)
        // value is used in ComparisonCircle
        let value = undefined
        if (comparisonData) value = widgetProps.metric === Types.POWER_METRIC ? comparisonData.powerPercentChange: comparisonData.percentChange 
        // dataMap is used in GainsLosses
        let dataMap = OverviewUtil.getComparisonDataMap(widgetProps.type, widgetProps.metric, comparisonData)
        let gainsComponent = (<GainsLosses dataMap={dataMap} />)
        let circleComponent = (<ComparisonCircle value={value} />)

        let description = OverviewUtil.getComparisonDescription(widgetProps.type, firstCommit, secondCommit, widgetProps.metric)
        return (
            <div className="overview-grid-panel">
                <div className="overview-grid-panel-column-item">
                    <div className="widget_header">
                        <div className="overview-header-link">             
                            <h3 style={{marginTop:"0"}}>{title}
                                <Tooltip title={description} >
                                    <InfoOutlined className="material-icons" style={{fontSize: "18px", display:"inline-block" }}/>
                                </Tooltip>
                            </h3>
                        </div>
                        <Create style={{ cursor:"pointer", display:"inline-flex", position: "absolute", right: "0", verticalAlign: "top", paddingRight: "10px", paddingTop: "5px", marginRight: "0", height: "20px", width: "20px", overflow: "visible", boxSizing: "content-box", }}
                                  onClick={() => this.setState({editing: true})}
                />
                    </div>
                    <h6 style={{marginTop:"2px"}}>
                        <Link to= {`/analysis?branchName=${firstCommit.branchName}&count=10&selectedCommit=${firstCommit.hash}`}>
                            {firstCommit.hash}
                        </Link>
                        &nbsp;on&nbsp;
                        <Link to= {`/commit?branchName=${firstCommit.branchName}`}>
                            {firstCommit.branchName}
                        </Link>
                        &nbsp;branch to&nbsp;
                        <Link to= {`/analysis?branchName=${secondCommit.branchName}&count=10&selectedCommit=${secondCommit.hash}`}>
                            {secondCommit.hash}
                        </Link>
                        &nbsp;on&nbsp;
                        <Link to= {`/commit?branchName=${secondCommit.branchName}`}>
                            {secondCommit.branchName}
                        </Link> branch
                    </h6>
                        {widgetProps.type === Types.BRANCH_COMPARISON? circleComponent : gainsComponent }
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state: Types.State) => {
    return {
        allBranches: state.branch,
        commits: state.commit,
        widgets: state.project[state.common.curProjectIndex].widgets,
    }
}

const mapDispatchToProps = (dispatch: *) => {
    return {
        actions: bindActionCreators(Actions, dispatch),
    }
}

export default withRouter(
    connect(mapStateToProps, mapDispatchToProps)(ComparisonSelection)
)
