// @flow
import React from 'react'
import * as Util from "../Util";
import * as Types from '../types'

/**
 * Filters commits based on a testName passed to it. Passes filtered commits 
 * to Units
 * @param {string} testName - Name of test wish to filter 
 * @param {Array<Types.CommitType>} commitRecords - Array of selected Commits on the current branch
 */
type Props = {
    commitRecords: Array<Types.CommitType>,
    testName: string,
    selectedCommit: ?Types.CommitType,
    children: (Array<Types.TestRecordType>, ?Types.CommitType) => Object,
}

class TestFilter extends React.PureComponent<Props> {
    /*:: filterSelectedCommit: (?Types.CommitType, string) => ?Types.CommitType */
    filterSelectedCommit(selectedCommit: ?Types.CommitType, testName: string): ?Types.CommitType {
        if (selectedCommit == null) return selectedCommit
        if (selectedCommit.testRecordSet.find(tr => tr.testName === testName)) return selectedCommit
        return null
    }

    render() {
        const { commitRecords, testName, selectedCommit } = this.props
        // filter commits for testRecords by testName and provide a label
        let filteredRecords = []
        commitRecords.forEach(commit => {
            let commitRecord = Util.findTestRecord(commit, testName)
            if (commitRecord == null) return
            commitRecord.commitHash = commit.hash
            commitRecord.commitId = commit._id
            filteredRecords.push(commitRecord)
        })
        if (filteredRecords === []) return <div />
        let filteredSelect = this.filterSelectedCommit(selectedCommit, testName)
        return (
            <div>
                {this.props.children(filteredRecords, filteredSelect)}
            </div>
        )
    }
}

export default TestFilter 
