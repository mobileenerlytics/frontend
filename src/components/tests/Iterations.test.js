// @flow
import React from "react"
import * as Types from "../../types"
import * as TestUtil from "./TestUtil"
import {shallow} from "enzyme"
import {Iterations} from "../Iterations"

describe('Iterations Tests', () => {
  const testName: string = "testName"
  let commitId = "selectedCommit"
  const count: number = 5

  const selectedCommit = TestUtil.mockCommitType(commitId, [])
  const selectedRecord = TestUtil.mockTestRecord(commitId, testName)
  const unSelectedRecord = TestUtil.mockTestRecord("unselectedRecord", "incorrectTestName")
    
  const records: Array<Types.RecordsType> = [
    {commitId, testName: "incorrectTestName", testRecords: [unSelectedRecord]},
    {commitId: "unSelectedCommit", testName, testRecords: [unSelectedRecord]},
    {commitId, testName, testRecords: [selectedRecord]},
  ]

  let props: Object = {
    actions: {fetchIterationsStart: () => {}} ,
    testName,
    selectedCommit,
    count,
    records
  }
  let testRecords: Array<Types.TestRecordType>
  let iterationsWrapper
  beforeEach(() => {
    iterationsWrapper = shallow(
      <Iterations {...props}> 
        {tRecords => (
          testRecords = tRecords
        )} 
      </Iterations>)
  })

  // Would like to say expect(filtered).toBe([matchingRecord]) but a === falsehood.
  // Think this is because in TestFilter, commitId is assigned to the filtered TestRecord
  it('Iterations should select TestRecords associated with correct commit.', () => {
    iterationsWrapper.setProps(props)
    expect(testRecords.length).toBe(1)
    expect(testRecords[0]).toBe(selectedRecord)
  })

  it('Iterations should not select any TestRecords associated with commit that does not exist.', () => {
    expect(iterationsWrapper.instance().getSelectedCommitTestRecords(records, "none", "none")).toEqual([])
  })

})