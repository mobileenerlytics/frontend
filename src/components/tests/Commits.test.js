// @flow
import React from "react"
import * as Types from "../../types"
import { shallow } from "enzyme"
import {Commits} from "../Commits"
import * as TestUtil from "./TestUtil"

describe('Commits Tests', () => {
    jest.spyOn(URLSearchParams.prototype, 'get').mockImplementation((key) => key)
    const currentBranchName: string = "currentBranch"
    const count: number = 2

    let currentCommit1 = TestUtil.mockCommitType("commit1", [])
    currentCommit1.branchName = currentBranchName
    let currentCommit2 = TestUtil.mockCommitType("commit2", [])
    currentCommit2.branchName = currentBranchName
    let currentCommit3 = TestUtil.mockCommitType("commit3", [])
    currentCommit3.branchName = "noBranch"
    let currentCommit4 = TestUtil.mockCommitType("commit4", [])
    currentCommit4.branchName = "noBranch"

    const allCommits: Array<Types.CommitType> = [currentCommit1, 
                                                 currentCommit2,
                                                 currentCommit3,
                                                 currentCommit4]
                                                
    let props: Object = {
        actions: {fetchCommitsStart: () => {}} ,
        allCommits,
        currentBranchName,
        count,
        location: {search : null}
    }

    let branchCommits: Array<Types.CommitType> = []
    let component
    beforeEach(() => {
        component = shallow(
            <Commits {...props}>
                {(selectedCommits) => (
                    branchCommits = selectedCommits
                )}
            </Commits>)
    })

   it('Commits should pass down commits from current branch.', () => {
        component.setProps({allCommits})
        expect(branchCommits).toEqual([currentCommit2, currentCommit1])
        expect(branchCommits.length).toEqual(2)
        expect(branchCommits.includes(currentCommit1)).toBe(true)
        expect(branchCommits.includes(currentCommit2)).toBe(true)
    })
})