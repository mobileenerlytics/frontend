// @flow
import React from "react"
import { shallow } from "enzyme"
import UrlManager from "../UrlManager"
import * as TestUtil from "./TestUtil"

describe('UrlManager Tests', () => {
    const testName: string = "testName"
    const branchName: string = "currentBranch"
    const count: number = 10
    const hash: string = "selectedCommit"
    let history = []

    const selectedCommit = TestUtil.mockCommitType(hash, [])

    let props: Object = {
        branchName,
        testName,
        count,
        selectedCommit,
        history
    }

    const firstExpectedUrl: string = `?branchName=${branchName}&testName=${testName}&count=${count.toString()}&selectedCommit=${hash}`
    const secondExpectedUrl: string = `?branchName=${branchName}&count=${count.toString()}&selectedCommit=${hash}`

    it('Url history should be updated with branchName, testName, count and selected Hash', () => {
        shallow(<UrlManager {...props} />)
        expect(history).toEqual([firstExpectedUrl])
    }),

    it('Url should not include testName if it is null as it will be in CommitContainer', () => {
        props.testName = null
        shallow(<UrlManager {...props} />)
        expect(history).toEqual([firstExpectedUrl, secondExpectedUrl])
    })

})