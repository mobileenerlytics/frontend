// @flow
import * as Types from "../../types"
import * as UnitUtil from "../UnitUtil"
import * as TestUtil from "./TestUtil"

describe('UnitUtil Tests', () => {
  const unit: Types.Unit = UnitUtil.units[0]
  let commitId1 = "randomHash1"
  let commitId2 = "randomHash2"

  const componentUnit = [{processName: null, name: "CPU", energy: 200, energySquared: 40000},
                      {processName: null, name: "GPU", energy: 100, energySquared: 10000},
                      {processName: null, name: "NETWORK", energy: 50, energySquared: 2500}]
  let componentUnitRecord = TestUtil.mockTestRecord(commitId1, "testName") 
  componentUnitRecord.componentUnit = componentUnit
  let threadUnitRecord = TestUtil.mockTestRecord(commitId2, "testName")

  const records: Array<Types.TestRecordType> = [componentUnitRecord, threadUnitRecord]

  const expectedComponentData: Array<Types.RecordMapDataType> = [ TestUtil.mockRecordMapData("CPU", 200, 40000, 10),
                                                                  TestUtil.mockRecordMapData("GPU", 100, 10000, 10),
                                                                  TestUtil.mockRecordMapData("NETWORK", 50, 2500, 10)]
  const expectedDataMap: Array<Types.RecordMapType> = [{xLabel: commitId1, id: componentUnitRecord._id, commitId: commitId1, data: expectedComponentData}, 
                                                       {xLabel: commitId2, id: threadUnitRecord._id, commitId: commitId2, data: []}]
  
  it('componentUnit should retrieve correct values from componentUnitRecord', () => {
    expect(unit.extractData(componentUnitRecord, 10)).toEqual(expectedComponentData)
  }),

  it('buildDataMap should get data from ComponentUnit but not ThreadUnit', () => {
      expect(UnitUtil.buildDataMap(records, unit, false)).toEqual(expectedDataMap)
  })

})