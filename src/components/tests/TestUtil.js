// @flow
import * as Types from "../../types"

export function mockUnitType(unitName: string, value: number, valueSquared: number): Types.UnitType {
    return {
        processName: unitName, 
        name: unitName,
        energy: value,
        energySquared: valueSquared
    }
}

export function mockTestRecord(commitHash: string, testName: string): Types.TestRecordType {
    return {
        _id: "ffffffffffffffffffffffff",
        count: 10,
        energy: 100,
        energySquared: 10000,
        testName,
        updatedMs: 1500000000000,
        threadCompEnergies: [],
        events: [],
        componentUnit: [],
        threadUnit: [],
        commitId: commitHash,
        commitHash,
    }
}

export function mockCommitType(hash: string, testRecordSet: Array<Types.TestRecordType>): Types.CommitType {
    return {
        _id: hash,
        hash,
        desc: hash,
        contributor: { email: "support@mobileenerlytics.com", name: "Moby" },
        branchName: "currentBranch",
        updatedMs: 1500000000000,
        jobStatus: null,
        jobDurationMs: 0,
        testRecordSet
    }
}

export function mockRecordMapData(category: string, value: number, valueSquared: number, count: number): Types.RecordMapDataType {
    return {
        category,
        value,
        valueSquared,
        count
    }
}

export function mockChartDataSet(label: string, data: Array<number>): Types.ChartDatasetType {
    return {
        label,
        xLabels: [],
        borderColor: "#fff",
        backgroundColor: "#fff",
        borderWidth: [],
        counts: [],
        data,
        deviations: [],
        intervals: [],
        id: [],
        visible: true,
        errorBars: []
    }
}

export function mockBranchType(branchName: string) {
    return {
        _id: branchName,
        branchName,
        updatedMs: 1500,
        commits: [],
        tests: []
    }
}