// @flow
import React from "react"
import * as Types from "../../types"
import * as TestUtil from "./TestUtil"
import CommitPrep from "../CommitPrep"
import {shallow} from "enzyme"

describe('CommitPrep Tests', () => {
    const testRecord1 = TestUtil.mockTestRecord("testRecord1", "testName1")
    const testRecord2 = TestUtil.mockTestRecord("testRecord2", "testName2")
    const commitRecord1 = TestUtil.mockCommitType("commit1", [testRecord1, testRecord2])
    const testRecord3 = TestUtil.mockTestRecord("testRecord3", "testName3")
    const testRecord4 = TestUtil.mockTestRecord("testRecord4", "testName4")
    const commitRecord2 = TestUtil.mockCommitType("commit2", [testRecord3, testRecord4])
    const commitRecords: Array<Types.CommitType> = [commitRecord1, commitRecord2]

    const commit1RecordMap: Types.RecordMapType = {
        xLabel: "commit1".substring(0, 8),
        id: "commit1",
        commitId: "commit1",
        data: [{ category: "testName1",
                 count: 10,
                 value: 100, 
                 valueSquared: 10000 },
               { category: "testName2", 
                 count: 10, 
                 value: 100,
                 valueSquared: 10000 }]
    }
    const commit2RecordMap: Types.RecordMapType = {
        xLabel: "commit2".substring(0, 8),
        id: "commit2",
        commitId: "commit2",
        data: [{ category: "testName3",
                 count: 10,
                 value: 100, 
                 valueSquared: 10000 },
               { category: "testName4", 
                 count: 10, 
                 value: 100,
                 valueSquared: 10000 }]
    }
    const expectedDataMap: Array<Types.RecordMapType> = [commit1RecordMap, commit2RecordMap]

    let dataMap: Array<Types.RecordMapType>
    beforeEach(() => {
        shallow(<CommitPrep commitRecords={commitRecords} >
            {commitPrepDataMap => (
                dataMap = commitPrepDataMap
            )} 
        </CommitPrep>)
    })

    it('CommitPrep should create correct dataMap for mulitple commits', () => {
        expect(dataMap).toEqual(expectedDataMap)
    })

})