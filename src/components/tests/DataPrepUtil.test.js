// @flow
import * as Types from "../../types"
import * as DataPrepUtil from "../DataPrepUtil"
import * as TestUtil from "./TestUtil"
import tValue from "sample-sizer/lib/tvalue"

describe('UnitUtil Tests', () => {
    const recordMap: Array<Types.RecordMapType> = [{
        xLabel: "testEntry1", 
        id: "testEntry1", 
        commitId: "testEntry1",
        data: [TestUtil.mockRecordMapData("CPU", 40, 1650, 5),
               TestUtil.mockRecordMapData("GPU", 30, 950, 5),
               TestUtil.mockRecordMapData("NETWORK", 20, 450, 5),
               TestUtil.mockRecordMapData("othercategory", 10, 100, 1)]
    }]

    const datasets: Array<Types.ChartDatasetType> = [TestUtil.mockChartDataSet("CPU", [1, 2, 3, 4]),
                                                     TestUtil.mockChartDataSet("GPU", [1, 2, 3, 4]),
                                                     TestUtil.mockChartDataSet("NETWORK", [1, 2, 3, 4])]
    const testLabels = new Set(["CPU", "GPU", "other"])

    it('filterDataByLabels should filter out anything not in labels and create an other category', () => {
        let filteredData: Array<Types.RecordMapDataType> = DataPrepUtil.filterDataByLabels(recordMap[0].data, testLabels)
        let expectedData: Array<Types.RecordMapDataType> = [TestUtil.mockRecordMapData("CPU", 40, 1650, 5), 
                                                            TestUtil.mockRecordMapData("GPU", 30, 950, 5),
                                                            TestUtil.mockRecordMapData("other", 30, 0, 1)]
        // other always has a valueSquared of 0 and count of 1 because we do not show errorBars on other.
        expect(filteredData).toEqual(expectedData)
    }),

    it('buildAndFilterCategoryLabels should include all categories when filtering on a number greater than the number of categories.', () => {
        let labels = DataPrepUtil.buildAndFilterCategoryLabels(recordMap, 10)
        expect(labels).toEqual(new Set(["CPU", "GPU", "NETWORK", "othercategory"]))
    }),

    it('buildAndFilterCategoryLabels should only include the two highest valued categories.', () => {
        let labels = DataPrepUtil.buildAndFilterCategoryLabels(recordMap, 2)
        expect(labels).toEqual(testLabels)
    }),

    it('calcStats should calculate correct statistics from given data.', () => {
       let stats = DataPrepUtil.calcStats(testLabels, datasets) 
       // The calcStats function is directly tested here. Because testLabels does not include Network 
       // Network's entries should be ignored. 
       // mean =((1 + 1) + (2 + 2) + (3 + 3) + (4 + 4)) / 4 = 5
       // expected(x^2) = ((1+1)^2 + (2 + 2)^2 + (3 + 3)^2 + (4 + 4)^2) / 4  = 30
       // deviation = sqrt(30 - 5*5)
       let expectedStats = {
           mean: 5,
           deviation: Math.sqrt(5)
       }
       expect(stats).toEqual(expectedStats)
    }),

    it('calcConfInt should calculate correct confidence interval for given data.', () => {
        let calcInterval = DataPrepUtil.calcConfInt(recordMap[0].data, "CPU")
        let expectedInterval = tValue(4, .95)*Math.sqrt(10)
        expect(calcInterval).toEqual(expectedInterval)
    }),

    it('getValueOfUnit should return correct value from dataMap', () => {
        expect(DataPrepUtil.getValueOfUnit(recordMap[0].data, "CPU")).toEqual(40)
    }),

    it('getCountOfUnit should return correct value from dataMap', () => {
        expect(DataPrepUtil.getCountOfUnit(recordMap[0].data, "CPU")).toEqual(5)
    }),

    it('getDeviation should return correct value from dataMap', () => {
        expect(DataPrepUtil.getDeviationOfUnit(recordMap[0].data, "CPU")).toEqual(Math.sqrt(50))
    }),

    it('buildChartDatasets should build datasets correctly', () => {
        let datasets = DataPrepUtil.buildChartDatasets(recordMap, testLabels, "#000", null)
        // datasets should have entry for "Show All", "Hide All", "CPU", "GPU", and "other"
        expect(datasets.length).toEqual(5)
        let cpuDataset = datasets.filter(dataset => dataset.label == "CPU")
        let gpuDataset = datasets.filter(dataset => dataset.label == "GPU")
        let otherDataset = datasets.filter(dataset => dataset.label == "other")
        let networkDataset = datasets.filter(dataset => dataset.label == "NETWORK")
        // datasets should have GPU, CPU, and other, but not NETWORK
        expect(cpuDataset.length).toEqual(1)
        expect(gpuDataset.length).toEqual(1)
        expect(otherDataset.length).toEqual(1)
        expect(networkDataset.length).toEqual(0)
        expect(cpuDataset[0].data[0]).toEqual(40)
        expect(gpuDataset[0].data[0]).toEqual(30)
        expect(otherDataset[0].data[0]).toEqual(30)
        // conf_interval = t*(deviation / sqrt(n))
        // n = 5
        // t* = tValue(degrees_freedom, level)
        // deviation = sqrt(1650 - 40*40) = sqrt(50)
        // deviation / sqrt(n) = sqrt(10)
        let interval = tValue(4, .95)*Math.sqrt(10)
        expect(cpuDataset[0].errorBars).toEqual({"testEntry1": {plus: interval, minus: -interval}})
    })
})