// @flow
import React from "react"
import * as Types from "../../types"
import { shallow } from "enzyme"
import TestFilter from "../TestFilter"
import * as TestUtil from "./TestUtil"

describe('TestFilter Tests', () => {
    const testName: string = "matchingTestName"
    const selectedTestName: string = "unMatchingTestName"
    const selectedCommitTestRecord = TestUtil.mockTestRecord("selectedCommitRecord", selectedTestName)
    const matchingRecord = TestUtil.mockTestRecord("matchingRecord", testName)
    const unMatchingRecord = TestUtil.mockTestRecord("unMatchingRecord", selectedTestName)

    const selectedCommit = TestUtil.mockCommitType("selectedCommit", [selectedCommitTestRecord])
    const testCommit = TestUtil.mockCommitType("testCommit", [matchingRecord, unMatchingRecord])

    const commitRecords: Array<Types.CommitType> = [selectedCommit, testCommit]

    let props: Object = {
        commitRecords,
        testName,
        selectedCommit,
    }

    let testFilterWrapper
    let filtered: Array<Types.TestRecordType>
    beforeEach(() => {
        testFilterWrapper = shallow(
            <TestFilter {...props}>
                {(filteredRecords) => (
                    filtered = filteredRecords
                )}
            </TestFilter>)
    })

    it('Selected Commit should be filtered out when it does not have a recordSet with the filtered testName.', () => {
        expect(testFilterWrapper.instance().filterSelectedCommit(selectedCommit, testName)).toEqual(null)
    })

    it('Selected Commit should not be filtered out when it has a recordSet with the filtered testName.', () => {
        expect(testFilterWrapper.instance().filterSelectedCommit(selectedCommit, selectedTestName)).toEqual(selectedCommit)
    })

    // Would like to say expect(filtered).toBe([matchingRecord]) but a === falsehood.
    // Think this is because in TestFilter, commitId is assigned to the filtered TestRecord
    it('TestFilter should filter only for testRecords that have correct testName.', () => {
        expect(filtered.length).toBe(1)
        expect(filtered[0]).toBe(matchingRecord)
    })

})