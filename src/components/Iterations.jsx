// @flow
import React from 'react'
import { withTheme } from '@material-ui/core/styles'
import * as Actions from "../actions"
import * as Types from '../types'
import { Divider } from '@material-ui/core'
import { connect } from "react-redux"
import { bindActionCreators } from "redux"
import { LinearProgress } from '@material-ui/core'

/**
 * Component to fetch Iterations based on selectedCommit and TestName. Sends an 
 * array of TestRecordTypes to Units to be rendered in a Chart.
 * 
 * @param {string} testName - Name of current displayed Test
 * @param {Types.CommitType} selectedCommit - ID of selectedCommit to fetch iteration records
 * @param {number} count - number of Iterations to fetch / render in chart
 */
type Props = {
    testName: string,
    selectedCommit: Types.CommitType,
    count: number,
    ...Types.ReduxProps<typeof mapStateToProps, typeof mapDispatchToProps>,
    children: (?Array<Types.TestRecordType>) => Object,
}

type State = {
    selectedRecords: Array<Types.TestRecordType>,
    loading: boolean,
    selectedCommit: Types.CommitType,
    testName: string
}

export class Iterations extends React.PureComponent<Props, State> {
    constructor(props: Object) {
        super(props)
        this.state = {
            loading: false,
            selectedRecords: [],
            selectedCommit: props.selectedCommit,
            testName: props.testName,
        }
    }

    componentDidMount() {
        Iterations.fetchRecordsAPI(this.props)
    }

    /** Fetch if a new commit was selected or a new test was selected. Otherwise update records. */
    static getDerivedStateFromProps(props: Props, state: State) {
        if (props.selectedCommit._id !== state.selectedCommit._id || props.testName !== state.testName) {
            Iterations.fetchRecordsAPI(props)
            return {
                selectedCommit: props.selectedCommit,
                testName: props.testName
            }
        }
        let selectedRecords = Iterations.getSelectedCommitTestRecords(props.records, props.selectedCommit._id, props.testName)
        return { selectedRecords }
    }

    /* API call to fetch records for a specific commit. */
    static fetchRecordsAPI(props: Props): void {
        let {selectedCommit, testName, count} = props
        props.actions.fetchIterationsStart(selectedCommit, testName, count)
    }

    /* First finds the commit with an id that matches the selectedID. Then extracts testRecord for it 
     * with corresponding testName and retrieves its records. */
    /*:: getSelectedCommitTestRecords: (Array<Types.RecordsType>, string, string) => Array<Types.TestRecordType> */
    static getSelectedCommitTestRecords(records: Array<Types.RecordsType>, commitId: string, testName: string): Array<Types.TestRecordType> {
        let selectedCommitRecord = records.find(rs => rs.commitId === commitId && rs.testName === testName)
        if (!selectedCommitRecord) return []
        // If there are records, add a hash
        return selectedCommitRecord.testRecords
    }

    render() {
        const { selectedCommit, recordStatus } = this.props
        const { selectedRecords } = this.state
        return (
            <div>
                <div style={{maxHeight: '6px', minHeight: '6px'}}>
                    {recordStatus === Types.STATUS_LOADING ? <LinearProgress/> : <Divider />}
                </div>
                <h3 className="text-center">
                    Iterations of Commit {selectedCommit.hash}
                </h3>
                {selectedRecords != null ?
                    this.props.children(selectedRecords) : null}
            </div>
        )
    }
}

const mapStateToProps = (state: Types.State) => {
    return {
        records: state.records,
        recordStatus: state.recordStatus
    }
}

const mapDispatchToProps = (dispatch: *) => {
    return {
        actions: bindActionCreators(Actions, dispatch)
    }
}

export default withTheme(connect(mapStateToProps, mapDispatchToProps)(Iterations))
