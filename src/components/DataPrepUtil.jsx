// @flow
import * as Util from "../Util"
import tValue from "sample-sizer/lib/tvalue"
import * as Types from "../types"

/** recordMap: 
 * [
 *  {xLabel: xLabel1, id: id, count: count, data: [{category: dataCategory1, value: value, valueSquared: valueSquared}, ...]},
 *  {xLabel: xLabel2, id: id, count: count, data: [{category: dataCategory1, value: value, valueSquared: valueSquared}, ...]},
 *   ...
 * ]
 * 
 * Takes a recordMap, sorts each entries category data by value, and extracts the top n labels from each 
 * into a set. 
 */

export function buildAndFilterCategoryLabels(recordMap: ?Array<Types.RecordMapType>, n: number): Set<string> {
    let labels = new Set()
    if (recordMap == null) return labels
    // Build labels: labels will hold union of top n categories from each
    // record which represents a single bar on the bar chart
    recordMap.forEach(barData => {
        barData.data.sort(function (categorySet1, categorySet2) { return -categorySet1.value + categorySet2.value })
        barData.data.slice(0, n).map(categorySet => { return categorySet.category }).forEach(category => { labels.add(category) })
    })
    // Filter on those labels
    recordMap.forEach(barData => {
        barData.data = filterDataByLabels(barData.data, labels)
    })
    return labels
}

/** 
 * Filters [{category: dataCategory1, value: value, valueSquared: valueSquared}, ...] list of category data entries.
 * by categories within selectedCategories and adds all others to new category "other". Adds "other" to 
 * selectedCategories if needed.
 */
export function filterDataByLabels(categoryData: Array<Types.RecordMapDataType>, selectedCategories: Set<string>): Array<Types.RecordMapDataType> {
    // filteredData will hold all the entries that belong to selectedCategories.
    let filteredData = categoryData.filter(dataEntry => selectedCategories.has(dataEntry.category))

    let otherDataValue = categoryData.filter(dataEntry => !selectedCategories.has(dataEntry.category)).reduce((a, b) => a + b.value, 0)
    if (otherDataValue > 0) {
        filteredData.push({ category: "other", value: otherDataValue, valueSquared: 0, count: 1 })
        selectedCategories.add("other")
    }
    return filteredData.filter(dataEntry => dataEntry.value > 0)
}

/**
 * Calculates mean and standard deviation over visibleLabels in dataset for band over iterations.
 * @param visibleLabels : Set of labels currently visible on the graph. i.e.: CPU and GPU, but not Network
 * @param dataSets: Array of datasets for each label on Graph. i.e. [{label: CPU, value: 9, ...}, ...]
 */
export function calcStats(visibleLabels: Set<string>, dataSets: Array<Types.ChartDatasetType>): { mean: number, deviation: number } {
    if (dataSets.length === 0)
        return {
            mean: 0,
            deviation: 0
        }
    let count = dataSets[0].data.length
    let barValues = new Array(count).fill(0)
    for (let dataEntry of dataSets) {
        if (visibleLabels.has(dataEntry.label)) {
            for (let i = 0; i < count; i++) {
                barValues[i] += dataEntry.data[i]
            }
        }
    }
    let expected = 0
    let expectedSquares = 0
    for (let i = 0; i < count; i++) {
        expectedSquares += barValues[i] * barValues[i]
        expected += barValues[i]
    }
    expected = expected / count
    expectedSquares = expectedSquares / count
    let deviation = Math.sqrt(expectedSquares - (expected * expected))
    return {
        mean: expected,
        deviation: deviation
    }
}

/**
 *  Calculates a confidence interval of a category in one bar of data 
 *  @param data : Array of data for each category in a single bar  
 *  @param name : name of category to compute confidence interval over
 */
export function calcConfInt(data: Array<Types.RecordMapDataType>, name: string): number {
    if (name === "other") return -1
    let dataEntry = data.find(catData => catData.category === name)
    if (dataEntry == null || dataEntry.count < 2) return -1
    let count = dataEntry.count
    let variance = dataEntry.valueSquared - dataEntry.value * dataEntry.value
    // variance is only negative if sigma is 0 and a floating point error occurs.
    if (variance < 0) return 0
    let t_star = tValue(Math.min(count - 1, 200), .95)
    return t_star * Math.sqrt(variance / count)
}

/**
 *  Retrieves value of a Category from one Bar of Data
 *  @param data : Array of data for each category in a single bar 
 *  @param name : name of category need the value of 
 */
export function getValueOfUnit(data: Array<Types.RecordMapDataType>, name: string): number {
    let dataEntry = data.find(catData => catData.category === name)
    if (dataEntry == null) return 0
    return dataEntry.value
}

/**
 *  Retrieves the count of a specific category out of a record. 
 *  @param data : Array of data for each category in a single bar 
 *  @param name : name of category need the value of 
 */
export function getCountOfUnit(data: Array<Types.RecordMapDataType>, name: string): number {
    let dataEntry = data.find(catData => catData.category === name)
    if (dataEntry == null) return 0
    return dataEntry.count
}

/** Retrieves the standard deviation of a specific category out of a record
 *  @param data : Array of data for each category in a single bar 
 *  @param name : name of category need the value of 
 */
export function getDeviationOfUnit(data: Array<Types.RecordMapDataType>, name: string): number {
    let dataEntry = data.find(catData => catData.category === name)
    if (dataEntry == null) return -1
    // want to avoid displaying floating point errors. 
    if (dataEntry.count < 2) return 0
    let variance = dataEntry.valueSquared - dataEntry.value * dataEntry.value
    // variance will only ever be negative because of floating point error for a sigma which is zero.
    // backend unit tests to ensure correctness of this
    if (variance < 0) return 0
    return Math.sqrt(variance)
}

/**
 * Builds an extra category that has no data in it. Used for "Hide All" and "Show All" 
 * @param name : label wanted for Extra Category
 * @param length: number of dummy entries needed in data
 */
export function buildExtraCategory(label: string, length: number): Types.ChartDatasetType {
    let fillArray = new Array(length).fill(0)
    return {
        label,
        xLabels: new Array(length).fill("0"),
        data: fillArray,
        borderWidth: fillArray,
        counts: fillArray,
        deviations: fillArray,
        intervals: fillArray,
        id: new Array(length).fill("0"),
        backgroundColor: "rgb(0, 0, 0, .7)",
        visible: false,
        borderColor: "black",
        errorBars: null
    }
}

/**  
 * Builds datasets for ChartJs to use out of recordMap. 
 * datasets is an array of objects {label: GPU, bordercolor, backgroundcolor, data, id}
 */
export function buildChartDatasets(recordMap: Array<Types.RecordMapType>, dataLabels: Set<string>, borderColor: string, selectedRecord: ?Types.CommitType): Array<Types.ChartDatasetType> {
    let datasets = []
    let n = recordMap.length
    let showAll = buildExtraCategory("Show All", n)
    let hideAll = buildExtraCategory("Hide All", n)
    // build datasets: datasets are per component and have data for every x entry. ie: 
    // {label: CPU, data: [5, 6, 7, 4] 
    dataLabels.forEach(name => {
        let errorBars = {}
        let intervals = []
        recordMap.forEach(r => {
            let interval = calcConfInt(r.data, name)
            errorBars[r.xLabel] = { plus: interval, minus: -interval }
            intervals.push(interval)
        })
        let data = {
            label: name,
            xLabels: recordMap.map(r => r.xLabel),
            borderColor: borderColor,
            backgroundColor: Util.getRandomRGB(name, 100),
            borderWidth: recordMap.map((r) => (selectedRecord != null && r.commitId === selectedRecord._id) ? 2.5 : 1),
            data: recordMap.map(r => getValueOfUnit(r.data, name)),
            counts: recordMap.map(r => getCountOfUnit(r.data, name)),
            deviations: recordMap.map(r => getDeviationOfUnit(r.data, name)),
            visible: name !== "other",
            errorBars,
            intervals,
            id: recordMap.map(d => d.id),
            commitId: recordMap.map(d => d.commitId)
        }
        datasets.push(data)
    })
    datasets.push(showAll)
    datasets.push(hideAll)
    return datasets
}

/** 
 * Create the annotation object the ChartJs annotation plugin needs to draw the mean and standard deviation bar on Iterations
 * @param stats: Object containing the mean and standard deviation to use
 */
export function getAnnotation(stats: { mean: number, deviation: number }): Object {
    return {
        drawTime: "afterDatasetsDraw",
        events: ["mouseover", "mouseout"],
        annotations: [{
            type: "line",
            mode: "horizontal",
            scaleID: "yAxis",
            value: stats.mean + stats.deviation,
            borderWidth: 0,
            borderColor: "rgb(0, 0, 0, .7)",
            label: {
                enabled: stats.deviation !== 0,
                content: "\u03BC\u0302 + \u03C3\u0302",
                position: "right",
                xAdjust: 25
            }
        }, {
            type: "line",
            mode: "horizontal",
            scaleID: "yAxis",
            value: stats.mean - stats.deviation,
            borderWidth: 0,
            borderColor: "rgb(0, 0, 0, .7)",
            label: {
                enabled: stats.deviation !== 0,
                content: "\u03BC\u0302 - \u03C3\u0302",
                position: "right",
                xAdjust: 25
            }
        }, {
            type: "line",
            mode: "horizontal",
            scaleID: "yAxis",
            value: stats.mean,
            borderWidth: 2,
            borderColor: "rgb(255, 70, 70, .8)",
            label: {
                enabled: true,
                content: "\u03BC\u0302 = " + Math.round(stats.mean) + ", \u03C3\u0302 = " + Math.round(stats.deviation) + ", \u03C3\u0302/\u03BC\u0302 = " + (Math.round(stats.deviation / stats.mean * 100) / 100),
                position: "right"
            }
        }]
    }
}

/* Creates a custom tooltip for Iterations page: should just show the xLabel, category hovering over, and its value */
export function getIterationsTooltip(): Object {
    return {
        custom: function (tooltip) {
            if (!tooltip) return
            tooltip.displayColors = false
            // tooltip.opacity = 100
        },
        callbacks: {
            label: function (tooltipItem, data) {
                var i = tooltipItem.index
                var dataset = data.datasets[tooltipItem.datasetIndex]
                let labels = []
                labels.push(dataset.label)
                labels.push((Math.round(dataset.data[i] * 100) / 100).toString())
                return labels
            }
        }
    }
}

/* Creates options to render ChartJs in the correct way */
export function getOptions(context: any): Object {
    const options = {
        responsive: true,
        tooltips: {
            custom: function (tooltip) {
                if (!tooltip) return
                tooltip.displayColors = false
                // tooltip.opacity = 100
            },
            callbacks: {
                label: function (tooltipItem, data) {
                    var i = tooltipItem.index
                    var dataset = data.datasets[tooltipItem.datasetIndex]
                    let labels = []
                    labels.push(dataset.label)
                    let deviation = dataset.deviations[i] >= 0 ? Math.round(dataset.deviations[i] * 100) / 100 : "undefined"
                    let mu = Math.round(dataset.data[i] * 100) / 100 
                    labels.push("n= " + dataset.counts[i] + ", \u03BC\u0302= " + mu + ", \u03C3\u0302= " + deviation)
                    let interval = dataset.intervals[i] >= 0 ?  "\u03BC\u0302 \xB1 " + Math.round(dataset.intervals[i] * 100) / 100: "N/A"
                    labels.push("95% CI: " + interval)
                    return labels
                }
            }
        },
        maintainAspectRatio: false,
        scales: {
            x: {
                    id: "xAxis",
                    stacked: true,
                    scaleLabel: {
                        display: true,
                        labelString: context.props.xLabel,
                        fontSize: context.props.theme.typography.h3.fontSize
                    }
                },
            y: {
                    id: "yAxis",
                    stacked: true,
                    scaleLabel: {
                        display: true,
                        labelString: context.props.yLabel,
                        fontSize: context.props.theme.typography.h3.fontSize
                    }
                }
        },

        legend: {
            labels: {
                fontSize: context.props.theme.typography.h3.fontSize
            },
            onClick: function (e, legendItem) {
                var ci = this.chart
                var legendItems = ci.legend.legendItems
                let visibleLabels = new Set()
                if (legendItem.text === "Hide All") {
                    legendItems.filter(l => l.text !== "Show All").filter(l => l.text !== "Hide All").filter(l => !l.hidden).forEach(l => {
                        let index = l.datasetIndex
                        let meta = ci.getDatasetMeta(index)
                        meta.hidden = meta.hidden === null ? true : null
                    })
                } else if (legendItem.text === "Show All") {
                    legendItems.filter(l => l.text !== "Show All").filter(l => l.text !== "Hide All").forEach(l => {
                        let index = l.datasetIndex
                        let meta = ci.getDatasetMeta(index)
                        if (l.hidden)
                            meta.hidden = meta.hidden === null ? false : null
                        visibleLabels.add(l.text)
                    })
                } else {
                    legendItems.filter(l => !l.hidden).filter(l => l !== legendItem).filter(l => l.text !== "Show All").filter(l => l.text !== "Hide All").forEach(l => visibleLabels.add(l.text))
                    if (legendItem.hidden)
                        visibleLabels.add(legendItem.text)
                    var index = legendItem.datasetIndex
                    var meta = ci.getDatasetMeta(index)
                    // See controller.isDatasetVisible comment
                    meta.hidden = meta.hidden === null ? !ci.data.datasets[index].hidden : null
                }
                // We hid a dataset ... rerender the chart
                ci.update()
                context.setState({ visibleCategories: visibleLabels })
            }
        },
        onClick: function (c, i) {
            if (i.length <= 0)
                return
            let e = i[0]
            let testRecordId = context.props.recordMap[e.index].id
            let projectId = context.props.projectId
            let url = "../timelines/index.html?projectId=" + projectId + "&id=" + testRecordId
            console.log("redirecting " + url)
            let win = window.open(url, '_blank')
            win.focus()
        }
    }
    return options
}
