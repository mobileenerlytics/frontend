// @flow
import * as Types from "../types"

export const units: Array<Types.Unit> = [
    {
        label: "Energy: components",
        extractData: (t: Types.TestRecordType, count: number) => {
            return t.componentUnit.map(comp => {
                return {
                    category: comp.name,
                    value: comp.energy,
                    valueSquared: comp.energySquared,
                    count: count
                }
            })
        },
        scaleLabelStr: "Energy (uAh)"
    },
    {
        label: "Power: components",
        extractData: (t: Types.TestRecordType, count: number) => {
            return t.componentUnit.map(comp => {
                return {
                    category: comp.name,
                    value: comp.power,
                    valueSquared: comp.powerSquared,
                    count: count
                }
            })
        },
        scaleLabelStr: "Power (mA)"
    },
    {
        label: "Energy: threads",
        extractData: (t: Types.TestRecordType, count: number) => {
            return t.threadUnit.map(thread => {
                return {
                    category: thread.name,
                    value: thread.energy,
                    valueSquared: thread.energySquared,
                    count: count
                }
            })
        },
        scaleLabelStr: "Energy (uAh)"
    },
    {
        label: "Power: threads",
        extractData: (t: Types.TestRecordType, count: number) => {
            return t.threadUnit.map(thread => {
                return {
                    category: thread.name,
                    value: thread.power,
                    valueSquared: thread.powerSquared,
                    count: count
                }
            })
        },
        scaleLabelStr: "Power (mA)"
    },
    {
        label: "Run time: jobs",
        extractData: extractEvent("job"),
        scaleLabelStr: "Time (s)"
    },
    {
        label: "Hold time: wakelocks",
        extractData: extractEvent("wake_lock_in"),
        scaleLabelStr: "Time (s)"
    },
    {
        label: "Alarms",
        extractData: extractEvent("alarm"),
        scaleLabelStr: "Time (s)"
    }
]

function extractEvent(eventName): (record: Types.TestRecordType, count: number)=> Array<Types.RecordMapDataType> {
    return (t: Types.TestRecordType, count: number) => {
        if (t.events === undefined) return []
        let data = t.events
            .filter(e => e.eventName === eventName)
            .map(e => {
                return {
                    category: e.name,
                    value: e.durationSec,
                    valueSquared: e.durationSecSquared,
                    count: count
                }
            })
        return data;
    }
}

export function buildAnalysisChartData(testRecord: Types.TestRecordType, extractData: (record: Types.TestRecordType, count: number)=> Array<Types.RecordMapDataType>): Array<Types.RecordMapDataType> {
    if (testRecord == null) return [];
    let count = testRecord.count ? testRecord.count : 1
    return extractData(testRecord, count)
}

export function buildDataMap(records: Array<Types.TestRecordType>, unit: Types.Unit, iterations: boolean): Array<Types.RecordMapType> {
    if (records == null) return []
    return records.map((record) => {
        return {
            xLabel: iterations ? new Date(record.updatedMs).toLocaleString() : record.commitHash.substring(0, 25),
            id: record._id,
            commitId: record.commitId,
            data: buildAnalysisChartData(record, unit.extractData)
        }
    })
}
