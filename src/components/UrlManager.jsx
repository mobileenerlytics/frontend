// @flow
import * as React from "react"
import type { RouterHistory } from "react-router"
import * as Types from '../types'

/**
 * Component in charge of Managing search URL. 
 * @param {string} branchName - name of current branch
 * @param {string} testName - name of currnet test
 * @param {number} count - current number of tests displayed
 * @param {Types.CommitType} selectedCommit - current commit selected
 * @param {RouterHistory} history - RouterHistory to update URL
 */
type Props = {
    branchName: ?string,
    testName: ?string,
    count: ?number,
    selectedCommit: ?Types.CommitType,
    history: RouterHistory,
}

class UrlManager extends React.PureComponent<Props> {
    constructor(props: Props) {
        super(props)
        let query = ``
        if (props.branchName) query += `branchName=${props.branchName}`
        if (props.testName) query += `&testName=${props.testName}`
        if (props.count) query += `&count=${props.count.toString()}`
        if (props.selectedCommit) query += `&selectedCommit=${props.selectedCommit.hash}`
        props.history.push(`?${query}`)
    }

    render() {
        return <div />
    }
}

export default UrlManager