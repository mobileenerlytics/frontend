import React from "react";
import { AppBar, Toolbar, MenuItem, Menu, Popover, IconButton } from "@material-ui/core";
import { Dehaze, MoreVert } from '@material-ui/icons';
import { Link, withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import _ from "underscore";
import * as Actions from "../actions";
import { withTheme } from '@material-ui/core/styles';

class Appbar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      anchorEl: null
    };
    this.handleClick = this.handleClick.bind(this);
    this.handleRequestClose = this.handleRequestClose.bind(this);
    this.signoff = this.signoff.bind(this);
    this.startTour = this.startTour.bind(this);
  }

  signoff() {
    console.log("signoff()  ");
    console.log(document.cookie);
    document.cookie='eagleHash= ;expires=Thu, 01 Jan 2010 00:00:00 GMT;domain=.mobileenerlytics.com;'
    window.location.href = "login.html";
    console.log(document.cookie);
  }

  handleClick(event) {
    // This prevents ghost click.
    event.preventDefault();
    this.setState({
      anchorEl: event.currentTarget
    });
  }

  handleRequestClose() {
    this.setState({
      anchorEl: null
    });
  }

  startTour() {
    this.handleRequestClose()
    console.log("In startTour");
    this.props.actions.updateProjectStart("tourStatus","true", this.props.projectId, ()=>console.log("updated tourStatus in appbar onstartTour"));
  }

  startTourManual() {
    window.open('https://www.youtube.com/watch?v=2kyaWORXtQw');
  }

  render() {
    const appbarOpen = {
      width: "80vw",
      margin: 0,
      //background: this.props.muiTheme.palette.textColor,
      //color: "#f7f7f7",
      gridArea: "h",
      top: 0,
      left: "20vw"
    };

    const appbarClosed = _.extend({}, appbarOpen, {
      width: "94vw",
      left: "6vw"
    });

    return (
      <div style={{borderRight: '0px', borderLeft: '0px'}} className="grid-item">
        {/* <AppBar className={this.props.openDrawer ? 'appbarOpen' : 'appbarClosed'}>
          // 
          // className approach doesn't work here. React material UI overrides user setting.
          // Need to set this in-place using style to override material UI settings */
        }
        <AppBar style={this.props.openDrawer ? appbarOpen : appbarClosed}>
          <Toolbar>
            <IconButton className="iconStyle" onClick={() =>
              this.props.actions.openDrawer(!this.props.openDrawer)
            }>
              <Dehaze/>
            </IconButton>
            {this.props.pageTitle}
            <span>
              <IconButton edge="end" className="iconStyle" onClick={this.handleClick} aria-haspopup="true">
                <MoreVert/>
              </IconButton>
              <Popover
                className="popoverMenu"
                open={Boolean(this.state.anchorEl)}
                anchorEl={this.state.anchorEl}
                anchorOrigin={{ horizontal: "left", vertical: "bottom" }}
                transformOrigin={{ horizontal: "right", vertical: "top" }}
                onClose={this.handleRequestClose}
              >
                <Menu>
                  <a className="docsLink" href={this.props.profile.role === "manual" ? "https://tester.mobileenerlytics.com/instructions.html?subscription=manual" : "https://tester.mobileenerlytics.com/instructions.html"} target="_blank" rel="noreferrer noopener">
                    <MenuItem primaryText="Getting Started" />
                  </a>
                  <a href="mailto:support@mobileenerlytics.com?Subject=Eagle Help Ticket">
                    <MenuItem primaryText="Help &amp; feedback" />
                  </a>
                  <Link to='settings' onClick={this.handleRequestClose}>
                    <MenuItem primaryText="Settings" />
                  </Link>
                  <Link to='/' onClick= {this.props.profile.role === "manual" ? this.startTourManual : this.startTour}>
                    <MenuItem primaryText="Guided Tour"/>
                  </Link>
                  <MenuItem primaryText="Sign out" onClick= {this.signoff}/>
                </Menu>
              </Popover>
            </span>
          </Toolbar>
        </AppBar>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    openDrawer: state.openDrawer,
    projectId: state.project[state.common.curProjectIndex]._id,
    profile: state.profile,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Actions, dispatch)
  };
}

export default withRouter(withTheme(connect(mapStateToProps, mapDispatchToProps)(Appbar)));
