import React from 'react';
import { Sunburst } from 'react-vis';
import { LabelSeries } from 'react-vis';
import { withRouter } from "react-router-dom";
import { withTheme } from '@material-ui/core/styles';


/**
 * Recursively work backwards from highlighted node to find path of valud nodes
 * @param {Object} node - the current node being considered
 * @returns {Array} an array of strings describing the key route to the current node
 */
function getKeyPath(node) {
  if (!node.parent) {
    return [];
  }
  // todo 
  return [(node.data && node.data.nameDisplay) || node.nameDisplay].concat(getKeyPath(node.parent));
}


function colorData(data) {
  if (data.children) data.children.map(child => colorData(child));
  return data;
}

/**
* Recursively modify data depending on whether or not each cell has been selected by the hover/highlight
* @param {Object} data - the current node being considered
* @param {Object|Boolean} keyPath - a map of keys that are in the highlight path
* if this is false then all nodes are marked as selected
* @returns {Object} Updated tree structure
*/
function updateData(data, keyPath) {
  if (data.children) {
    data.children.map(child => updateData(child, keyPath));
  }
  data.style = {
    ...data.style,
    fillOpacity: keyPath && !keyPath[data.nameDisplay] ? 0.2 : 1
  };

  return data;
}


function Arrow({ sequenceData }) {
  return (
    /*<div style={{ display: "flex" }}>*/
    <svg style={{minWidth: "550px", height: "40px"}}>
      {
        sequenceData.map((data, index) => {
          var width=160;
          // console.log(data);
          var label=(data.label.length<15) ? data.label : (data.label.substr(0, 13) + "...");
          return (
            /*<div key={data.label}>
              <span                
                style={{backgroundColor: data.hex, fontFamily: "Open Sans", color: "white", fontSize: "13px" }}
              >*/
              <g key={data.label} transform={`translate(${(width + 3) * index}, 0)`}>
              <polygon points={`0,0 ${width},0 ${width + 10},15 ${width},30 0,30 10,15`} style={{fill: data.hex}}></polygon>
              <text x={`${width/2}`} y="15" dy="0.35em" textAnchor="middle" fill="#fff" fontWeight="600">{label}</text>
              </g>
                /*{data.label}
              </span>
              <span> > </span>
            </div>*/
          );
        })
      }
      </svg>
    /*</div>*/
  );
}


function getSequenceData(node) {
  if (!node) return [];
  var data = [];
  data.unshift({
    label: node.nameDisplay,
    hex: node.hex,
  })
  var curNode = node.parent;
  while (curNode != null && curNode.parent != null) {
    data.unshift({
      label: curNode.data.nameDisplay,
      hex: curNode.data.hex,
    })
    curNode = curNode.parent;
  }
  return data;
}

class BasicSunburst extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      currentNode: false,
      clicked: false
    }

    // const BIG_LABEL_STYLE = {
    //   fontSize: this.props.muiThemeable.pallete.typography.h2.fontSize,
    //   textAnchor: 'middle'
    // };

    // const SMALL_LABEL_STYLE = {
    //   fontSize: this.props.muiThemeable.pallete.typography.h3.fontSize,
    //   textAnchor: 'middle'
    // };

  }

  render() {

    const { clicked, currentNode } = this.state;
    var { data } = this.props;
    data = colorData(data);
    data = updateData(data, false);
    const sum = data.sum;
    const sequenceData = getSequenceData(currentNode);
    return (
      <div className="basic-sunburst-example-wrapper">   
        <div style= {{minHeight: "40px"}}> 
          <Arrow  sequenceData={sequenceData} />
        </div>           
        <Sunburst
          animation
          className="basic-sunburst-example"
          hideRootNode
          onValueMouseOver={node => {
            if (clicked) {
              return;
            } //node.value
            const path = getKeyPath(node).reverse();
            const pathAsMap = path.reduce((res, row) => {
              res[row] = true;
              return res;
            }, {});
            this.setState({
              currentNode: node,
              data: updateData(data, pathAsMap)
            });
          }}
          onValueMouseOut={() => clicked ? () => { } : this.setState({
            currentNode: false,
            finalValue: false,
            data: updateData(data, false)
          })}
          onValueClick={() => this.setState({ clicked: !clicked })}
          style={{
            stroke: '#ddd',
            strokeOpacity: 0.3,
            strokeWidth: '0.5'
          }}
          colorType="literal"
          getSize={d => d.value}
          getColor={d => d.hex}
          data={data}
          height={550}
          width={550}>
          
          {currentNode ?
            <LabelSeries data={[
              { x: 0, 
                y: 0, 
                label: `${(currentNode.sum / sum * 100).toFixed(2)}%`, 
                style: {fontSize: this.props.theme.typography.h2.fontSize, textAnchor: "middle"}
              }, 
              { x: 0, 
                y: -20, 
                label: `${currentNode.sum.toFixed(2)} uAh`,
                style: {fontSize: this.props.theme.typography.chart.fontSize, textAnchor: "middle"}
              },
              { x: 0, 
                y: -40, 
                label: `out of ${sum.toFixed(2)} uAh`,
                style: {fontSize: this.props.theme.typography.chart.fontSize, textAnchor: "middle"}
              }
            ]} /> 
          :
            <LabelSeries data={[
            { x: 0, 
              y: 0, 
              label: `${sum.toFixed(2)}`, 
              style: {fontSize: this.props.theme.typography.h2.fontSize, textAnchor: "middle"}
            },
            { x: 0, 
              y: -20, 
              label: `uAh total spent energy`,
              style: {fontSize: this.props.theme.typography.chart.fontSize, textAnchor: "middle"}
            }
          ]} />
        }
        </Sunburst>
        <div>
      {/*clicked ? 'click to unlock selection' : 'click to lock selection'*/}
      </div>

      </div>
    );
  }

}

export default withRouter(
  withTheme(BasicSunburst)
);
