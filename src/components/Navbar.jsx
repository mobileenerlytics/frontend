// @flow
import React from "react"
import logo from "../assets/logo.png"
import logoTrimmed from "../assets/logo-trimmed.png"
import { Link, withRouter } from "react-router-dom"
import { connect } from "react-redux"
import { bindActionCreators } from "redux"
import * as Actions from "../actions"
import { Home, Assessment, Settings } from "@material-ui/icons"
import { MenuItem, List, ListItem, ListSubheader, SvgIcon } from "@material-ui/core"
import { withTheme } from '@material-ui/core/styles'
import type { ContextRouter } from "react-router"
import * as Types from "../types"
import type { Theme } from "@material-ui/core/styles/createMuiTheme"

const ReleaseIcon = props => (
  <SvgIcon {...props}>
    <path
      d="M19 11c0 1.3-.8 2.4-2 2.8V15c0 2.2-1.8 4-4 4h-2.2c-.4 1.2-1.5 2-2.8 2-1.7 0-3-1.3-3-3 0-1.3.8-2.4 2-2.8V8.8C5.9 8.4 5 7.3 5 6c0-1.7 1.3-3 3-3s3 1.3 3 3c0 1.3-.8 2.4-2.1 2.8v6.4c.9.3 1.6.9 1.9 1.8H13c1.1 0 2-.9 2-2v-1.2c-1.2-.4-2-1.5-2-2.8 0-1.7 1.3-3 3-3s3 1.3 3 3zM8 5c-.5 0-1 .4-1 1s.4 1 1 1 1-.4 1-1-.4-1-1-1zm8 7c.6 0 1-.4 1-1s-.4-1-1-1-1 .4-1 1 .4 1 1 1zm-8 7c.6 0 1-.4 1-1s-.4-1-1-1-1 .4-1 1 .4 1 1 1z"
      fill="action"
      fillRule="evenodd"
      role="presentation"
    />
  </SvgIcon>
)

const CommitIcon = props => (
  <SvgIcon {...props}>
    <path
      d="M16 12c0-1.9-1.3-3.4-3-3.9V4c0-.6-.4-1-1-1s-1 .4-1 1v4.1c-1.7.4-3 2-3 3.9s1.3 3.4 3 3.9V20c0 .6.4 1 1 1s1-.4 1-1v-4.1c1.7-.5 3-2 3-3.9zm-4 2c-1.1 0-2-.9-2-2s.9-2 2-2 2 .9 2 2-.9 2-2 2z"
      fill="action"
      fillRule="evenodd"
      role="presentation"
    />
  </SvgIcon>
)

type OwnProps = {
  theme: Theme
}
type Props = {
  ...OwnProps,
  ...ContextRouter,   // For history and location to work
  ...Types.ReduxProps<typeof mapStateToProps, typeof mapDispatchToProps>
}

class Navbar extends React.PureComponent<Props> {

  render() {    
    const { location, about, profile } = this.props
    const {role} = profile
    let isManualUser = role === "manual" 
    const { pathname } = location
    const color = "#f7f7f7"
    return (
      <div className={this.props.openDrawer ? "navBarOpen" : "navBarClose"}>

        {/* logo, userinfo */}
        <div id="navbarWrapper">
          <List>
            <ListItem disabled={true}>
              {this.props.openDrawer && <img src={logo} className="appLogoOpen" alt="logo" />}
              {!this.props.openDrawer && <img src={logoTrimmed} className="appLogoClose" alt="logo" />}
            </ListItem>
            {this.props.openDrawer &&
              <ListSubheader>
                <br/><br/>
                <h3 className='navbar-username'>
                  Welcome {this.props.profile.firstname}
                </h3>
                <p className='navbar-user-id' id="user-id">{this.props.profile.username}</p>
              </ListSubheader>
              }
          </List>
          <div>
            <ul className={"navbarLinks"}>

              <Link to="/" className={pathname.endsWith("/") ? 'navbar-selected' : ''} hidden={isManualUser}>
                <p className="navbarLink" >
                  <MenuItem className={this.props.openDrawer ? "navSpanFlexOpen" : "navSpanFlexClose"} style={{display: "inline"}} >
                    <span className="spanFlexItem">
                      <Home className="naviconStyle" />
                    </span>
                    {this.props.openDrawer && <span className="spanFlexItem" id="overviewDrawer">Overview</span>}
                  </MenuItem>
                </p>
              </Link>
              {/* todo toutev1 */}
              <Link to="/release" className={pathname.endsWith("/release") ? 'navbar-selected' : ''} hidden={isManualUser} >
                <p className="navbarLink">
                  <MenuItem className={this.props.openDrawer ? "navSpanFlexOpen" : "navSpanFlexClose"}  style={{display: "inline"}}>
                    <span className="spanFlexItem">
                      <ReleaseIcon className="naviconStyle" />
                    </span>
                    {this.props.openDrawer && <span className="spanFlexItem" id="releaseTrendDrawer"> Release Trend </span>}
                  </MenuItem>
                </p>
              </Link>
              <Link to="/commit?branchName=origin/master" className={pathname.endsWith("/commit") ? 'navbar-selected' : ''} hidden={isManualUser}  >
                <p className="navbarLink">
                  <MenuItem className={this.props.openDrawer ? "navSpanFlexOpen" : "navSpanFlexClose"}  style={{display: "inline"}}>
                    <span className="spanFlexItem">
                      <CommitIcon className="naviconStyle" />
                    </span>
                    {this.props.openDrawer && <span className="spanFlexItem" id="commitTrendDrawer">Commit Trend</span>}
                  </MenuItem>
                </p>
              </Link>
              <Link to="/analysis" className={pathname.endsWith("/analysis") ? 'navbar-selected' : ''} >
                <p className="navbarLink">
                  <MenuItem className={this.props.openDrawer ? "navSpanFlexOpen" : "navSpanFlexClose"}  style={{display: "inline"}}>
                    <span className="spanFlexItem">
                      <Assessment className="naviconStyle" />
                    </span>
                    {this.props.openDrawer && <span className="spanFlexItem" id="testAnalysisDrawer"> Test Analysis </span>}
                  </MenuItem>
                </p>
              </Link>
              <Link to="/settings" className={pathname.endsWith("/settings") ? 'navbar-selected' : ''}>
                <p className="navbarLink">
                  <MenuItem className={this.props.openDrawer ? "navSpanFlexOpen" : "navSpanFlexClose"}  style={{display: "inline"}}>
                    <span className="spanFlexItem">
                      <Settings className="naviconStyle"/>
                    </span>
                    {this.props.openDrawer && <span className="spanFlexItem" id="settingsDrawer">Settings</span>}
                  </MenuItem>
                </p>
              </Link>
            </ul>
          </div>

          {/* about */}
          <div style={{color: `${this.props.theme? this.props.theme.palette.greyDarker : color}`, fontSize: "small"}}>
           {this.props.openDrawer && `Eagle Tester \n`} {about.version}
          </div>

        </div>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    openDrawer: state.openDrawer,
    profile: state.profile,
    about: state.about,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Actions, dispatch)
  }
}

export default withRouter(
  withTheme(connect(mapStateToProps, mapDispatchToProps)(Navbar))
)
