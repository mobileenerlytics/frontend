// @flow
import React from "react"
import { MenuItem, Menu, CircularProgress, Button } from "@material-ui/core"
import _ from "underscore"
import { connect } from "react-redux"
import { withRouter } from "react-router-dom"
import * as Util from "../Util"
import * as UnitUtil from "./UnitUtil"
import * as Types from '../types'
import type { ContextRouter } from "react-router"
import PopupState, { bindTrigger, bindMenu } from 'material-ui-popup-state'

/**
 * Holds the dropdown menus to select results to display in Commit/Analysis Container. 
 * Manages the selected branch, unit, count, and testName and passes these on to all components down
 * stream such as Commits.
 * 
 * @param {boolean} testsEnabled - boolean indicating whether to enable testDropDown. Not needed for Commit
 *                                 Container, disabled by default
 * @param {boolean} unitsEnabled - boolean indicating whether to enabled unitsDropDown. Not needed for COmmit
 *                                 Container, disabled by default
 */
type Props = {
  ...ContextRouter,   // For history and location to work
  testsEnabled: ?boolean,
  unitsEnabled: ?boolean,
  ...Types.ReduxProp<typeof mapStateToProps>,
  children: (currentBranch: Types.BranchType, unit: Types.Unit, count: number, testName: string) => Object
}

type State = {
  currentBranch: Types.BranchType,
  unit: Types.Unit,
  count: number,
  testName: string,
}

class AnalysisTopbar extends React.PureComponent<Props, State> {
  constructor(props) {
    super(props);
    const search = new URLSearchParams(props.location.search)
    let defaultBranch = _.first(props.allBranches)
    let branchName = Util.extractFromURL(search, "branchName", defaultBranch.branchName)
    let currentBranch = props.allBranches.find(branch => branch.branchName === branchName)
    let firstTest = (currentBranch && currentBranch.tests.length > 0) ? currentBranch.tests[0] : ""
    let testName = Util.extractFromURL(search, "testName", firstTest)
    let count = parseInt(Util.extractFromURL(search, "count", "10"))
    this.state = {
      currentBranch: (currentBranch)? currentBranch : defaultBranch,
      testName,
      count,
      unit: UnitUtil.units[0],
      loading: false
    }
    this.handleBranchChange = this.handleBranchChange.bind(this)
    this.handleUnitChange = this.handleUnitChange.bind(this)
    this.handleCountChange = this.handleCountChange.bind(this)
    this.handleTestChange = this.handleTestChange.bind(this)
  }

  /*:: handleBranchChange: (Object, string) => void */
  handleBranchChange(selectedBranchName: string, popupState: Object) {
    return (e: Object) => {
      let currentBranch = this.props.allBranches.find(branch => branch.branchName === selectedBranchName)
      let firstTest = (currentBranch && currentBranch.tests.length > 0) ? currentBranch.tests[0] : ""
      this.setState({currentBranch, testName: firstTest})
      popupState.close()
    }
  }

  /*:: handleUnitChange: (Object, number) => void */
  handleUnitChange(unit: Units, popupState: Object) {
    return (e: Object) => {
      this.setState({unit})
      popupState.close()
    }
  }

  /*:: handleCountChange: (number: Object) => void */
  handleCountChange(selectedCount: number, popupState: Object) {
    return (e: Object) => {
      this.setState({count: selectedCount})
      popupState.close()
    }
  }

  /*:: handleTestChange: (string) => void */
  handleTestChange(selectedTestName: string, popupState: Object) {
    return (e: Object) => {
      this.setState({testName: selectedTestName})
      popupState.close()
    }
  }

  render() {
    const numbers = [10, 20, 40];
    const { allBranches, commitStatus } = this.props
    const { currentBranch, unit, count, testName } = this.state
    return (
      <div>
        <div className="topbarColWrapper">
          <div className="topbarWrapper" >
            <div className="topbarItem">
              <div className="branchSelectorStyle">
                <span className="flexAuto"> Branch:&nbsp;&nbsp;</span>
                <span className="flexAuto">
                <PopupState variant="popover" popupId="branch-popup">
                  {(popupState) => (
                    <React.Fragment>
                      <Button variant="contained" disabled={commitStatus === Types.STATUS_LOADING} {...bindTrigger(popupState)}>
                        {currentBranch? currentBranch.branchName : "Set branch name"}
                      </Button>
                      <Menu {...bindMenu(popupState)}>
                        {allBranches.length !== 0 &&
                          allBranches.map(branch => (
                            <MenuItem key={branch.branchName} 
                              onClick={this.handleBranchChange(branch.branchName, popupState)}>
                              {branch.branchName}
                            </MenuItem>
                          ))}
                      </Menu>
                    </React.Fragment>
                  )}
                </PopupState>
                </span>
                  {commitStatus === Types.STATUS_LOADING && <CircularProgress size={24} />}
              </div>
            </div>
            {
              this.props.unitsEnabled &&
              <div className="topbarItem">
                <span>
                  <PopupState variant="popover" popupId="unit-popup">
                    {(popupState) => (
                      <React.Fragment>
                        <Button variant="contained" disabled={commitStatus === Types.STATUS_LOADING} {...bindTrigger(popupState)}>
                          {unit.label}
                        </Button>
                        <Menu {...bindMenu(popupState)}>
                          {UnitUtil.units.map(u => (<MenuItem key={u.label} onClick={this.handleUnitChange(u, popupState)}> {u.label} </MenuItem>))}
                        </Menu>
                      </React.Fragment>
                    )}
                  </PopupState>
                </span>
              </div>
            }
            <div className="topbarItem">
              <span>
                <PopupState variant="popover" popupId="count-popup">
                  {(popupState) => (
                    <React.Fragment>
                      <Button variant="contained" disabled={commitStatus === Types.STATUS_LOADING} {...bindTrigger(popupState)}>
                        {count}
                      </Button>
                      <Menu {...bindMenu(popupState)}>
                        {numbers.map(number => (<MenuItem key={number} onClick={this.handleCountChange(number, popupState)}> {number} </MenuItem>))}
                      </Menu>
                    </React.Fragment>
                  )}
                </PopupState>
              </span>

            </div>

            {this.props.testsEnabled && (
              <div className="topbarItem">
                <div className="testSelectorStyle">
                  <span className="flexAuto"> Test:&nbsp;&nbsp;</span>
                  <span className="flexAuto">
                    <PopupState variant="popover" popupId="test-popup">
                      {(popupState) => (
                        <React.Fragment>
                          <Button variant="contained" disabled={commitStatus === Types.STATUS_LOADING} {...bindTrigger(popupState)}>
                            {testName}
                          </Button>
                          <Menu {...bindMenu(popupState)}>
                            {currentBranch &&
                              currentBranch.tests.map(test => (
                                <MenuItem key={test} onClick={this.handleTestChange(test, popupState)}> {test} </MenuItem>
                              ))
                            }
                          </Menu>
                        </React.Fragment>
                      )}
                    </PopupState>
                  </span>
                </div>
              </div>
            )}
          </div>
        </div>
        <br/>
        <div className="chartColWrapper">
          {this.props.children(currentBranch, unit, count, testName)}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: Types.State) => {
  return {
    allBranches: state.branch,
    openDrawer: state.openDrawer,
    commitStatus: state.commitStatus
  };
};

export default withRouter(
  connect(mapStateToProps)(AnalysisTopbar)
);
