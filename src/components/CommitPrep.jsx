// @flow
import React from 'react'
import * as Types from '../types'

/**
 * CommitPrep receives a list of commits from Commits and prepares the information on specific tests
 * to be passed to ChartDataPrep 
 * It prepares the data into a testrecordMap that will be passed into ChartDataPrep along
 * with Energy(uAh) as the yAxis label.
 * 
 * @param {Array<Types.CommitType>} commitRecords - Array of Commits on the current branch to be rendered
 *                                                  in a Chart
 */
type Props = { 
    commitRecords: Array<Types.CommitType>,
    xLabelIsBranchName: boolean,
    children: (dataMap: Array<Types.RecordMapType>, yLabel: string) => Object
}
class CommitPrep extends React.PureComponent<Props> {

    buildDataMap(commitRecords: Array<Types.CommitType>): Array<Types.RecordMapType> {
        return commitRecords.map(commitRecord => {
            let recordMap = {}
            recordMap.id = commitRecord._id
            recordMap.commitId = commitRecord._id
            recordMap.xLabel = this.props.xLabelIsBranchName ? commitRecord.branchName : commitRecord.hash
            recordMap.data = commitRecord.testRecordSet.map(testRecord => {
                return {
                    category: testRecord.testName, 
                    count: testRecord.count, 
                    value: testRecord.energy, 
                    valueSquared: testRecord.energySquared
                }
            })
            return recordMap
        })
    }

    render() {
        const { commitRecords } = this.props
        let yLabel = "Energy (uAh)"
        let dataMap = this.buildDataMap(commitRecords)
        return (
            <div>
                {this.props.children(dataMap, yLabel)}
            </div>
        )
    }
}

export default CommitPrep
