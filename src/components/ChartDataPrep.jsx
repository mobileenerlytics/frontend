// @flow
import React from 'react'
import { withTheme } from '@material-ui/core/styles'
import { Bar as BarChart } from "react-chartjs-2"
import { Chart } from "react-chartjs-2"
import { connect } from "react-redux";
import * as DataPrepUtil from "./DataPrepUtil"
import "chartjs-plugin-annotation"
import "../Plugin.Errorbars"
import * as Types from "../types"
import type { ContextRouter } from "react-router"

/**
 * Final step before Rendering in ChartJs. Data should already be filtered
 * by Unit for Test Analysis and organized by test for Commit Trends by the
 * time it reaches this stage.
 * 
 * @param {function} onClick - provides a function to be used as an onClick listener for the bars
 * @param {string} selectedCommit - specifies the commitId for a selectedCommit
 * @param {Set<string>} selectLabels - specifies set of labels (CPU, GPU, etc) to make visible. Passed for Iterations 
 *                                     to guarantee its labels are consistent with the TestAnalysis Chart
 * @param {string} title - specifies title of chart (if there is one)
 * @param {string} xLabel - specifies an xLabel for the Chart
 * @param {string} yLabel - specifies a yLabel for the Chart
 * @param {boolean} meanbar - indicates whether mean band should be drawn
 * @param {Array<Types.RecordMapType>} recordMap - ChartDataPrep receives data to be rendered as a Map:
 * [
 *  {xLabel: xLabel1, id: id, data: [{category: dataCategory1, count: count, value: value, valueSquared: valueSquared}, ...]},
 *  {xLabel: xLabel2, id: id, data: [{category: dataCategory1, count: count, value: value, valueSquared: valueSquared}, ...]},
 *   ...
 * ]
 */
type Props = {
    onClick: ?() => mixed,
    selectedCommit: ?Types.CommitType,
    xLabel: string,
    yLabel: string,
    meanbar: ?boolean,
    recordMap: Array<Types.RecordMapType>,
    selectLabels: ?Set<string>,
    ...ContextRouter,   // For history and location to work
    ...Types.ReduxProp<typeof mapStateToProps>,
    children: ?(labels: Set<string>) => Object,
    theme: Object
}

type State = {
    categories: Set<string>,
    visibleCategories: Set<string>
}

class ChartDataPrep extends React.Component<Props, State> {
    constructor(props) {
        super(props)
        let categories = this.props.selectLabels ? this.props.selectLabels : DataPrepUtil.buildAndFilterCategoryLabels(this.props.recordMap, 3)
        this.state = {
            categories,
            visibleCategories: categories,
        }
    }

    static getDerivedStateFromProps(props: Props, state: State) {
        let recordMap = props.recordMap
        let categories = DataPrepUtil.buildAndFilterCategoryLabels(recordMap, 3)
        return {
            categories,
            visibleCategories: categories
        }
    }

    render() {
        const { recordMap, meanbar, theme, selectedCommit, onClick } = this.props
        const { categories, visibleCategories } = this.state
        let options = DataPrepUtil.getOptions(this)
        if (onClick) options.onClick = onClick
        let datasets = DataPrepUtil.buildChartDatasets(recordMap, categories, theme.palette.graphFirstBorder, selectedCommit)
        const data = {
            labels: recordMap.map(barData => barData.xLabel.substring(0, 25)),
            datasets: datasets
        }
        // Add mean and standard deviation bar 
        if (meanbar) {
            let stats = DataPrepUtil.calcStats(visibleCategories, datasets)
            options.annotation = DataPrepUtil.getAnnotation(stats)
            options.tooltips = DataPrepUtil.getIterationsTooltip()
        }

        Chart.defaults.font.size = theme.typography.chart.fontSize;
        Chart.defaults.font.family = theme.typography.fontFamily;
        return (
            <div>
                <div className="chartColWrapper">
                    <BarChart
                        data={data}
                        options={options}
                        width={100}
                        height={100}
                        ref="chart"
                    />
                </div>
                    {this.props.children != null ?
                        this.props.children(categories)
                        : null}
            </div>
        );
    }
}

const mapStateToProps = (state: Types.State) => {
    return {
        projectId: state.common.projectId
    };
};

export default withTheme(connect(mapStateToProps)(ChartDataPrep))
