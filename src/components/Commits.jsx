// @flow
import React from 'react'
import { withRouter } from "react-router-dom";
import { withTheme } from '@material-ui/core/styles'
import { connect } from "react-redux";
import { bindActionCreators } from "redux"
import * as Actions from "../actions"
import * as Util from "../Util"
import * as Types from '../types'
import type { ContextRouter } from "react-router"

/**
 * Commits is a wrapper for all Commits the graph is using. It receives from Branches
 * a list of commits that contain summaries for specific commits and each test within a 
 * commit. Commits manages which test is selected and passes those testRecords on to Units
 * to render the Bar Chart.
 * 
 * @param {string} currentBranchName - name of current branch 
 * @param {number} count - number of commits being displayed
 * 
 * Passes to children: commitRecords, selectedCommit, selectedRecords, handleCommitSelect
 */
type Props = {
    ...ContextRouter,   // For history and location to work
    ...Types.ReduxProps<typeof mapStateToProps, typeof mapDispatchToProps>,
    currentBranchName: string,
    count: number,
    children: (selectedCommits: Array<Types.CommitType>, selectedCommit: ?Types.CommitType, onCommitSelect: (c: Object, i: Array<Object>)=>void) => Object,
}
type State = {
    midHash: string,
    selectedCommitHash: string,
    selectedCommits: Array<Types.CommitType>,
}

export class Commits extends React.PureComponent<Props, State> {
    constructor(props: Object) {
        super(props)
        const search = new URLSearchParams(props.location.search)
        let selectedCommitHash = Util.extractFromURL(search, "selectedCommit", "")
        let midHash = Util.extractFromURL(search, "selectedCommit", "")
        this.state = {
            midHash,
            selectedCommitHash, 
            selectedCommits: [],
            currentBranchName: this.props.currentBranchName,
            count: this.props.count,
        }
        this.getSelectedCommit = this.getSelectedCommit.bind(this)
        this.handleCommitSelect = this.handleCommitSelect.bind(this)
    }

    componentDidMount() {
        this.props.actions.fetchCommitsStart(this.state.count, null, this.state.currentBranchName, this.state.midHash)
    }

    static getDerivedStateFromProps(props: Object, state: Object) {
        let selectedCommits = Util.getNCommits(props.allCommits,  props.currentBranchName, props.count)
        if (props.currentBranchName !== state.currentBranchName || props.count > state.count) {
            props.actions.fetchCommitsStart(state.count, null, state.currentBranchName)
        }
        return {
            selectedCommits, 
            currentBranchName: props.currentBranchName, 
            count: props.count
        }
    }

    /**
     *  When a commit is selected set the state and re-render. On render if commit selected will 
     *  render any children. 
     */
    /*:: handleCommitSelect: (Object, Array<Object>) => void */
    handleCommitSelect(c: Object, i: Array<Object>): void {
        if (i.length <= 0)
            return
        let e = i[0]
        // e._view.label = 1
        let selectedCommit = this.getSelectedCommit(c.chart, e.index)
        this.setState({ selectedCommitHash: selectedCommit ? selectedCommit.hash : "" })
    }

    /* Retreive the ith commit from the given chart */
    /*:: getSelectedCommit: (Object, number) => ?Types.CommitType */
    getSelectedCommit(chart: Object, i: number): ?Types.CommitType {
        if (chart == null || chart.data == null || chart.data.datasets == null)
            return null
        let datasets = chart.data.datasets;
        if (datasets.length === 0)
            return null
        let commitId = datasets[0].commitId[i]
        return this.state.selectedCommits.find(commit => commit._id === commitId)
    }

    

    render() {
        const { selectedCommitHash, selectedCommits } = this.state
        const { count } = this.props
        const selectedNCommits = selectedCommits.slice(0, count).reverse()
        let selectedCommit = selectedCommits.find((commit) => commit.hash === selectedCommitHash)
        return (
            <div>
                {this.props.children(selectedNCommits, selectedCommit, this.handleCommitSelect)}
            </div>
        )
    }
}

const mapStateToProps = (state: Types.State) => {
    return {
        allCommits: state.commit,
    };
}

const mapDispatchToProps = (dispatch: *) => {
    return {
        actions: bindActionCreators(Actions, dispatch)
    }
}

export default withRouter(
    withTheme(connect(mapStateToProps, mapDispatchToProps)(Commits))
)
