// @flow
import React from 'react'
import * as UnitUtil from "./UnitUtil"
import * as Types from "../types"

/**
 * Units receives a list of test records from Commits and filters them based on the current
 * unit. It prepares the data into a testrecordMap that will be passed into ChartDataPrep along
 * with the Unit label to be used as the yAxis label.
 * 
 * @param {Array<Types.TestRecordType>} records - an array of records that have been filtered by testName for Units to 
 *                                                process
 * @param {Types.Unit} unit - A unit to filter the records by : i.e. componentUnit, threadUnit, etc. 
 * @param {boolean} iterations - A boolean to indicate whether this Unit belongs to iterations. Controls which xLabels 
 *                               to put in RecordMap
 */
type Props = { 
    records: Array<Types.TestRecordType>,
    unit: Types.Unit,
    iterations: boolean,
    children: (dataMap: Array<Types.RecordMapType>, yLabel: string) => Object
}

class Units extends React.PureComponent<Props> {
    render() {
        const { records, unit, iterations } = this.props
        if (records == null) return <div />
        let dataMap = UnitUtil.buildDataMap(records, unit, iterations)
        return (
            <div>
                {this.props.children(dataMap, unit.scaleLabelStr)}
            </div>
        )
    }
}
export default Units
