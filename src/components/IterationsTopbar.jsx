import React from "react";
import { MenuItem, Menu } from "@material-ui/core"
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { bindActionCreators } from "redux";
import * as Actions from "../actions";

class IterationsTopBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: 1,      
    };
  }

  render() {
    const numbers = [10, 20, 40];
    const {loading } = this.props;
    const units = this.props.units;
    const unit = this.props.unit;
    return (
      <div className="topbarWrapper" >
        {
          this.props.handleUnitChange != null &&
        <div className="topbarItem">
          <span>
            <Menu
              value= {unit.label}
              onChange= {this.props.handleUnitChange}
              disabled= {loading}
          >
            {units.map(u => (<MenuItem key={u.label} value={u.label} primaryText={u.label}/> ))}
          </Menu>    
          </span>
        </div>
        }
        <div className="topbarItem">
          <span>
            <Menu
              value= {this.props.count}
              onChange= {this.props.handleCountChange}
              disabled= {loading}
          >
            {numbers.map(number => (<MenuItem key={number} value={number} primaryText={number}/> ))}
          </Menu>    
          </span>

        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    allBranches: state.branch,
    openDrawer: state.openDrawer
  };
};

const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators(Actions, dispatch)
  };
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(IterationsTopBar)
);
